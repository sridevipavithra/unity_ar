﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkDiscovery : NetworkDiscovery {

    public static CustomNetworkDiscovery instance;
    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        base.OnReceivedBroadcast(fromAddress, data);
        if(!CustomNetworkManger.instance.isNetworkActive){
            CustomNetworkManger.instance.networkAddress = fromAddress;
            CustomNetworkManger.instance.StartClient();
        }
    }
}
