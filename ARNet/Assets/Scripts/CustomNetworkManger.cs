﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkManger : NetworkManager {

    public static CustomNetworkManger instance;
    public ARSessionManager sessionManager;
    public GameObject homePanel;
    public UnityARCameraManager cameraManager;
    public PlayerScript currentPlayer;
	// Use this for initialization
	void Start () {
        if (instance == null){
            instance = this;
        }
	}
	
    public void InitializeNetwork(bool host){
        if(!isNetworkActive){
            networkAddress = Network.player.ipAddress;
            if(host){
                StartHost();
                CustomNetworkDiscovery.instance.Initialize();
                CustomNetworkDiscovery.instance.StartAsServer();
            } else {
                CustomNetworkDiscovery.instance.Initialize();
                CustomNetworkDiscovery.instance.StartAsClient();
            }
        }
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        base.OnServerConnect(conn);
        if(NetworkServer.connections.Count == 2){
            sessionManager.InitializeServerSession();
            cameraManager.SetCamera(Camera.main);
            homePanel.SetActive(false);
        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        Debug.Log("Connected to server");
    }
}
