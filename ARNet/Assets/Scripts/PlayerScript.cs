﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR.iOS;

public class PlayerScript : NetworkBehaviour {

	// Use this for initialization
	void Start () {
        if(isLocalPlayer){
            CustomNetworkManger.instance.currentPlayer = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Command]
    public void CmdSendWorldMap(byte[] data){
        StartCoroutine(GetComponent<NetworkTransmitter>().SendBytesToClientsRoutine(0, data));
        //RpcSetWorldMap(worldMap);
    }

    [Client]
    private void OnDataComepletelyReceived(int transmissionId, byte[] data)
    {
        print("Done -" + CustomNetworkManger.instance.networkAddress);
    }

    [Client]
    private void OnDataFragmentReceived(int transmissionId, byte[] data)
    {
        print("Receiving---" + CustomNetworkManger.instance.networkAddress);
    }

}
