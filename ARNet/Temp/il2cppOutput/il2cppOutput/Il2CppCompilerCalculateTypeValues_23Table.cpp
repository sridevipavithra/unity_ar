﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t2049372221;
// UnityEngine.XR.iOS.ARResourceFilename[]
struct ARResourceFilenameU5BU5D_t3417617624;
// UnityEngine.XR.iOS.ARResourceInfo
struct ARResourceInfo_t1991666228;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4
struct serializableUnityARMatrix4x4_t255097097;
// UnityEngine.XR.iOS.Utils.serializableFaceGeometry
struct serializableFaceGeometry_t1893768467;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// UnityEngine.XR.iOS.Utils.SerializableVector4
struct SerializableVector4_t2739337940;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.XR.iOS.ARWorldMap
struct ARWorldMap_t2240790807;
// ARSessionManager
struct ARSessionManager_t3964960879;
// ARLocationSync
struct ARLocationSync_t2697562060;
// ARMapper
struct ARMapper_t696765295;
// UnityEngine.XR.iOS.ARResourceProperties
struct ARResourceProperties_t3627430478;
// UnityEngine.XR.iOS.ARResourceGroupInfo
struct ARResourceGroupInfo_t191861313;
// UnityEngine.XR.iOS.ARResourceGroupResource[]
struct ARResourceGroupResourceU5BU5D_t211583057;
// NetworkTransmitter/TransmissionData
struct TransmissionData_t2763532033;
// NetworkTransmitter
struct NetworkTransmitter_t3634351483;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.UnityARVideoFormat>
struct List_1_t3416529523;
// UnityEngine.XR.iOS.VideoFormatEnumerator
struct VideoFormatEnumerator_t3131638505;
// UnityEngine.XR.iOS.Utils.serializableARKitInit
struct serializableARKitInit_t3839227232;
// UnityEngine.XR.iOS.Utils.serializableUnityARLightData
struct serializableUnityARLightData_t3029229948;
// UnityEngine.XR.iOS.Utils.serializablePointCloud
struct serializablePointCloud_t4241265545;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.XR.iOS.Utils.serializableSHC
struct serializableSHC_t226029808;
// UnityEngine.XR.iOS.Utils.serializablePlaneGeometry
struct serializablePlaneGeometry_t3471745378;
// UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration
struct serializableARSessionConfiguration_t30565192;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// ARReferenceObjectAsset[]
struct ARReferenceObjectAssetU5BU5D_t3521236592;
// UnityEngine.Object
struct Object_t631007953;
// ARReferenceImage[]
struct ARReferenceImageU5BU5D_t715634648;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t3299519057;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker>
struct Dictionary_2_t100189446;
// UnityEngine.Networking.HostTopology
struct HostTopology_t4059263395;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>
struct Dictionary_2_t1959671187;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t4173981269;
// UnityEngine.Networking.GlobalConfig
struct GlobalConfig_t833512557;
// System.Collections.Generic.List`1<UnityEngine.Networking.QosType>
struct List_1_t743604312;
// UnityEngine.Networking.NetworkMigrationManager
struct NetworkMigrationManager_t2405847916;
// System.Net.EndPoint
struct EndPoint_t982345378;
// UnityEngine.Networking.NetworkClient
struct NetworkClient_t3758195968;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Networking.Match.MatchInfo
struct MatchInfo_t221301733;
// UnityEngine.Networking.Match.NetworkMatch
struct NetworkMatch_t2930480025;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>
struct List_1_t343529635;
// UnityEngine.Networking.NetworkSystem.AddPlayerMessage
struct AddPlayerMessage_t787692541;
// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage
struct RemovePlayerMessage_t1120190071;
// UnityEngine.Networking.NetworkSystem.ErrorMessage
struct ErrorMessage_t4257973676;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_t2705220091;
// ColorPicker
struct ColorPicker_t228004619;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t3272854023;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3929719369;
// UnityEngine.Material
struct Material_t340375123;
// ObjectScanSessionManager
struct ObjectScanSessionManager_t3985853727;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARReferenceObject>
struct List_1_t2537441456;
// UnityEngine.XR.iOS.PickBoundingBox
struct PickBoundingBox_t873364976;
// Collections.Hybrid.Generic.LinkedListDictionary`2<System.String,UnityEngine.GameObject>
struct LinkedListDictionary_2_t1717611866;
// UnityARCameraManager
struct UnityARCameraManager_t4002280589;
// UnityEngine.XR.iOS.serializableARWorldMap
struct serializableARWorldMap_t2514323794;
// ARReferenceObjectAsset
struct ARReferenceObjectAsset_t1156404477;
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t175708936;
// UnityEngine.Cubemap
struct Cubemap_t1972384409;
// ReflectionProbeGameObject
struct ReflectionProbeGameObject_t172576734;
// Collections.Hybrid.Generic.LinkedListDictionary`2<System.String,ReflectionProbeGameObject>
struct LinkedListDictionary_2_t776551981;
// UnityEngine.Transform
struct Transform_t3600365921;
// VideoFormatButton/VideoFormatButtonPressed
struct VideoFormatButtonPressed_t1187798507;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// ARReferenceImage
struct ARReferenceImage_t2463148469;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// UnityEngine.XR.iOS.ConnectToEditor
struct ConnectToEditor_t595742893;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3081694049;
// ARReferenceImagesSet
struct ARReferenceImagesSet_t4271437859;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t2380464200;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Image
struct Image_t2670269651;
// ColorChangedEvent
struct ColorChangedEvent_t3019780707;
// HSVChangedEvent
struct HSVChangedEvent_t911780251;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t245602842;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.Dictionary`2<System.Int32,NetworkTransmitter/TransmissionData>
struct Dictionary_2_t1652245364;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Byte[]>
struct UnityAction_2_t1861820427;
// PlayerScript
struct PlayerScript_t1783516946;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t439394298;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ARPLANEANCHORGAMEOBJECT_T1947719815_H
#define ARPLANEANCHORGAMEOBJECT_T1947719815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct  ARPlaneAnchorGameObject_t1947719815  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.ARPlaneAnchorGameObject::gameObject
	GameObject_t1113636619 * ___gameObject_0;
	// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.ARPlaneAnchorGameObject::planeAnchor
	ARPlaneAnchor_t2049372221 * ___planeAnchor_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t1947719815, ___gameObject_0)); }
	inline GameObject_t1113636619 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1113636619 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_planeAnchor_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t1947719815, ___planeAnchor_1)); }
	inline ARPlaneAnchor_t2049372221 * get_planeAnchor_1() const { return ___planeAnchor_1; }
	inline ARPlaneAnchor_t2049372221 ** get_address_of_planeAnchor_1() { return &___planeAnchor_1; }
	inline void set_planeAnchor_1(ARPlaneAnchor_t2049372221 * value)
	{
		___planeAnchor_1 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORGAMEOBJECT_T1947719815_H
#ifndef ARREFERENCEOBJECTRESOURCECONTENTS_T4233467830_H
#define ARREFERENCEOBJECTRESOURCECONTENTS_T4233467830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARReferenceObjectResourceContents
struct  ARReferenceObjectResourceContents_t4233467830  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.ARResourceFilename[] UnityEngine.XR.iOS.ARReferenceObjectResourceContents::objects
	ARResourceFilenameU5BU5D_t3417617624* ___objects_0;
	// UnityEngine.XR.iOS.ARResourceInfo UnityEngine.XR.iOS.ARReferenceObjectResourceContents::info
	ARResourceInfo_t1991666228 * ___info_1;
	// System.String UnityEngine.XR.iOS.ARReferenceObjectResourceContents::referenceObjectName
	String_t* ___referenceObjectName_2;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(ARReferenceObjectResourceContents_t4233467830, ___objects_0)); }
	inline ARResourceFilenameU5BU5D_t3417617624* get_objects_0() const { return ___objects_0; }
	inline ARResourceFilenameU5BU5D_t3417617624** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(ARResourceFilenameU5BU5D_t3417617624* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___objects_0), value);
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(ARReferenceObjectResourceContents_t4233467830, ___info_1)); }
	inline ARResourceInfo_t1991666228 * get_info_1() const { return ___info_1; }
	inline ARResourceInfo_t1991666228 ** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(ARResourceInfo_t1991666228 * value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}

	inline static int32_t get_offset_of_referenceObjectName_2() { return static_cast<int32_t>(offsetof(ARReferenceObjectResourceContents_t4233467830, ___referenceObjectName_2)); }
	inline String_t* get_referenceObjectName_2() const { return ___referenceObjectName_2; }
	inline String_t** get_address_of_referenceObjectName_2() { return &___referenceObjectName_2; }
	inline void set_referenceObjectName_2(String_t* value)
	{
		___referenceObjectName_2 = value;
		Il2CppCodeGenWriteBarrier((&___referenceObjectName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARREFERENCEOBJECTRESOURCECONTENTS_T4233467830_H
#ifndef SERIALIZABLEPOINTCLOUD_T4241265545_H
#define SERIALIZABLEPOINTCLOUD_T4241265545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializablePointCloud
struct  serializablePointCloud_t4241265545  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t4116647657* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t4241265545, ___pointCloudData_0)); }
	inline ByteU5BU5D_t4116647657* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t4116647657* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T4241265545_H
#ifndef SERIALIZABLEUNITYARFACEANCHOR_T1413500457_H
#define SERIALIZABLEUNITYARFACEANCHOR_T1413500457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableUnityARFaceAnchor
struct  serializableUnityARFaceAnchor_t1413500457  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4 UnityEngine.XR.iOS.Utils.serializableUnityARFaceAnchor::worldTransform
	serializableUnityARMatrix4x4_t255097097 * ___worldTransform_0;
	// UnityEngine.XR.iOS.Utils.serializableFaceGeometry UnityEngine.XR.iOS.Utils.serializableUnityARFaceAnchor::faceGeometry
	serializableFaceGeometry_t1893768467 * ___faceGeometry_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> UnityEngine.XR.iOS.Utils.serializableUnityARFaceAnchor::arBlendShapes
	Dictionary_2_t1182523073 * ___arBlendShapes_2;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableUnityARFaceAnchor::identifierStr
	ByteU5BU5D_t4116647657* ___identifierStr_3;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t1413500457, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t255097097 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t255097097 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t255097097 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_faceGeometry_1() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t1413500457, ___faceGeometry_1)); }
	inline serializableFaceGeometry_t1893768467 * get_faceGeometry_1() const { return ___faceGeometry_1; }
	inline serializableFaceGeometry_t1893768467 ** get_address_of_faceGeometry_1() { return &___faceGeometry_1; }
	inline void set_faceGeometry_1(serializableFaceGeometry_t1893768467 * value)
	{
		___faceGeometry_1 = value;
		Il2CppCodeGenWriteBarrier((&___faceGeometry_1), value);
	}

	inline static int32_t get_offset_of_arBlendShapes_2() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t1413500457, ___arBlendShapes_2)); }
	inline Dictionary_2_t1182523073 * get_arBlendShapes_2() const { return ___arBlendShapes_2; }
	inline Dictionary_2_t1182523073 ** get_address_of_arBlendShapes_2() { return &___arBlendShapes_2; }
	inline void set_arBlendShapes_2(Dictionary_2_t1182523073 * value)
	{
		___arBlendShapes_2 = value;
		Il2CppCodeGenWriteBarrier((&___arBlendShapes_2), value);
	}

	inline static int32_t get_offset_of_identifierStr_3() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t1413500457, ___identifierStr_3)); }
	inline ByteU5BU5D_t4116647657* get_identifierStr_3() const { return ___identifierStr_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_identifierStr_3() { return &___identifierStr_3; }
	inline void set_identifierStr_3(ByteU5BU5D_t4116647657* value)
	{
		___identifierStr_3 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARFACEANCHOR_T1413500457_H
#ifndef SERIALIZABLEFACEGEOMETRY_T1893768467_H
#define SERIALIZABLEFACEGEOMETRY_T1893768467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableFaceGeometry
struct  serializableFaceGeometry_t1893768467  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableFaceGeometry::vertices
	ByteU5BU5D_t4116647657* ___vertices_0;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableFaceGeometry::texCoords
	ByteU5BU5D_t4116647657* ___texCoords_1;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableFaceGeometry::triIndices
	ByteU5BU5D_t4116647657* ___triIndices_2;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t1893768467, ___vertices_0)); }
	inline ByteU5BU5D_t4116647657* get_vertices_0() const { return ___vertices_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(ByteU5BU5D_t4116647657* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_texCoords_1() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t1893768467, ___texCoords_1)); }
	inline ByteU5BU5D_t4116647657* get_texCoords_1() const { return ___texCoords_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_texCoords_1() { return &___texCoords_1; }
	inline void set_texCoords_1(ByteU5BU5D_t4116647657* value)
	{
		___texCoords_1 = value;
		Il2CppCodeGenWriteBarrier((&___texCoords_1), value);
	}

	inline static int32_t get_offset_of_triIndices_2() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t1893768467, ___triIndices_2)); }
	inline ByteU5BU5D_t4116647657* get_triIndices_2() const { return ___triIndices_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_triIndices_2() { return &___triIndices_2; }
	inline void set_triIndices_2(ByteU5BU5D_t4116647657* value)
	{
		___triIndices_2 = value;
		Il2CppCodeGenWriteBarrier((&___triIndices_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFACEGEOMETRY_T1893768467_H
#ifndef SERIALIZABLEPLANEGEOMETRY_T3471745378_H
#define SERIALIZABLEPLANEGEOMETRY_T3471745378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializablePlaneGeometry
struct  serializablePlaneGeometry_t3471745378  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializablePlaneGeometry::vertices
	ByteU5BU5D_t4116647657* ___vertices_0;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializablePlaneGeometry::texCoords
	ByteU5BU5D_t4116647657* ___texCoords_1;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializablePlaneGeometry::triIndices
	ByteU5BU5D_t4116647657* ___triIndices_2;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializablePlaneGeometry::boundaryVertices
	ByteU5BU5D_t4116647657* ___boundaryVertices_3;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(serializablePlaneGeometry_t3471745378, ___vertices_0)); }
	inline ByteU5BU5D_t4116647657* get_vertices_0() const { return ___vertices_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(ByteU5BU5D_t4116647657* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_texCoords_1() { return static_cast<int32_t>(offsetof(serializablePlaneGeometry_t3471745378, ___texCoords_1)); }
	inline ByteU5BU5D_t4116647657* get_texCoords_1() const { return ___texCoords_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_texCoords_1() { return &___texCoords_1; }
	inline void set_texCoords_1(ByteU5BU5D_t4116647657* value)
	{
		___texCoords_1 = value;
		Il2CppCodeGenWriteBarrier((&___texCoords_1), value);
	}

	inline static int32_t get_offset_of_triIndices_2() { return static_cast<int32_t>(offsetof(serializablePlaneGeometry_t3471745378, ___triIndices_2)); }
	inline ByteU5BU5D_t4116647657* get_triIndices_2() const { return ___triIndices_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_triIndices_2() { return &___triIndices_2; }
	inline void set_triIndices_2(ByteU5BU5D_t4116647657* value)
	{
		___triIndices_2 = value;
		Il2CppCodeGenWriteBarrier((&___triIndices_2), value);
	}

	inline static int32_t get_offset_of_boundaryVertices_3() { return static_cast<int32_t>(offsetof(serializablePlaneGeometry_t3471745378, ___boundaryVertices_3)); }
	inline ByteU5BU5D_t4116647657* get_boundaryVertices_3() const { return ___boundaryVertices_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_boundaryVertices_3() { return &___boundaryVertices_3; }
	inline void set_boundaryVertices_3(ByteU5BU5D_t4116647657* value)
	{
		___boundaryVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___boundaryVertices_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPLANEGEOMETRY_T3471745378_H
#ifndef HSVUTIL_T1472193074_H
#define HSVUTIL_T1472193074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVUtil
struct  HSVUtil_t1472193074  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T1472193074_H
#ifndef SERIALIZABLEUNITYARMATRIX4X4_T255097097_H
#define SERIALIZABLEUNITYARMATRIX4X4_T255097097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4
struct  serializableUnityARMatrix4x4_t255097097  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4::column0
	SerializableVector4_t2739337940 * ___column0_0;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4::column1
	SerializableVector4_t2739337940 * ___column1_1;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4::column2
	SerializableVector4_t2739337940 * ___column2_2;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4::column3
	SerializableVector4_t2739337940 * ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t255097097, ___column0_0)); }
	inline SerializableVector4_t2739337940 * get_column0_0() const { return ___column0_0; }
	inline SerializableVector4_t2739337940 ** get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(SerializableVector4_t2739337940 * value)
	{
		___column0_0 = value;
		Il2CppCodeGenWriteBarrier((&___column0_0), value);
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t255097097, ___column1_1)); }
	inline SerializableVector4_t2739337940 * get_column1_1() const { return ___column1_1; }
	inline SerializableVector4_t2739337940 ** get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(SerializableVector4_t2739337940 * value)
	{
		___column1_1 = value;
		Il2CppCodeGenWriteBarrier((&___column1_1), value);
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t255097097, ___column2_2)); }
	inline SerializableVector4_t2739337940 * get_column2_2() const { return ___column2_2; }
	inline SerializableVector4_t2739337940 ** get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(SerializableVector4_t2739337940 * value)
	{
		___column2_2 = value;
		Il2CppCodeGenWriteBarrier((&___column2_2), value);
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t255097097, ___column3_3)); }
	inline SerializableVector4_t2739337940 * get_column3_3() const { return ___column3_3; }
	inline SerializableVector4_t2739337940 ** get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(SerializableVector4_t2739337940 * value)
	{
		___column3_3 = value;
		Il2CppCodeGenWriteBarrier((&___column3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARMATRIX4X4_T255097097_H
#ifndef SERIALIZABLEVECTOR4_T2739337940_H
#define SERIALIZABLEVECTOR4_T2739337940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.SerializableVector4
struct  SerializableVector4_t2739337940  : public RuntimeObject
{
public:
	// System.Single UnityEngine.XR.iOS.Utils.SerializableVector4::x
	float ___x_0;
	// System.Single UnityEngine.XR.iOS.Utils.SerializableVector4::y
	float ___y_1;
	// System.Single UnityEngine.XR.iOS.Utils.SerializableVector4::z
	float ___z_2;
	// System.Single UnityEngine.XR.iOS.Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t2739337940, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t2739337940, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t2739337940, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t2739337940, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T2739337940_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T1997214439_H
#define OBJECTSERIALIZATIONEXTENSION_T1997214439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t1997214439  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T1997214439_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef ARRESOURCEGROUPINFO_T191861313_H
#define ARRESOURCEGROUPINFO_T191861313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceGroupInfo
struct  ARResourceGroupInfo_t191861313  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARResourceGroupInfo::version
	int32_t ___version_0;
	// System.String UnityEngine.XR.iOS.ARResourceGroupInfo::author
	String_t* ___author_1;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(ARResourceGroupInfo_t191861313, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}

	inline static int32_t get_offset_of_author_1() { return static_cast<int32_t>(offsetof(ARResourceGroupInfo_t191861313, ___author_1)); }
	inline String_t* get_author_1() const { return ___author_1; }
	inline String_t** get_address_of_author_1() { return &___author_1; }
	inline void set_author_1(String_t* value)
	{
		___author_1 = value;
		Il2CppCodeGenWriteBarrier((&___author_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEGROUPINFO_T191861313_H
#ifndef SERIALIZABLESHC_T226029808_H
#define SERIALIZABLESHC_T226029808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableSHC
struct  serializableSHC_t226029808  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableSHC::shcData
	ByteU5BU5D_t4116647657* ___shcData_0;

public:
	inline static int32_t get_offset_of_shcData_0() { return static_cast<int32_t>(offsetof(serializableSHC_t226029808, ___shcData_0)); }
	inline ByteU5BU5D_t4116647657* get_shcData_0() const { return ___shcData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_shcData_0() { return &___shcData_0; }
	inline void set_shcData_0(ByteU5BU5D_t4116647657* value)
	{
		___shcData_0 = value;
		Il2CppCodeGenWriteBarrier((&___shcData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLESHC_T226029808_H
#ifndef CONNECTIONMESSAGEIDS_T1387126779_H
#define CONNECTIONMESSAGEIDS_T1387126779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectionMessageIds
struct  ConnectionMessageIds_t1387126779  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMESSAGEIDS_T1387126779_H
#ifndef U3CRELOCATEU3EC__ITERATOR0_T661745925_H
#define U3CRELOCATEU3EC__ITERATOR0_T661745925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARLocationSync/<Relocate>c__Iterator0
struct  U3CRelocateU3Ec__Iterator0_t661745925  : public RuntimeObject
{
public:
	// System.Byte[] ARLocationSync/<Relocate>c__Iterator0::receivedBytes
	ByteU5BU5D_t4116647657* ___receivedBytes_0;
	// UnityEngine.XR.iOS.ARWorldMap ARLocationSync/<Relocate>c__Iterator0::<arWorldMap>__0
	ARWorldMap_t2240790807 * ___U3CarWorldMapU3E__0_1;
	// ARSessionManager ARLocationSync/<Relocate>c__Iterator0::<sm>__0
	ARSessionManager_t3964960879 * ___U3CsmU3E__0_2;
	// ARLocationSync ARLocationSync/<Relocate>c__Iterator0::$this
	ARLocationSync_t2697562060 * ___U24this_3;
	// System.Object ARLocationSync/<Relocate>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ARLocationSync/<Relocate>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ARLocationSync/<Relocate>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_receivedBytes_0() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___receivedBytes_0)); }
	inline ByteU5BU5D_t4116647657* get_receivedBytes_0() const { return ___receivedBytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_receivedBytes_0() { return &___receivedBytes_0; }
	inline void set_receivedBytes_0(ByteU5BU5D_t4116647657* value)
	{
		___receivedBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___receivedBytes_0), value);
	}

	inline static int32_t get_offset_of_U3CarWorldMapU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U3CarWorldMapU3E__0_1)); }
	inline ARWorldMap_t2240790807 * get_U3CarWorldMapU3E__0_1() const { return ___U3CarWorldMapU3E__0_1; }
	inline ARWorldMap_t2240790807 ** get_address_of_U3CarWorldMapU3E__0_1() { return &___U3CarWorldMapU3E__0_1; }
	inline void set_U3CarWorldMapU3E__0_1(ARWorldMap_t2240790807 * value)
	{
		___U3CarWorldMapU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CarWorldMapU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsmU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U3CsmU3E__0_2)); }
	inline ARSessionManager_t3964960879 * get_U3CsmU3E__0_2() const { return ___U3CsmU3E__0_2; }
	inline ARSessionManager_t3964960879 ** get_address_of_U3CsmU3E__0_2() { return &___U3CsmU3E__0_2; }
	inline void set_U3CsmU3E__0_2(ARSessionManager_t3964960879 * value)
	{
		___U3CsmU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsmU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U24this_3)); }
	inline ARLocationSync_t2697562060 * get_U24this_3() const { return ___U24this_3; }
	inline ARLocationSync_t2697562060 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ARLocationSync_t2697562060 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CRelocateU3Ec__Iterator0_t661745925, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOCATEU3EC__ITERATOR0_T661745925_H
#ifndef SUBMESSAGEIDS_T1008824323_H
#define SUBMESSAGEIDS_T1008824323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.SubMessageIds
struct  SubMessageIds_t1008824323  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMESSAGEIDS_T1008824323_H
#ifndef U3CMAPAREAU3EC__ITERATOR0_T980979684_H
#define U3CMAPAREAU3EC__ITERATOR0_T980979684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMapper/<MapArea>c__Iterator0
struct  U3CMapAreaU3Ec__Iterator0_t980979684  : public RuntimeObject
{
public:
	// ARMapper ARMapper/<MapArea>c__Iterator0::$this
	ARMapper_t696765295 * ___U24this_0;
	// System.Object ARMapper/<MapArea>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ARMapper/<MapArea>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ARMapper/<MapArea>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CMapAreaU3Ec__Iterator0_t980979684, ___U24this_0)); }
	inline ARMapper_t696765295 * get_U24this_0() const { return ___U24this_0; }
	inline ARMapper_t696765295 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ARMapper_t696765295 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CMapAreaU3Ec__Iterator0_t980979684, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CMapAreaU3Ec__Iterator0_t980979684, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CMapAreaU3Ec__Iterator0_t980979684, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAPAREAU3EC__ITERATOR0_T980979684_H
#ifndef ARRESOURCEFILENAME_T3614159029_H
#define ARRESOURCEFILENAME_T3614159029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceFilename
struct  ARResourceFilename_t3614159029  : public RuntimeObject
{
public:
	// System.String UnityEngine.XR.iOS.ARResourceFilename::idiom
	String_t* ___idiom_0;
	// System.String UnityEngine.XR.iOS.ARResourceFilename::filename
	String_t* ___filename_1;

public:
	inline static int32_t get_offset_of_idiom_0() { return static_cast<int32_t>(offsetof(ARResourceFilename_t3614159029, ___idiom_0)); }
	inline String_t* get_idiom_0() const { return ___idiom_0; }
	inline String_t** get_address_of_idiom_0() { return &___idiom_0; }
	inline void set_idiom_0(String_t* value)
	{
		___idiom_0 = value;
		Il2CppCodeGenWriteBarrier((&___idiom_0), value);
	}

	inline static int32_t get_offset_of_filename_1() { return static_cast<int32_t>(offsetof(ARResourceFilename_t3614159029, ___filename_1)); }
	inline String_t* get_filename_1() const { return ___filename_1; }
	inline String_t** get_address_of_filename_1() { return &___filename_1; }
	inline void set_filename_1(String_t* value)
	{
		___filename_1 = value;
		Il2CppCodeGenWriteBarrier((&___filename_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEFILENAME_T3614159029_H
#ifndef U3CMAPPINGCOROUTINEU3EC__ITERATOR0_T2201572394_H
#define U3CMAPPINGCOROUTINEU3EC__ITERATOR0_T2201572394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARSessionManager/<MappingCoroutine>c__Iterator0
struct  U3CMappingCoroutineU3Ec__Iterator0_t2201572394  : public RuntimeObject
{
public:
	// ARSessionManager ARSessionManager/<MappingCoroutine>c__Iterator0::$this
	ARSessionManager_t3964960879 * ___U24this_0;
	// System.Object ARSessionManager/<MappingCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ARSessionManager/<MappingCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ARSessionManager/<MappingCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CMappingCoroutineU3Ec__Iterator0_t2201572394, ___U24this_0)); }
	inline ARSessionManager_t3964960879 * get_U24this_0() const { return ___U24this_0; }
	inline ARSessionManager_t3964960879 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ARSessionManager_t3964960879 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CMappingCoroutineU3Ec__Iterator0_t2201572394, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CMappingCoroutineU3Ec__Iterator0_t2201572394, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CMappingCoroutineU3Ec__Iterator0_t2201572394, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAPPINGCOROUTINEU3EC__ITERATOR0_T2201572394_H
#ifndef ARRESOURCEPROPERTIES_T3627430478_H
#define ARRESOURCEPROPERTIES_T3627430478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceProperties
struct  ARResourceProperties_t3627430478  : public RuntimeObject
{
public:
	// System.Single UnityEngine.XR.iOS.ARResourceProperties::width
	float ___width_0;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARResourceProperties_t3627430478, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEPROPERTIES_T3627430478_H
#ifndef ARRESOURCECONTENTS_T645904403_H
#define ARRESOURCECONTENTS_T645904403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceContents
struct  ARResourceContents_t645904403  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.ARResourceFilename[] UnityEngine.XR.iOS.ARResourceContents::images
	ARResourceFilenameU5BU5D_t3417617624* ___images_0;
	// UnityEngine.XR.iOS.ARResourceInfo UnityEngine.XR.iOS.ARResourceContents::info
	ARResourceInfo_t1991666228 * ___info_1;
	// UnityEngine.XR.iOS.ARResourceProperties UnityEngine.XR.iOS.ARResourceContents::properties
	ARResourceProperties_t3627430478 * ___properties_2;

public:
	inline static int32_t get_offset_of_images_0() { return static_cast<int32_t>(offsetof(ARResourceContents_t645904403, ___images_0)); }
	inline ARResourceFilenameU5BU5D_t3417617624* get_images_0() const { return ___images_0; }
	inline ARResourceFilenameU5BU5D_t3417617624** get_address_of_images_0() { return &___images_0; }
	inline void set_images_0(ARResourceFilenameU5BU5D_t3417617624* value)
	{
		___images_0 = value;
		Il2CppCodeGenWriteBarrier((&___images_0), value);
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(ARResourceContents_t645904403, ___info_1)); }
	inline ARResourceInfo_t1991666228 * get_info_1() const { return ___info_1; }
	inline ARResourceInfo_t1991666228 ** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(ARResourceInfo_t1991666228 * value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ARResourceContents_t645904403, ___properties_2)); }
	inline ARResourceProperties_t3627430478 * get_properties_2() const { return ___properties_2; }
	inline ARResourceProperties_t3627430478 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(ARResourceProperties_t3627430478 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCECONTENTS_T645904403_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ARRESOURCEGROUPCONTENTS_T1795740205_H
#define ARRESOURCEGROUPCONTENTS_T1795740205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceGroupContents
struct  ARResourceGroupContents_t1795740205  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.ARResourceGroupInfo UnityEngine.XR.iOS.ARResourceGroupContents::info
	ARResourceGroupInfo_t191861313 * ___info_0;
	// UnityEngine.XR.iOS.ARResourceGroupResource[] UnityEngine.XR.iOS.ARResourceGroupContents::resources
	ARResourceGroupResourceU5BU5D_t211583057* ___resources_1;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(ARResourceGroupContents_t1795740205, ___info_0)); }
	inline ARResourceGroupInfo_t191861313 * get_info_0() const { return ___info_0; }
	inline ARResourceGroupInfo_t191861313 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(ARResourceGroupInfo_t191861313 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier((&___info_0), value);
	}

	inline static int32_t get_offset_of_resources_1() { return static_cast<int32_t>(offsetof(ARResourceGroupContents_t1795740205, ___resources_1)); }
	inline ARResourceGroupResourceU5BU5D_t211583057* get_resources_1() const { return ___resources_1; }
	inline ARResourceGroupResourceU5BU5D_t211583057** get_address_of_resources_1() { return &___resources_1; }
	inline void set_resources_1(ARResourceGroupResourceU5BU5D_t211583057* value)
	{
		___resources_1 = value;
		Il2CppCodeGenWriteBarrier((&___resources_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEGROUPCONTENTS_T1795740205_H
#ifndef TRANSMISSIONDATA_T2763532033_H
#define TRANSMISSIONDATA_T2763532033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkTransmitter/TransmissionData
struct  TransmissionData_t2763532033  : public RuntimeObject
{
public:
	// System.Int32 NetworkTransmitter/TransmissionData::curDataIndex
	int32_t ___curDataIndex_0;
	// System.Byte[] NetworkTransmitter/TransmissionData::data
	ByteU5BU5D_t4116647657* ___data_1;

public:
	inline static int32_t get_offset_of_curDataIndex_0() { return static_cast<int32_t>(offsetof(TransmissionData_t2763532033, ___curDataIndex_0)); }
	inline int32_t get_curDataIndex_0() const { return ___curDataIndex_0; }
	inline int32_t* get_address_of_curDataIndex_0() { return &___curDataIndex_0; }
	inline void set_curDataIndex_0(int32_t value)
	{
		___curDataIndex_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(TransmissionData_t2763532033, ___data_1)); }
	inline ByteU5BU5D_t4116647657* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t4116647657* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONDATA_T2763532033_H
#ifndef U3CSENDBYTESTOCLIENTSROUTINEU3EC__ITERATOR0_T487921339_H
#define U3CSENDBYTESTOCLIENTSROUTINEU3EC__ITERATOR0_T487921339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0
struct  U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339  : public RuntimeObject
{
public:
	// System.Int32 NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::transmissionId
	int32_t ___transmissionId_0;
	// System.Byte[] NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::data
	ByteU5BU5D_t4116647657* ___data_1;
	// NetworkTransmitter/TransmissionData NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::<dataToTransmit>__0
	TransmissionData_t2763532033 * ___U3CdataToTransmitU3E__0_2;
	// System.Int32 NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::<bufferSize>__0
	int32_t ___U3CbufferSizeU3E__0_3;
	// System.Int32 NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::<remaining>__1
	int32_t ___U3CremainingU3E__1_4;
	// System.Byte[] NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::<buffer>__1
	ByteU5BU5D_t4116647657* ___U3CbufferU3E__1_5;
	// NetworkTransmitter NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::$this
	NetworkTransmitter_t3634351483 * ___U24this_6;
	// System.Object NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 NetworkTransmitter/<SendBytesToClientsRoutine>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_transmissionId_0() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___transmissionId_0)); }
	inline int32_t get_transmissionId_0() const { return ___transmissionId_0; }
	inline int32_t* get_address_of_transmissionId_0() { return &___transmissionId_0; }
	inline void set_transmissionId_0(int32_t value)
	{
		___transmissionId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___data_1)); }
	inline ByteU5BU5D_t4116647657* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t4116647657* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_U3CdataToTransmitU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U3CdataToTransmitU3E__0_2)); }
	inline TransmissionData_t2763532033 * get_U3CdataToTransmitU3E__0_2() const { return ___U3CdataToTransmitU3E__0_2; }
	inline TransmissionData_t2763532033 ** get_address_of_U3CdataToTransmitU3E__0_2() { return &___U3CdataToTransmitU3E__0_2; }
	inline void set_U3CdataToTransmitU3E__0_2(TransmissionData_t2763532033 * value)
	{
		___U3CdataToTransmitU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataToTransmitU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CbufferSizeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U3CbufferSizeU3E__0_3)); }
	inline int32_t get_U3CbufferSizeU3E__0_3() const { return ___U3CbufferSizeU3E__0_3; }
	inline int32_t* get_address_of_U3CbufferSizeU3E__0_3() { return &___U3CbufferSizeU3E__0_3; }
	inline void set_U3CbufferSizeU3E__0_3(int32_t value)
	{
		___U3CbufferSizeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CremainingU3E__1_4() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U3CremainingU3E__1_4)); }
	inline int32_t get_U3CremainingU3E__1_4() const { return ___U3CremainingU3E__1_4; }
	inline int32_t* get_address_of_U3CremainingU3E__1_4() { return &___U3CremainingU3E__1_4; }
	inline void set_U3CremainingU3E__1_4(int32_t value)
	{
		___U3CremainingU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CbufferU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U3CbufferU3E__1_5)); }
	inline ByteU5BU5D_t4116647657* get_U3CbufferU3E__1_5() const { return ___U3CbufferU3E__1_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbufferU3E__1_5() { return &___U3CbufferU3E__1_5; }
	inline void set_U3CbufferU3E__1_5(ByteU5BU5D_t4116647657* value)
	{
		___U3CbufferU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U24this_6)); }
	inline NetworkTransmitter_t3634351483 * get_U24this_6() const { return ___U24this_6; }
	inline NetworkTransmitter_t3634351483 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(NetworkTransmitter_t3634351483 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDBYTESTOCLIENTSROUTINEU3EC__ITERATOR0_T487921339_H
#ifndef ARRESOURCEINFO_T1991666228_H
#define ARRESOURCEINFO_T1991666228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceInfo
struct  ARResourceInfo_t1991666228  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARResourceInfo::version
	int32_t ___version_0;
	// System.String UnityEngine.XR.iOS.ARResourceInfo::author
	String_t* ___author_1;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(ARResourceInfo_t1991666228, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}

	inline static int32_t get_offset_of_author_1() { return static_cast<int32_t>(offsetof(ARResourceInfo_t1991666228, ___author_1)); }
	inline String_t* get_author_1() const { return ___author_1; }
	inline String_t** get_address_of_author_1() { return &___author_1; }
	inline void set_author_1(String_t* value)
	{
		___author_1 = value;
		Il2CppCodeGenWriteBarrier((&___author_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEINFO_T1991666228_H
#ifndef ARRESOURCEGROUPRESOURCE_T1510500848_H
#define ARRESOURCEGROUPRESOURCE_T1510500848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARResourceGroupResource
struct  ARResourceGroupResource_t1510500848  : public RuntimeObject
{
public:
	// System.String UnityEngine.XR.iOS.ARResourceGroupResource::filename
	String_t* ___filename_0;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(ARResourceGroupResource_t1510500848, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRESOURCEGROUPRESOURCE_T1510500848_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef UNITYEVENT_3_T1697774568_H
#define UNITYEVENT_3_T1697774568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t1697774568  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1697774568, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1697774568_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef UNITYEVENT_2_T1827368229_H
#define UNITYEVENT_2_T1827368229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t1827368229  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1827368229, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1827368229_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef HSVCOLOR_T2280895388_H
#define HSVCOLOR_T2280895388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvColor
struct  HsvColor_t2280895388 
{
public:
	// System.Double HsvColor::H
	double ___H_0;
	// System.Double HsvColor::S
	double ___S_1;
	// System.Double HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T2280895388_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYVIDEOPARAMS_T4155354995_H
#define UNITYVIDEOPARAMS_T4155354995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityVideoParams
struct  UnityVideoParams_t4155354995 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yWidth
	int32_t ___yWidth_0;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yHeight
	int32_t ___yHeight_1;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::screenOrientation
	int32_t ___screenOrientation_2;
	// System.Single UnityEngine.XR.iOS.UnityVideoParams::texCoordScale
	float ___texCoordScale_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityVideoParams::cvPixelBufferPtr
	intptr_t ___cvPixelBufferPtr_4;

public:
	inline static int32_t get_offset_of_yWidth_0() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yWidth_0)); }
	inline int32_t get_yWidth_0() const { return ___yWidth_0; }
	inline int32_t* get_address_of_yWidth_0() { return &___yWidth_0; }
	inline void set_yWidth_0(int32_t value)
	{
		___yWidth_0 = value;
	}

	inline static int32_t get_offset_of_yHeight_1() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yHeight_1)); }
	inline int32_t get_yHeight_1() const { return ___yHeight_1; }
	inline int32_t* get_address_of_yHeight_1() { return &___yHeight_1; }
	inline void set_yHeight_1(int32_t value)
	{
		___yHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenOrientation_2() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___screenOrientation_2)); }
	inline int32_t get_screenOrientation_2() const { return ___screenOrientation_2; }
	inline int32_t* get_address_of_screenOrientation_2() { return &___screenOrientation_2; }
	inline void set_screenOrientation_2(int32_t value)
	{
		___screenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_texCoordScale_3() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___texCoordScale_3)); }
	inline float get_texCoordScale_3() const { return ___texCoordScale_3; }
	inline float* get_address_of_texCoordScale_3() { return &___texCoordScale_3; }
	inline void set_texCoordScale_3(float value)
	{
		___texCoordScale_3 = value;
	}

	inline static int32_t get_offset_of_cvPixelBufferPtr_4() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___cvPixelBufferPtr_4)); }
	inline intptr_t get_cvPixelBufferPtr_4() const { return ___cvPixelBufferPtr_4; }
	inline intptr_t* get_address_of_cvPixelBufferPtr_4() { return &___cvPixelBufferPtr_4; }
	inline void set_cvPixelBufferPtr_4(intptr_t value)
	{
		___cvPixelBufferPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVIDEOPARAMS_T4155354995_H
#ifndef LIGHTDATATYPE_T2323651587_H
#define LIGHTDATATYPE_T2323651587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.LightDataType
struct  LightDataType_t2323651587 
{
public:
	// System.Int32 UnityEngine.XR.iOS.LightDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightDataType_t2323651587, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTDATATYPE_T2323651587_H
#ifndef COLORVALUES_T1603089408_H
#define COLORVALUES_T1603089408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorValues
struct  ColorValues_t1603089408 
{
public:
	// System.Int32 ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t1603089408, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T1603089408_H
#ifndef COLORCHANGEDEVENT_T3019780707_H
#define COLORCHANGEDEVENT_T3019780707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t3019780707  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T3019780707_H
#ifndef HSVCHANGEDEVENT_T911780251_H
#define HSVCHANGEDEVENT_T911780251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t911780251  : public UnityEvent_3_t1697774568
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T911780251_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef UNITYARPLANEDETECTION_T1367733575_H
#define UNITYARPLANEDETECTION_T1367733575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t1367733575 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t1367733575, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T1367733575_H
#ifndef UNITYARALIGNMENT_T3792119710_H
#define UNITYARALIGNMENT_T3792119710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t3792119710 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t3792119710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T3792119710_H
#ifndef ARWORLDMAPPINGSTATUS_T1606355445_H
#define ARWORLDMAPPINGSTATUS_T1606355445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARWorldMappingStatus
struct  ARWorldMappingStatus_t1606355445 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARWorldMappingStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARWorldMappingStatus_t1606355445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPPINGSTATUS_T1606355445_H
#ifndef AXIS_T1354568546_H
#define AXIS_T1354568546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Axis
struct  Axis_t1354568546 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1354568546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1354568546_H
#ifndef ARTRACKINGSTATEREASON_T2348933773_H
#define ARTRACKINGSTATEREASON_T2348933773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2348933773 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2348933773, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2348933773_H
#ifndef BOXSLIDEREVENT_T439394298_H
#define BOXSLIDEREVENT_T439394298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t439394298  : public UnityEvent_2_t1827368229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T439394298_H
#ifndef ARPLANEANCHORALIGNMENT_T2311256121_H
#define ARPLANEANCHORALIGNMENT_T2311256121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t2311256121 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t2311256121, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T2311256121_H
#ifndef FOCUSSTATE_T138798281_H
#define FOCUSSTATE_T138798281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocusSquare/FocusState
struct  FocusState_t138798281 
{
public:
	// System.Int32 FocusSquare/FocusState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FocusState_t138798281, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSSTATE_T138798281_H
#ifndef UNITYARSESSIONRUNOPTION_T942967030_H
#define UNITYARSESSIONRUNOPTION_T942967030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t942967030 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t942967030, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T942967030_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef FILTERLEVEL_T3348888663_H
#define FILTERLEVEL_T3348888663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LogFilter/FilterLevel
struct  FilterLevel_t3348888663 
{
public:
	// System.Int32 UnityEngine.Networking.LogFilter/FilterLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterLevel_t3348888663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERLEVEL_T3348888663_H
#ifndef PLAYERSPAWNMETHOD_T923284173_H
#define PLAYERSPAWNMETHOD_T923284173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerSpawnMethod
struct  PlayerSpawnMethod_t923284173 
{
public:
	// System.Int32 UnityEngine.Networking.PlayerSpawnMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayerSpawnMethod_t923284173, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSPAWNMETHOD_T923284173_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ARTRACKINGSTATE_T3182235352_H
#define ARTRACKINGSTATE_T3182235352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3182235352 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3182235352, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3182235352_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef HITTYPE_T3206133659_H
#define HITTYPE_T3206133659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.PickBoundingBox/HitType
struct  HitType_t3206133659 
{
public:
	// System.Int32 UnityEngine.XR.iOS.PickBoundingBox/HitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HitType_t3206133659, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTYPE_T3206133659_H
#ifndef ARHITTESTRESULTTYPE_T475323638_H
#define ARHITTESTRESULTTYPE_T475323638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t475323638 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t475323638, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T475323638_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef UNITYARVIDEOFORMAT_T1944454781_H
#define UNITYARVIDEOFORMAT_T1944454781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideoFormat
struct  UnityARVideoFormat_t1944454781 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARVideoFormat::videoFormatPtr
	intptr_t ___videoFormatPtr_0;
	// System.Single UnityEngine.XR.iOS.UnityARVideoFormat::imageResolutionWidth
	float ___imageResolutionWidth_1;
	// System.Single UnityEngine.XR.iOS.UnityARVideoFormat::imageResolutionHeight
	float ___imageResolutionHeight_2;
	// System.Int32 UnityEngine.XR.iOS.UnityARVideoFormat::framesPerSecond
	int32_t ___framesPerSecond_3;

public:
	inline static int32_t get_offset_of_videoFormatPtr_0() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781, ___videoFormatPtr_0)); }
	inline intptr_t get_videoFormatPtr_0() const { return ___videoFormatPtr_0; }
	inline intptr_t* get_address_of_videoFormatPtr_0() { return &___videoFormatPtr_0; }
	inline void set_videoFormatPtr_0(intptr_t value)
	{
		___videoFormatPtr_0 = value;
	}

	inline static int32_t get_offset_of_imageResolutionWidth_1() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781, ___imageResolutionWidth_1)); }
	inline float get_imageResolutionWidth_1() const { return ___imageResolutionWidth_1; }
	inline float* get_address_of_imageResolutionWidth_1() { return &___imageResolutionWidth_1; }
	inline void set_imageResolutionWidth_1(float value)
	{
		___imageResolutionWidth_1 = value;
	}

	inline static int32_t get_offset_of_imageResolutionHeight_2() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781, ___imageResolutionHeight_2)); }
	inline float get_imageResolutionHeight_2() const { return ___imageResolutionHeight_2; }
	inline float* get_address_of_imageResolutionHeight_2() { return &___imageResolutionHeight_2; }
	inline void set_imageResolutionHeight_2(float value)
	{
		___imageResolutionHeight_2 = value;
	}

	inline static int32_t get_offset_of_framesPerSecond_3() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781, ___framesPerSecond_3)); }
	inline int32_t get_framesPerSecond_3() const { return ___framesPerSecond_3; }
	inline int32_t* get_address_of_framesPerSecond_3() { return &___framesPerSecond_3; }
	inline void set_framesPerSecond_3(int32_t value)
	{
		___framesPerSecond_3 = value;
	}
};

struct UnityARVideoFormat_t1944454781_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.iOS.UnityARVideoFormat> UnityEngine.XR.iOS.UnityARVideoFormat::videoFormatsList
	List_1_t3416529523 * ___videoFormatsList_4;
	// UnityEngine.XR.iOS.VideoFormatEnumerator UnityEngine.XR.iOS.UnityARVideoFormat::<>f__mg$cache0
	VideoFormatEnumerator_t3131638505 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_videoFormatsList_4() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781_StaticFields, ___videoFormatsList_4)); }
	inline List_1_t3416529523 * get_videoFormatsList_4() const { return ___videoFormatsList_4; }
	inline List_1_t3416529523 ** get_address_of_videoFormatsList_4() { return &___videoFormatsList_4; }
	inline void set_videoFormatsList_4(List_1_t3416529523 * value)
	{
		___videoFormatsList_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoFormatsList_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(UnityARVideoFormat_t1944454781_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline VideoFormatEnumerator_t3131638505 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline VideoFormatEnumerator_t3131638505 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(VideoFormatEnumerator_t3131638505 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEOFORMAT_T1944454781_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T2731251371_H
#define SERIALIZABLEFROMEDITORMESSAGE_T2731251371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t2731251371  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.XR.iOS.Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// UnityEngine.XR.iOS.Utils.serializableARKitInit UnityEngine.XR.iOS.Utils.serializableFromEditorMessage::arkitConfigMsg
	serializableARKitInit_t3839227232 * ___arkitConfigMsg_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2731251371, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_arkitConfigMsg_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t2731251371, ___arkitConfigMsg_1)); }
	inline serializableARKitInit_t3839227232 * get_arkitConfigMsg_1() const { return ___arkitConfigMsg_1; }
	inline serializableARKitInit_t3839227232 ** get_address_of_arkitConfigMsg_1() { return &___arkitConfigMsg_1; }
	inline void set_arkitConfigMsg_1(serializableARKitInit_t3839227232 * value)
	{
		___arkitConfigMsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___arkitConfigMsg_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T2731251371_H
#ifndef DIRECTION_T524882829_H
#define DIRECTION_T524882829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Direction
struct  Direction_t524882829 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t524882829, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T524882829_H
#ifndef SERIALIZABLEUNITYARCAMERA_T1848993995_H
#define SERIALIZABLEUNITYARCAMERA_T1848993995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableUnityARCamera
struct  serializableUnityARCamera_t1848993995  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4 UnityEngine.XR.iOS.Utils.serializableUnityARCamera::worldTransform
	serializableUnityARMatrix4x4_t255097097 * ___worldTransform_0;
	// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4 UnityEngine.XR.iOS.Utils.serializableUnityARCamera::projectionMatrix
	serializableUnityARMatrix4x4_t255097097 * ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.Utils.serializableUnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.Utils.serializableUnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.Utils.serializableUnityARCamera::videoParams
	UnityVideoParams_t4155354995  ___videoParams_4;
	// UnityEngine.XR.iOS.Utils.serializableUnityARLightData UnityEngine.XR.iOS.Utils.serializableUnityARCamera::lightData
	serializableUnityARLightData_t3029229948 * ___lightData_5;
	// UnityEngine.XR.iOS.Utils.serializablePointCloud UnityEngine.XR.iOS.Utils.serializableUnityARCamera::pointCloud
	serializablePointCloud_t4241265545 * ___pointCloud_6;
	// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4 UnityEngine.XR.iOS.Utils.serializableUnityARCamera::displayTransform
	serializableUnityARMatrix4x4_t255097097 * ___displayTransform_7;
	// UnityEngine.XR.iOS.ARWorldMappingStatus UnityEngine.XR.iOS.Utils.serializableUnityARCamera::worldMappingStatus
	int32_t ___worldMappingStatus_8;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t255097097 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t255097097 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t255097097 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___projectionMatrix_1)); }
	inline serializableUnityARMatrix4x4_t255097097 * get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline serializableUnityARMatrix4x4_t255097097 ** get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(serializableUnityARMatrix4x4_t255097097 * value)
	{
		___projectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___projectionMatrix_1), value);
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___videoParams_4)); }
	inline UnityVideoParams_t4155354995  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t4155354995 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t4155354995  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___lightData_5)); }
	inline serializableUnityARLightData_t3029229948 * get_lightData_5() const { return ___lightData_5; }
	inline serializableUnityARLightData_t3029229948 ** get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(serializableUnityARLightData_t3029229948 * value)
	{
		___lightData_5 = value;
		Il2CppCodeGenWriteBarrier((&___lightData_5), value);
	}

	inline static int32_t get_offset_of_pointCloud_6() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___pointCloud_6)); }
	inline serializablePointCloud_t4241265545 * get_pointCloud_6() const { return ___pointCloud_6; }
	inline serializablePointCloud_t4241265545 ** get_address_of_pointCloud_6() { return &___pointCloud_6; }
	inline void set_pointCloud_6(serializablePointCloud_t4241265545 * value)
	{
		___pointCloud_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloud_6), value);
	}

	inline static int32_t get_offset_of_displayTransform_7() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___displayTransform_7)); }
	inline serializableUnityARMatrix4x4_t255097097 * get_displayTransform_7() const { return ___displayTransform_7; }
	inline serializableUnityARMatrix4x4_t255097097 ** get_address_of_displayTransform_7() { return &___displayTransform_7; }
	inline void set_displayTransform_7(serializableUnityARMatrix4x4_t255097097 * value)
	{
		___displayTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___displayTransform_7), value);
	}

	inline static int32_t get_offset_of_worldMappingStatus_8() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t1848993995, ___worldMappingStatus_8)); }
	inline int32_t get_worldMappingStatus_8() const { return ___worldMappingStatus_8; }
	inline int32_t* get_address_of_worldMappingStatus_8() { return &___worldMappingStatus_8; }
	inline void set_worldMappingStatus_8(int32_t value)
	{
		___worldMappingStatus_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARCAMERA_T1848993995_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef SERIALIZABLEUNITYARLIGHTDATA_T3029229948_H
#define SERIALIZABLEUNITYARLIGHTDATA_T3029229948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableUnityARLightData
struct  serializableUnityARLightData_t3029229948  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.LightDataType UnityEngine.XR.iOS.Utils.serializableUnityARLightData::whichLight
	int32_t ___whichLight_0;
	// UnityEngine.XR.iOS.Utils.serializableSHC UnityEngine.XR.iOS.Utils.serializableUnityARLightData::lightSHC
	serializableSHC_t226029808 * ___lightSHC_1;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARLightData::primaryLightDirAndIntensity
	SerializableVector4_t2739337940 * ___primaryLightDirAndIntensity_2;
	// System.Single UnityEngine.XR.iOS.Utils.serializableUnityARLightData::ambientIntensity
	float ___ambientIntensity_3;
	// System.Single UnityEngine.XR.iOS.Utils.serializableUnityARLightData::ambientColorTemperature
	float ___ambientColorTemperature_4;

public:
	inline static int32_t get_offset_of_whichLight_0() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3029229948, ___whichLight_0)); }
	inline int32_t get_whichLight_0() const { return ___whichLight_0; }
	inline int32_t* get_address_of_whichLight_0() { return &___whichLight_0; }
	inline void set_whichLight_0(int32_t value)
	{
		___whichLight_0 = value;
	}

	inline static int32_t get_offset_of_lightSHC_1() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3029229948, ___lightSHC_1)); }
	inline serializableSHC_t226029808 * get_lightSHC_1() const { return ___lightSHC_1; }
	inline serializableSHC_t226029808 ** get_address_of_lightSHC_1() { return &___lightSHC_1; }
	inline void set_lightSHC_1(serializableSHC_t226029808 * value)
	{
		___lightSHC_1 = value;
		Il2CppCodeGenWriteBarrier((&___lightSHC_1), value);
	}

	inline static int32_t get_offset_of_primaryLightDirAndIntensity_2() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3029229948, ___primaryLightDirAndIntensity_2)); }
	inline SerializableVector4_t2739337940 * get_primaryLightDirAndIntensity_2() const { return ___primaryLightDirAndIntensity_2; }
	inline SerializableVector4_t2739337940 ** get_address_of_primaryLightDirAndIntensity_2() { return &___primaryLightDirAndIntensity_2; }
	inline void set_primaryLightDirAndIntensity_2(SerializableVector4_t2739337940 * value)
	{
		___primaryLightDirAndIntensity_2 = value;
		Il2CppCodeGenWriteBarrier((&___primaryLightDirAndIntensity_2), value);
	}

	inline static int32_t get_offset_of_ambientIntensity_3() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3029229948, ___ambientIntensity_3)); }
	inline float get_ambientIntensity_3() const { return ___ambientIntensity_3; }
	inline float* get_address_of_ambientIntensity_3() { return &___ambientIntensity_3; }
	inline void set_ambientIntensity_3(float value)
	{
		___ambientIntensity_3 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_4() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3029229948, ___ambientColorTemperature_4)); }
	inline float get_ambientColorTemperature_4() const { return ___ambientColorTemperature_4; }
	inline float* get_address_of_ambientColorTemperature_4() { return &___ambientColorTemperature_4; }
	inline void set_ambientColorTemperature_4(float value)
	{
		___ambientColorTemperature_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARLIGHTDATA_T3029229948_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SERIALIZABLEUNITYARPLANEANCHOR_T3965207599_H
#define SERIALIZABLEUNITYARPLANEANCHOR_T3965207599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor
struct  serializableUnityARPlaneAnchor_t3965207599  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.Utils.serializableUnityARMatrix4x4 UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::worldTransform
	serializableUnityARMatrix4x4_t255097097 * ___worldTransform_0;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::center
	SerializableVector4_t2739337940 * ___center_1;
	// UnityEngine.XR.iOS.Utils.SerializableVector4 UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::extent
	SerializableVector4_t2739337940 * ___extent_2;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::planeAlignment
	int64_t ___planeAlignment_3;
	// UnityEngine.XR.iOS.Utils.serializablePlaneGeometry UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::planeGeometry
	serializablePlaneGeometry_t3471745378 * ___planeGeometry_4;
	// System.Byte[] UnityEngine.XR.iOS.Utils.serializableUnityARPlaneAnchor::identifierStr
	ByteU5BU5D_t4116647657* ___identifierStr_5;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t255097097 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t255097097 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t255097097 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_center_1() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___center_1)); }
	inline SerializableVector4_t2739337940 * get_center_1() const { return ___center_1; }
	inline SerializableVector4_t2739337940 ** get_address_of_center_1() { return &___center_1; }
	inline void set_center_1(SerializableVector4_t2739337940 * value)
	{
		___center_1 = value;
		Il2CppCodeGenWriteBarrier((&___center_1), value);
	}

	inline static int32_t get_offset_of_extent_2() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___extent_2)); }
	inline SerializableVector4_t2739337940 * get_extent_2() const { return ___extent_2; }
	inline SerializableVector4_t2739337940 ** get_address_of_extent_2() { return &___extent_2; }
	inline void set_extent_2(SerializableVector4_t2739337940 * value)
	{
		___extent_2 = value;
		Il2CppCodeGenWriteBarrier((&___extent_2), value);
	}

	inline static int32_t get_offset_of_planeAlignment_3() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___planeAlignment_3)); }
	inline int64_t get_planeAlignment_3() const { return ___planeAlignment_3; }
	inline int64_t* get_address_of_planeAlignment_3() { return &___planeAlignment_3; }
	inline void set_planeAlignment_3(int64_t value)
	{
		___planeAlignment_3 = value;
	}

	inline static int32_t get_offset_of_planeGeometry_4() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___planeGeometry_4)); }
	inline serializablePlaneGeometry_t3471745378 * get_planeGeometry_4() const { return ___planeGeometry_4; }
	inline serializablePlaneGeometry_t3471745378 ** get_address_of_planeGeometry_4() { return &___planeGeometry_4; }
	inline void set_planeGeometry_4(serializablePlaneGeometry_t3471745378 * value)
	{
		___planeGeometry_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeGeometry_4), value);
	}

	inline static int32_t get_offset_of_identifierStr_5() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t3965207599, ___identifierStr_5)); }
	inline ByteU5BU5D_t4116647657* get_identifierStr_5() const { return ___identifierStr_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_identifierStr_5() { return &___identifierStr_5; }
	inline void set_identifierStr_5(ByteU5BU5D_t4116647657* value)
	{
		___identifierStr_5 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARPLANEANCHOR_T3965207599_H
#ifndef ARHITTESTRESULT_T1279023930_H
#define ARHITTESTRESULT_T1279023930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t1279023930 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t1817901843  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t1817901843  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___localTransform_2)); }
	inline Matrix4x4_t1817901843  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t1817901843 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t1817901843  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___worldTransform_3)); }
	inline Matrix4x4_t1817901843  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t1817901843 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t1817901843  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1279023930, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1279023930_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1817901843  ___localTransform_2;
	Matrix4x4_t1817901843  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1279023930_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1817901843  ___localTransform_2;
	Matrix4x4_t1817901843  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T1279023930_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SERIALIZABLEARKITINIT_T3839227232_H
#define SERIALIZABLEARKITINIT_T3839227232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableARKitInit
struct  serializableARKitInit_t3839227232  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration UnityEngine.XR.iOS.Utils.serializableARKitInit::config
	serializableARSessionConfiguration_t30565192 * ___config_0;
	// UnityEngine.XR.iOS.UnityARSessionRunOption UnityEngine.XR.iOS.Utils.serializableARKitInit::runOption
	int32_t ___runOption_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(serializableARKitInit_t3839227232, ___config_0)); }
	inline serializableARSessionConfiguration_t30565192 * get_config_0() const { return ___config_0; }
	inline serializableARSessionConfiguration_t30565192 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(serializableARSessionConfiguration_t30565192 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_runOption_1() { return static_cast<int32_t>(offsetof(serializableARKitInit_t3839227232, ___runOption_1)); }
	inline int32_t get_runOption_1() const { return ___runOption_1; }
	inline int32_t* get_address_of_runOption_1() { return &___runOption_1; }
	inline void set_runOption_1(int32_t value)
	{
		___runOption_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARKITINIT_T3839227232_H
#ifndef SERIALIZABLEARSESSIONCONFIGURATION_T30565192_H
#define SERIALIZABLEARSESSIONCONFIGURATION_T30565192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration
struct  serializableARSessionConfiguration_t30565192  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;
	// System.Boolean UnityEngine.XR.iOS.Utils.serializableARSessionConfiguration::enableAutoFocus
	bool ___enableAutoFocus_4;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t30565192, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t30565192, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t30565192, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t30565192, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}

	inline static int32_t get_offset_of_enableAutoFocus_4() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t30565192, ___enableAutoFocus_4)); }
	inline bool get_enableAutoFocus_4() const { return ___enableAutoFocus_4; }
	inline bool* get_address_of_enableAutoFocus_4() { return &___enableAutoFocus_4; }
	inline void set_enableAutoFocus_4(bool value)
	{
		___enableAutoFocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARSESSIONCONFIGURATION_T30565192_H
#ifndef VIDEOFORMATBUTTONPRESSED_T1187798507_H
#define VIDEOFORMATBUTTONPRESSED_T1187798507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoFormatButton/VideoFormatButtonPressed
struct  VideoFormatButtonPressed_t1187798507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOFORMATBUTTONPRESSED_T1187798507_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ARREFERENCEOBJECTSSETASSET_T1991292594_H
#define ARREFERENCEOBJECTSSETASSET_T1991292594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARReferenceObjectsSetAsset
struct  ARReferenceObjectsSetAsset_t1991292594  : public ScriptableObject_t2528358522
{
public:
	// System.String ARReferenceObjectsSetAsset::resourceGroupName
	String_t* ___resourceGroupName_2;
	// ARReferenceObjectAsset[] ARReferenceObjectsSetAsset::referenceObjectAssets
	ARReferenceObjectAssetU5BU5D_t3521236592* ___referenceObjectAssets_3;

public:
	inline static int32_t get_offset_of_resourceGroupName_2() { return static_cast<int32_t>(offsetof(ARReferenceObjectsSetAsset_t1991292594, ___resourceGroupName_2)); }
	inline String_t* get_resourceGroupName_2() const { return ___resourceGroupName_2; }
	inline String_t** get_address_of_resourceGroupName_2() { return &___resourceGroupName_2; }
	inline void set_resourceGroupName_2(String_t* value)
	{
		___resourceGroupName_2 = value;
		Il2CppCodeGenWriteBarrier((&___resourceGroupName_2), value);
	}

	inline static int32_t get_offset_of_referenceObjectAssets_3() { return static_cast<int32_t>(offsetof(ARReferenceObjectsSetAsset_t1991292594, ___referenceObjectAssets_3)); }
	inline ARReferenceObjectAssetU5BU5D_t3521236592* get_referenceObjectAssets_3() const { return ___referenceObjectAssets_3; }
	inline ARReferenceObjectAssetU5BU5D_t3521236592** get_address_of_referenceObjectAssets_3() { return &___referenceObjectAssets_3; }
	inline void set_referenceObjectAssets_3(ARReferenceObjectAssetU5BU5D_t3521236592* value)
	{
		___referenceObjectAssets_3 = value;
		Il2CppCodeGenWriteBarrier((&___referenceObjectAssets_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARREFERENCEOBJECTSSETASSET_T1991292594_H
#ifndef ARREFERENCEOBJECTASSET_T1156404477_H
#define ARREFERENCEOBJECTASSET_T1156404477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARReferenceObjectAsset
struct  ARReferenceObjectAsset_t1156404477  : public ScriptableObject_t2528358522
{
public:
	// System.String ARReferenceObjectAsset::objectName
	String_t* ___objectName_2;
	// UnityEngine.Object ARReferenceObjectAsset::referenceObject
	Object_t631007953 * ___referenceObject_3;

public:
	inline static int32_t get_offset_of_objectName_2() { return static_cast<int32_t>(offsetof(ARReferenceObjectAsset_t1156404477, ___objectName_2)); }
	inline String_t* get_objectName_2() const { return ___objectName_2; }
	inline String_t** get_address_of_objectName_2() { return &___objectName_2; }
	inline void set_objectName_2(String_t* value)
	{
		___objectName_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectName_2), value);
	}

	inline static int32_t get_offset_of_referenceObject_3() { return static_cast<int32_t>(offsetof(ARReferenceObjectAsset_t1156404477, ___referenceObject_3)); }
	inline Object_t631007953 * get_referenceObject_3() const { return ___referenceObject_3; }
	inline Object_t631007953 ** get_address_of_referenceObject_3() { return &___referenceObject_3; }
	inline void set_referenceObject_3(Object_t631007953 * value)
	{
		___referenceObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___referenceObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARREFERENCEOBJECTASSET_T1156404477_H
#ifndef ARREFERENCEIMAGESSET_T4271437859_H
#define ARREFERENCEIMAGESSET_T4271437859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARReferenceImagesSet
struct  ARReferenceImagesSet_t4271437859  : public ScriptableObject_t2528358522
{
public:
	// System.String ARReferenceImagesSet::resourceGroupName
	String_t* ___resourceGroupName_2;
	// ARReferenceImage[] ARReferenceImagesSet::referenceImages
	ARReferenceImageU5BU5D_t715634648* ___referenceImages_3;

public:
	inline static int32_t get_offset_of_resourceGroupName_2() { return static_cast<int32_t>(offsetof(ARReferenceImagesSet_t4271437859, ___resourceGroupName_2)); }
	inline String_t* get_resourceGroupName_2() const { return ___resourceGroupName_2; }
	inline String_t** get_address_of_resourceGroupName_2() { return &___resourceGroupName_2; }
	inline void set_resourceGroupName_2(String_t* value)
	{
		___resourceGroupName_2 = value;
		Il2CppCodeGenWriteBarrier((&___resourceGroupName_2), value);
	}

	inline static int32_t get_offset_of_referenceImages_3() { return static_cast<int32_t>(offsetof(ARReferenceImagesSet_t4271437859, ___referenceImages_3)); }
	inline ARReferenceImageU5BU5D_t715634648* get_referenceImages_3() const { return ___referenceImages_3; }
	inline ARReferenceImageU5BU5D_t715634648** get_address_of_referenceImages_3() { return &___referenceImages_3; }
	inline void set_referenceImages_3(ARReferenceImageU5BU5D_t715634648* value)
	{
		___referenceImages_3 = value;
		Il2CppCodeGenWriteBarrier((&___referenceImages_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARREFERENCEIMAGESSET_T4271437859_H
#ifndef ARREFERENCEIMAGE_T2463148469_H
#define ARREFERENCEIMAGE_T2463148469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARReferenceImage
struct  ARReferenceImage_t2463148469  : public ScriptableObject_t2528358522
{
public:
	// System.String ARReferenceImage::imageName
	String_t* ___imageName_2;
	// UnityEngine.Texture2D ARReferenceImage::imageTexture
	Texture2D_t3840446185 * ___imageTexture_3;
	// System.Single ARReferenceImage::physicalSize
	float ___physicalSize_4;

public:
	inline static int32_t get_offset_of_imageName_2() { return static_cast<int32_t>(offsetof(ARReferenceImage_t2463148469, ___imageName_2)); }
	inline String_t* get_imageName_2() const { return ___imageName_2; }
	inline String_t** get_address_of_imageName_2() { return &___imageName_2; }
	inline void set_imageName_2(String_t* value)
	{
		___imageName_2 = value;
		Il2CppCodeGenWriteBarrier((&___imageName_2), value);
	}

	inline static int32_t get_offset_of_imageTexture_3() { return static_cast<int32_t>(offsetof(ARReferenceImage_t2463148469, ___imageTexture_3)); }
	inline Texture2D_t3840446185 * get_imageTexture_3() const { return ___imageTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_imageTexture_3() { return &___imageTexture_3; }
	inline void set_imageTexture_3(Texture2D_t3840446185 * value)
	{
		___imageTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___imageTexture_3), value);
	}

	inline static int32_t get_offset_of_physicalSize_4() { return static_cast<int32_t>(offsetof(ARReferenceImage_t2463148469, ___physicalSize_4)); }
	inline float get_physicalSize_4() const { return ___physicalSize_4; }
	inline float* get_address_of_physicalSize_4() { return &___physicalSize_4; }
	inline void set_physicalSize_4(float value)
	{
		___physicalSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARREFERENCEIMAGE_T2463148469_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef DONTDESTROYONLOAD_T1456007215_H
#define DONTDESTROYONLOAD_T1456007215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_t1456007215  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T1456007215_H
#ifndef NETWORKBEHAVIOUR_T204670959_H
#define NETWORKBEHAVIOUR_T204670959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour
struct  NetworkBehaviour_t204670959  : public MonoBehaviour_t3962482529
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkBehaviour::m_SyncVarDirtyBits
	uint32_t ___m_SyncVarDirtyBits_2;
	// System.Single UnityEngine.Networking.NetworkBehaviour::m_LastSendTime
	float ___m_LastSendTime_3;
	// System.Boolean UnityEngine.Networking.NetworkBehaviour::m_SyncVarGuard
	bool ___m_SyncVarGuard_4;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.NetworkBehaviour::m_MyView
	NetworkIdentity_t3299519057 * ___m_MyView_6;

public:
	inline static int32_t get_offset_of_m_SyncVarDirtyBits_2() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_SyncVarDirtyBits_2)); }
	inline uint32_t get_m_SyncVarDirtyBits_2() const { return ___m_SyncVarDirtyBits_2; }
	inline uint32_t* get_address_of_m_SyncVarDirtyBits_2() { return &___m_SyncVarDirtyBits_2; }
	inline void set_m_SyncVarDirtyBits_2(uint32_t value)
	{
		___m_SyncVarDirtyBits_2 = value;
	}

	inline static int32_t get_offset_of_m_LastSendTime_3() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_LastSendTime_3)); }
	inline float get_m_LastSendTime_3() const { return ___m_LastSendTime_3; }
	inline float* get_address_of_m_LastSendTime_3() { return &___m_LastSendTime_3; }
	inline void set_m_LastSendTime_3(float value)
	{
		___m_LastSendTime_3 = value;
	}

	inline static int32_t get_offset_of_m_SyncVarGuard_4() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_SyncVarGuard_4)); }
	inline bool get_m_SyncVarGuard_4() const { return ___m_SyncVarGuard_4; }
	inline bool* get_address_of_m_SyncVarGuard_4() { return &___m_SyncVarGuard_4; }
	inline void set_m_SyncVarGuard_4(bool value)
	{
		___m_SyncVarGuard_4 = value;
	}

	inline static int32_t get_offset_of_m_MyView_6() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_MyView_6)); }
	inline NetworkIdentity_t3299519057 * get_m_MyView_6() const { return ___m_MyView_6; }
	inline NetworkIdentity_t3299519057 ** get_address_of_m_MyView_6() { return &___m_MyView_6; }
	inline void set_m_MyView_6(NetworkIdentity_t3299519057 * value)
	{
		___m_MyView_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MyView_6), value);
	}
};

struct NetworkBehaviour_t204670959_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker> UnityEngine.Networking.NetworkBehaviour::s_CmdHandlerDelegates
	Dictionary_2_t100189446 * ___s_CmdHandlerDelegates_7;

public:
	inline static int32_t get_offset_of_s_CmdHandlerDelegates_7() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959_StaticFields, ___s_CmdHandlerDelegates_7)); }
	inline Dictionary_2_t100189446 * get_s_CmdHandlerDelegates_7() const { return ___s_CmdHandlerDelegates_7; }
	inline Dictionary_2_t100189446 ** get_address_of_s_CmdHandlerDelegates_7() { return &___s_CmdHandlerDelegates_7; }
	inline void set_s_CmdHandlerDelegates_7(Dictionary_2_t100189446 * value)
	{
		___s_CmdHandlerDelegates_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CmdHandlerDelegates_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKBEHAVIOUR_T204670959_H
#ifndef NETWORKDISCOVERY_T986186286_H
#define NETWORKDISCOVERY_T986186286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkDiscovery
struct  NetworkDiscovery_t986186286  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastPort
	int32_t ___m_BroadcastPort_3;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastKey
	int32_t ___m_BroadcastKey_4;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastVersion
	int32_t ___m_BroadcastVersion_5;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastSubVersion
	int32_t ___m_BroadcastSubVersion_6;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastInterval
	int32_t ___m_BroadcastInterval_7;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_UseNetworkManager
	bool ___m_UseNetworkManager_8;
	// System.String UnityEngine.Networking.NetworkDiscovery::m_BroadcastData
	String_t* ___m_BroadcastData_9;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_ShowGUI
	bool ___m_ShowGUI_10;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetX
	int32_t ___m_OffsetX_11;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetY
	int32_t ___m_OffsetY_12;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_HostId
	int32_t ___m_HostId_13;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_Running
	bool ___m_Running_14;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsServer
	bool ___m_IsServer_15;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsClient
	bool ___m_IsClient_16;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgOutBuffer
	ByteU5BU5D_t4116647657* ___m_MsgOutBuffer_17;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgInBuffer
	ByteU5BU5D_t4116647657* ___m_MsgInBuffer_18;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkDiscovery::m_DefaultTopology
	HostTopology_t4059263395 * ___m_DefaultTopology_19;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult> UnityEngine.Networking.NetworkDiscovery::m_BroadcastsReceived
	Dictionary_2_t1959671187 * ___m_BroadcastsReceived_20;

public:
	inline static int32_t get_offset_of_m_BroadcastPort_3() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastPort_3)); }
	inline int32_t get_m_BroadcastPort_3() const { return ___m_BroadcastPort_3; }
	inline int32_t* get_address_of_m_BroadcastPort_3() { return &___m_BroadcastPort_3; }
	inline void set_m_BroadcastPort_3(int32_t value)
	{
		___m_BroadcastPort_3 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastKey_4() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastKey_4)); }
	inline int32_t get_m_BroadcastKey_4() const { return ___m_BroadcastKey_4; }
	inline int32_t* get_address_of_m_BroadcastKey_4() { return &___m_BroadcastKey_4; }
	inline void set_m_BroadcastKey_4(int32_t value)
	{
		___m_BroadcastKey_4 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastVersion_5() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastVersion_5)); }
	inline int32_t get_m_BroadcastVersion_5() const { return ___m_BroadcastVersion_5; }
	inline int32_t* get_address_of_m_BroadcastVersion_5() { return &___m_BroadcastVersion_5; }
	inline void set_m_BroadcastVersion_5(int32_t value)
	{
		___m_BroadcastVersion_5 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastSubVersion_6() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastSubVersion_6)); }
	inline int32_t get_m_BroadcastSubVersion_6() const { return ___m_BroadcastSubVersion_6; }
	inline int32_t* get_address_of_m_BroadcastSubVersion_6() { return &___m_BroadcastSubVersion_6; }
	inline void set_m_BroadcastSubVersion_6(int32_t value)
	{
		___m_BroadcastSubVersion_6 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastInterval_7() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastInterval_7)); }
	inline int32_t get_m_BroadcastInterval_7() const { return ___m_BroadcastInterval_7; }
	inline int32_t* get_address_of_m_BroadcastInterval_7() { return &___m_BroadcastInterval_7; }
	inline void set_m_BroadcastInterval_7(int32_t value)
	{
		___m_BroadcastInterval_7 = value;
	}

	inline static int32_t get_offset_of_m_UseNetworkManager_8() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_UseNetworkManager_8)); }
	inline bool get_m_UseNetworkManager_8() const { return ___m_UseNetworkManager_8; }
	inline bool* get_address_of_m_UseNetworkManager_8() { return &___m_UseNetworkManager_8; }
	inline void set_m_UseNetworkManager_8(bool value)
	{
		___m_UseNetworkManager_8 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastData_9() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastData_9)); }
	inline String_t* get_m_BroadcastData_9() const { return ___m_BroadcastData_9; }
	inline String_t** get_address_of_m_BroadcastData_9() { return &___m_BroadcastData_9; }
	inline void set_m_BroadcastData_9(String_t* value)
	{
		___m_BroadcastData_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastData_9), value);
	}

	inline static int32_t get_offset_of_m_ShowGUI_10() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_ShowGUI_10)); }
	inline bool get_m_ShowGUI_10() const { return ___m_ShowGUI_10; }
	inline bool* get_address_of_m_ShowGUI_10() { return &___m_ShowGUI_10; }
	inline void set_m_ShowGUI_10(bool value)
	{
		___m_ShowGUI_10 = value;
	}

	inline static int32_t get_offset_of_m_OffsetX_11() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_OffsetX_11)); }
	inline int32_t get_m_OffsetX_11() const { return ___m_OffsetX_11; }
	inline int32_t* get_address_of_m_OffsetX_11() { return &___m_OffsetX_11; }
	inline void set_m_OffsetX_11(int32_t value)
	{
		___m_OffsetX_11 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_12() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_OffsetY_12)); }
	inline int32_t get_m_OffsetY_12() const { return ___m_OffsetY_12; }
	inline int32_t* get_address_of_m_OffsetY_12() { return &___m_OffsetY_12; }
	inline void set_m_OffsetY_12(int32_t value)
	{
		___m_OffsetY_12 = value;
	}

	inline static int32_t get_offset_of_m_HostId_13() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_HostId_13)); }
	inline int32_t get_m_HostId_13() const { return ___m_HostId_13; }
	inline int32_t* get_address_of_m_HostId_13() { return &___m_HostId_13; }
	inline void set_m_HostId_13(int32_t value)
	{
		___m_HostId_13 = value;
	}

	inline static int32_t get_offset_of_m_Running_14() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_Running_14)); }
	inline bool get_m_Running_14() const { return ___m_Running_14; }
	inline bool* get_address_of_m_Running_14() { return &___m_Running_14; }
	inline void set_m_Running_14(bool value)
	{
		___m_Running_14 = value;
	}

	inline static int32_t get_offset_of_m_IsServer_15() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_IsServer_15)); }
	inline bool get_m_IsServer_15() const { return ___m_IsServer_15; }
	inline bool* get_address_of_m_IsServer_15() { return &___m_IsServer_15; }
	inline void set_m_IsServer_15(bool value)
	{
		___m_IsServer_15 = value;
	}

	inline static int32_t get_offset_of_m_IsClient_16() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_IsClient_16)); }
	inline bool get_m_IsClient_16() const { return ___m_IsClient_16; }
	inline bool* get_address_of_m_IsClient_16() { return &___m_IsClient_16; }
	inline void set_m_IsClient_16(bool value)
	{
		___m_IsClient_16 = value;
	}

	inline static int32_t get_offset_of_m_MsgOutBuffer_17() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_MsgOutBuffer_17)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgOutBuffer_17() const { return ___m_MsgOutBuffer_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgOutBuffer_17() { return &___m_MsgOutBuffer_17; }
	inline void set_m_MsgOutBuffer_17(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgOutBuffer_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgOutBuffer_17), value);
	}

	inline static int32_t get_offset_of_m_MsgInBuffer_18() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_MsgInBuffer_18)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgInBuffer_18() const { return ___m_MsgInBuffer_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgInBuffer_18() { return &___m_MsgInBuffer_18; }
	inline void set_m_MsgInBuffer_18(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgInBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgInBuffer_18), value);
	}

	inline static int32_t get_offset_of_m_DefaultTopology_19() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_DefaultTopology_19)); }
	inline HostTopology_t4059263395 * get_m_DefaultTopology_19() const { return ___m_DefaultTopology_19; }
	inline HostTopology_t4059263395 ** get_address_of_m_DefaultTopology_19() { return &___m_DefaultTopology_19; }
	inline void set_m_DefaultTopology_19(HostTopology_t4059263395 * value)
	{
		___m_DefaultTopology_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultTopology_19), value);
	}

	inline static int32_t get_offset_of_m_BroadcastsReceived_20() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastsReceived_20)); }
	inline Dictionary_2_t1959671187 * get_m_BroadcastsReceived_20() const { return ___m_BroadcastsReceived_20; }
	inline Dictionary_2_t1959671187 ** get_address_of_m_BroadcastsReceived_20() { return &___m_BroadcastsReceived_20; }
	inline void set_m_BroadcastsReceived_20(Dictionary_2_t1959671187 * value)
	{
		___m_BroadcastsReceived_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastsReceived_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKDISCOVERY_T986186286_H
#ifndef NETWORKMANAGER_T468665779_H
#define NETWORKMANAGER_T468665779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkManager
struct  NetworkManager_t468665779  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.Networking.NetworkManager::m_NetworkPort
	int32_t ___m_NetworkPort_2;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ServerBindToIP
	bool ___m_ServerBindToIP_3;
	// System.String UnityEngine.Networking.NetworkManager::m_ServerBindAddress
	String_t* ___m_ServerBindAddress_4;
	// System.String UnityEngine.Networking.NetworkManager::m_NetworkAddress
	String_t* ___m_NetworkAddress_5;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_DontDestroyOnLoad
	bool ___m_DontDestroyOnLoad_6;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_RunInBackground
	bool ___m_RunInBackground_7;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ScriptCRCCheck
	bool ___m_ScriptCRCCheck_8;
	// System.Single UnityEngine.Networking.NetworkManager::m_MaxDelay
	float ___m_MaxDelay_9;
	// UnityEngine.Networking.LogFilter/FilterLevel UnityEngine.Networking.NetworkManager::m_LogLevel
	int32_t ___m_LogLevel_10;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkManager::m_PlayerPrefab
	GameObject_t1113636619 * ___m_PlayerPrefab_11;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AutoCreatePlayer
	bool ___m_AutoCreatePlayer_12;
	// UnityEngine.Networking.PlayerSpawnMethod UnityEngine.Networking.NetworkManager::m_PlayerSpawnMethod
	int32_t ___m_PlayerSpawnMethod_13;
	// System.String UnityEngine.Networking.NetworkManager::m_OfflineScene
	String_t* ___m_OfflineScene_14;
	// System.String UnityEngine.Networking.NetworkManager::m_OnlineScene
	String_t* ___m_OnlineScene_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.Networking.NetworkManager::m_SpawnPrefabs
	List_1_t2585711361 * ___m_SpawnPrefabs_16;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_CustomConfig
	bool ___m_CustomConfig_17;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxConnections
	int32_t ___m_MaxConnections_18;
	// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.NetworkManager::m_ConnectionConfig
	ConnectionConfig_t4173981269 * ___m_ConnectionConfig_19;
	// UnityEngine.Networking.GlobalConfig UnityEngine.Networking.NetworkManager::m_GlobalConfig
	GlobalConfig_t833512557 * ___m_GlobalConfig_20;
	// System.Collections.Generic.List`1<UnityEngine.Networking.QosType> UnityEngine.Networking.NetworkManager::m_Channels
	List_1_t743604312 * ___m_Channels_21;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseWebSockets
	bool ___m_UseWebSockets_22;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseSimulator
	bool ___m_UseSimulator_23;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_24;
	// System.Single UnityEngine.Networking.NetworkManager::m_PacketLossPercentage
	float ___m_PacketLossPercentage_25;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxBufferedPackets
	int32_t ___m_MaxBufferedPackets_26;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AllowFragmentation
	bool ___m_AllowFragmentation_27;
	// System.String UnityEngine.Networking.NetworkManager::m_MatchHost
	String_t* ___m_MatchHost_28;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MatchPort
	int32_t ___m_MatchPort_29;
	// System.String UnityEngine.Networking.NetworkManager::matchName
	String_t* ___matchName_30;
	// System.UInt32 UnityEngine.Networking.NetworkManager::matchSize
	uint32_t ___matchSize_31;
	// UnityEngine.Networking.NetworkMigrationManager UnityEngine.Networking.NetworkManager::m_MigrationManager
	NetworkMigrationManager_t2405847916 * ___m_MigrationManager_32;
	// System.Net.EndPoint UnityEngine.Networking.NetworkManager::m_EndPoint
	EndPoint_t982345378 * ___m_EndPoint_33;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ClientLoadedScene
	bool ___m_ClientLoadedScene_34;
	// System.Boolean UnityEngine.Networking.NetworkManager::isNetworkActive
	bool ___isNetworkActive_36;
	// UnityEngine.Networking.NetworkClient UnityEngine.Networking.NetworkManager::client
	NetworkClient_t3758195968 * ___client_37;
	// UnityEngine.Networking.Match.MatchInfo UnityEngine.Networking.NetworkManager::matchInfo
	MatchInfo_t221301733 * ___matchInfo_40;
	// UnityEngine.Networking.Match.NetworkMatch UnityEngine.Networking.NetworkManager::matchMaker
	NetworkMatch_t2930480025 * ___matchMaker_41;
	// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot> UnityEngine.Networking.NetworkManager::matches
	List_1_t343529635 * ___matches_42;

public:
	inline static int32_t get_offset_of_m_NetworkPort_2() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_NetworkPort_2)); }
	inline int32_t get_m_NetworkPort_2() const { return ___m_NetworkPort_2; }
	inline int32_t* get_address_of_m_NetworkPort_2() { return &___m_NetworkPort_2; }
	inline void set_m_NetworkPort_2(int32_t value)
	{
		___m_NetworkPort_2 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindToIP_3() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_ServerBindToIP_3)); }
	inline bool get_m_ServerBindToIP_3() const { return ___m_ServerBindToIP_3; }
	inline bool* get_address_of_m_ServerBindToIP_3() { return &___m_ServerBindToIP_3; }
	inline void set_m_ServerBindToIP_3(bool value)
	{
		___m_ServerBindToIP_3 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindAddress_4() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_ServerBindAddress_4)); }
	inline String_t* get_m_ServerBindAddress_4() const { return ___m_ServerBindAddress_4; }
	inline String_t** get_address_of_m_ServerBindAddress_4() { return &___m_ServerBindAddress_4; }
	inline void set_m_ServerBindAddress_4(String_t* value)
	{
		___m_ServerBindAddress_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerBindAddress_4), value);
	}

	inline static int32_t get_offset_of_m_NetworkAddress_5() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_NetworkAddress_5)); }
	inline String_t* get_m_NetworkAddress_5() const { return ___m_NetworkAddress_5; }
	inline String_t** get_address_of_m_NetworkAddress_5() { return &___m_NetworkAddress_5; }
	inline void set_m_NetworkAddress_5(String_t* value)
	{
		___m_NetworkAddress_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkAddress_5), value);
	}

	inline static int32_t get_offset_of_m_DontDestroyOnLoad_6() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_DontDestroyOnLoad_6)); }
	inline bool get_m_DontDestroyOnLoad_6() const { return ___m_DontDestroyOnLoad_6; }
	inline bool* get_address_of_m_DontDestroyOnLoad_6() { return &___m_DontDestroyOnLoad_6; }
	inline void set_m_DontDestroyOnLoad_6(bool value)
	{
		___m_DontDestroyOnLoad_6 = value;
	}

	inline static int32_t get_offset_of_m_RunInBackground_7() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_RunInBackground_7)); }
	inline bool get_m_RunInBackground_7() const { return ___m_RunInBackground_7; }
	inline bool* get_address_of_m_RunInBackground_7() { return &___m_RunInBackground_7; }
	inline void set_m_RunInBackground_7(bool value)
	{
		___m_RunInBackground_7 = value;
	}

	inline static int32_t get_offset_of_m_ScriptCRCCheck_8() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_ScriptCRCCheck_8)); }
	inline bool get_m_ScriptCRCCheck_8() const { return ___m_ScriptCRCCheck_8; }
	inline bool* get_address_of_m_ScriptCRCCheck_8() { return &___m_ScriptCRCCheck_8; }
	inline void set_m_ScriptCRCCheck_8(bool value)
	{
		___m_ScriptCRCCheck_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxDelay_9() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MaxDelay_9)); }
	inline float get_m_MaxDelay_9() const { return ___m_MaxDelay_9; }
	inline float* get_address_of_m_MaxDelay_9() { return &___m_MaxDelay_9; }
	inline void set_m_MaxDelay_9(float value)
	{
		___m_MaxDelay_9 = value;
	}

	inline static int32_t get_offset_of_m_LogLevel_10() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_LogLevel_10)); }
	inline int32_t get_m_LogLevel_10() const { return ___m_LogLevel_10; }
	inline int32_t* get_address_of_m_LogLevel_10() { return &___m_LogLevel_10; }
	inline void set_m_LogLevel_10(int32_t value)
	{
		___m_LogLevel_10 = value;
	}

	inline static int32_t get_offset_of_m_PlayerPrefab_11() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_PlayerPrefab_11)); }
	inline GameObject_t1113636619 * get_m_PlayerPrefab_11() const { return ___m_PlayerPrefab_11; }
	inline GameObject_t1113636619 ** get_address_of_m_PlayerPrefab_11() { return &___m_PlayerPrefab_11; }
	inline void set_m_PlayerPrefab_11(GameObject_t1113636619 * value)
	{
		___m_PlayerPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerPrefab_11), value);
	}

	inline static int32_t get_offset_of_m_AutoCreatePlayer_12() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_AutoCreatePlayer_12)); }
	inline bool get_m_AutoCreatePlayer_12() const { return ___m_AutoCreatePlayer_12; }
	inline bool* get_address_of_m_AutoCreatePlayer_12() { return &___m_AutoCreatePlayer_12; }
	inline void set_m_AutoCreatePlayer_12(bool value)
	{
		___m_AutoCreatePlayer_12 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSpawnMethod_13() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_PlayerSpawnMethod_13)); }
	inline int32_t get_m_PlayerSpawnMethod_13() const { return ___m_PlayerSpawnMethod_13; }
	inline int32_t* get_address_of_m_PlayerSpawnMethod_13() { return &___m_PlayerSpawnMethod_13; }
	inline void set_m_PlayerSpawnMethod_13(int32_t value)
	{
		___m_PlayerSpawnMethod_13 = value;
	}

	inline static int32_t get_offset_of_m_OfflineScene_14() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_OfflineScene_14)); }
	inline String_t* get_m_OfflineScene_14() const { return ___m_OfflineScene_14; }
	inline String_t** get_address_of_m_OfflineScene_14() { return &___m_OfflineScene_14; }
	inline void set_m_OfflineScene_14(String_t* value)
	{
		___m_OfflineScene_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OfflineScene_14), value);
	}

	inline static int32_t get_offset_of_m_OnlineScene_15() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_OnlineScene_15)); }
	inline String_t* get_m_OnlineScene_15() const { return ___m_OnlineScene_15; }
	inline String_t** get_address_of_m_OnlineScene_15() { return &___m_OnlineScene_15; }
	inline void set_m_OnlineScene_15(String_t* value)
	{
		___m_OnlineScene_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnlineScene_15), value);
	}

	inline static int32_t get_offset_of_m_SpawnPrefabs_16() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_SpawnPrefabs_16)); }
	inline List_1_t2585711361 * get_m_SpawnPrefabs_16() const { return ___m_SpawnPrefabs_16; }
	inline List_1_t2585711361 ** get_address_of_m_SpawnPrefabs_16() { return &___m_SpawnPrefabs_16; }
	inline void set_m_SpawnPrefabs_16(List_1_t2585711361 * value)
	{
		___m_SpawnPrefabs_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpawnPrefabs_16), value);
	}

	inline static int32_t get_offset_of_m_CustomConfig_17() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_CustomConfig_17)); }
	inline bool get_m_CustomConfig_17() const { return ___m_CustomConfig_17; }
	inline bool* get_address_of_m_CustomConfig_17() { return &___m_CustomConfig_17; }
	inline void set_m_CustomConfig_17(bool value)
	{
		___m_CustomConfig_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxConnections_18() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MaxConnections_18)); }
	inline int32_t get_m_MaxConnections_18() const { return ___m_MaxConnections_18; }
	inline int32_t* get_address_of_m_MaxConnections_18() { return &___m_MaxConnections_18; }
	inline void set_m_MaxConnections_18(int32_t value)
	{
		___m_MaxConnections_18 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionConfig_19() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_ConnectionConfig_19)); }
	inline ConnectionConfig_t4173981269 * get_m_ConnectionConfig_19() const { return ___m_ConnectionConfig_19; }
	inline ConnectionConfig_t4173981269 ** get_address_of_m_ConnectionConfig_19() { return &___m_ConnectionConfig_19; }
	inline void set_m_ConnectionConfig_19(ConnectionConfig_t4173981269 * value)
	{
		___m_ConnectionConfig_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionConfig_19), value);
	}

	inline static int32_t get_offset_of_m_GlobalConfig_20() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_GlobalConfig_20)); }
	inline GlobalConfig_t833512557 * get_m_GlobalConfig_20() const { return ___m_GlobalConfig_20; }
	inline GlobalConfig_t833512557 ** get_address_of_m_GlobalConfig_20() { return &___m_GlobalConfig_20; }
	inline void set_m_GlobalConfig_20(GlobalConfig_t833512557 * value)
	{
		___m_GlobalConfig_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalConfig_20), value);
	}

	inline static int32_t get_offset_of_m_Channels_21() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_Channels_21)); }
	inline List_1_t743604312 * get_m_Channels_21() const { return ___m_Channels_21; }
	inline List_1_t743604312 ** get_address_of_m_Channels_21() { return &___m_Channels_21; }
	inline void set_m_Channels_21(List_1_t743604312 * value)
	{
		___m_Channels_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_21), value);
	}

	inline static int32_t get_offset_of_m_UseWebSockets_22() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_UseWebSockets_22)); }
	inline bool get_m_UseWebSockets_22() const { return ___m_UseWebSockets_22; }
	inline bool* get_address_of_m_UseWebSockets_22() { return &___m_UseWebSockets_22; }
	inline void set_m_UseWebSockets_22(bool value)
	{
		___m_UseWebSockets_22 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_23() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_UseSimulator_23)); }
	inline bool get_m_UseSimulator_23() const { return ___m_UseSimulator_23; }
	inline bool* get_address_of_m_UseSimulator_23() { return &___m_UseSimulator_23; }
	inline void set_m_UseSimulator_23(bool value)
	{
		___m_UseSimulator_23 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_24() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_SimulatedLatency_24)); }
	inline int32_t get_m_SimulatedLatency_24() const { return ___m_SimulatedLatency_24; }
	inline int32_t* get_address_of_m_SimulatedLatency_24() { return &___m_SimulatedLatency_24; }
	inline void set_m_SimulatedLatency_24(int32_t value)
	{
		___m_SimulatedLatency_24 = value;
	}

	inline static int32_t get_offset_of_m_PacketLossPercentage_25() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_PacketLossPercentage_25)); }
	inline float get_m_PacketLossPercentage_25() const { return ___m_PacketLossPercentage_25; }
	inline float* get_address_of_m_PacketLossPercentage_25() { return &___m_PacketLossPercentage_25; }
	inline void set_m_PacketLossPercentage_25(float value)
	{
		___m_PacketLossPercentage_25 = value;
	}

	inline static int32_t get_offset_of_m_MaxBufferedPackets_26() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MaxBufferedPackets_26)); }
	inline int32_t get_m_MaxBufferedPackets_26() const { return ___m_MaxBufferedPackets_26; }
	inline int32_t* get_address_of_m_MaxBufferedPackets_26() { return &___m_MaxBufferedPackets_26; }
	inline void set_m_MaxBufferedPackets_26(int32_t value)
	{
		___m_MaxBufferedPackets_26 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_27() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_AllowFragmentation_27)); }
	inline bool get_m_AllowFragmentation_27() const { return ___m_AllowFragmentation_27; }
	inline bool* get_address_of_m_AllowFragmentation_27() { return &___m_AllowFragmentation_27; }
	inline void set_m_AllowFragmentation_27(bool value)
	{
		___m_AllowFragmentation_27 = value;
	}

	inline static int32_t get_offset_of_m_MatchHost_28() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MatchHost_28)); }
	inline String_t* get_m_MatchHost_28() const { return ___m_MatchHost_28; }
	inline String_t** get_address_of_m_MatchHost_28() { return &___m_MatchHost_28; }
	inline void set_m_MatchHost_28(String_t* value)
	{
		___m_MatchHost_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_MatchHost_28), value);
	}

	inline static int32_t get_offset_of_m_MatchPort_29() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MatchPort_29)); }
	inline int32_t get_m_MatchPort_29() const { return ___m_MatchPort_29; }
	inline int32_t* get_address_of_m_MatchPort_29() { return &___m_MatchPort_29; }
	inline void set_m_MatchPort_29(int32_t value)
	{
		___m_MatchPort_29 = value;
	}

	inline static int32_t get_offset_of_matchName_30() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___matchName_30)); }
	inline String_t* get_matchName_30() const { return ___matchName_30; }
	inline String_t** get_address_of_matchName_30() { return &___matchName_30; }
	inline void set_matchName_30(String_t* value)
	{
		___matchName_30 = value;
		Il2CppCodeGenWriteBarrier((&___matchName_30), value);
	}

	inline static int32_t get_offset_of_matchSize_31() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___matchSize_31)); }
	inline uint32_t get_matchSize_31() const { return ___matchSize_31; }
	inline uint32_t* get_address_of_matchSize_31() { return &___matchSize_31; }
	inline void set_matchSize_31(uint32_t value)
	{
		___matchSize_31 = value;
	}

	inline static int32_t get_offset_of_m_MigrationManager_32() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_MigrationManager_32)); }
	inline NetworkMigrationManager_t2405847916 * get_m_MigrationManager_32() const { return ___m_MigrationManager_32; }
	inline NetworkMigrationManager_t2405847916 ** get_address_of_m_MigrationManager_32() { return &___m_MigrationManager_32; }
	inline void set_m_MigrationManager_32(NetworkMigrationManager_t2405847916 * value)
	{
		___m_MigrationManager_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_MigrationManager_32), value);
	}

	inline static int32_t get_offset_of_m_EndPoint_33() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_EndPoint_33)); }
	inline EndPoint_t982345378 * get_m_EndPoint_33() const { return ___m_EndPoint_33; }
	inline EndPoint_t982345378 ** get_address_of_m_EndPoint_33() { return &___m_EndPoint_33; }
	inline void set_m_EndPoint_33(EndPoint_t982345378 * value)
	{
		___m_EndPoint_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_EndPoint_33), value);
	}

	inline static int32_t get_offset_of_m_ClientLoadedScene_34() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___m_ClientLoadedScene_34)); }
	inline bool get_m_ClientLoadedScene_34() const { return ___m_ClientLoadedScene_34; }
	inline bool* get_address_of_m_ClientLoadedScene_34() { return &___m_ClientLoadedScene_34; }
	inline void set_m_ClientLoadedScene_34(bool value)
	{
		___m_ClientLoadedScene_34 = value;
	}

	inline static int32_t get_offset_of_isNetworkActive_36() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___isNetworkActive_36)); }
	inline bool get_isNetworkActive_36() const { return ___isNetworkActive_36; }
	inline bool* get_address_of_isNetworkActive_36() { return &___isNetworkActive_36; }
	inline void set_isNetworkActive_36(bool value)
	{
		___isNetworkActive_36 = value;
	}

	inline static int32_t get_offset_of_client_37() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___client_37)); }
	inline NetworkClient_t3758195968 * get_client_37() const { return ___client_37; }
	inline NetworkClient_t3758195968 ** get_address_of_client_37() { return &___client_37; }
	inline void set_client_37(NetworkClient_t3758195968 * value)
	{
		___client_37 = value;
		Il2CppCodeGenWriteBarrier((&___client_37), value);
	}

	inline static int32_t get_offset_of_matchInfo_40() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___matchInfo_40)); }
	inline MatchInfo_t221301733 * get_matchInfo_40() const { return ___matchInfo_40; }
	inline MatchInfo_t221301733 ** get_address_of_matchInfo_40() { return &___matchInfo_40; }
	inline void set_matchInfo_40(MatchInfo_t221301733 * value)
	{
		___matchInfo_40 = value;
		Il2CppCodeGenWriteBarrier((&___matchInfo_40), value);
	}

	inline static int32_t get_offset_of_matchMaker_41() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___matchMaker_41)); }
	inline NetworkMatch_t2930480025 * get_matchMaker_41() const { return ___matchMaker_41; }
	inline NetworkMatch_t2930480025 ** get_address_of_matchMaker_41() { return &___matchMaker_41; }
	inline void set_matchMaker_41(NetworkMatch_t2930480025 * value)
	{
		___matchMaker_41 = value;
		Il2CppCodeGenWriteBarrier((&___matchMaker_41), value);
	}

	inline static int32_t get_offset_of_matches_42() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779, ___matches_42)); }
	inline List_1_t343529635 * get_matches_42() const { return ___matches_42; }
	inline List_1_t343529635 ** get_address_of_matches_42() { return &___matches_42; }
	inline void set_matches_42(List_1_t343529635 * value)
	{
		___matches_42 = value;
		Il2CppCodeGenWriteBarrier((&___matches_42), value);
	}
};

struct NetworkManager_t468665779_StaticFields
{
public:
	// System.String UnityEngine.Networking.NetworkManager::networkSceneName
	String_t* ___networkSceneName_35;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.Networking.NetworkManager::s_StartPositions
	List_1_t777473367 * ___s_StartPositions_38;
	// System.Int32 UnityEngine.Networking.NetworkManager::s_StartPositionIndex
	int32_t ___s_StartPositionIndex_39;
	// UnityEngine.Networking.NetworkManager UnityEngine.Networking.NetworkManager::singleton
	NetworkManager_t468665779 * ___singleton_43;
	// UnityEngine.Networking.NetworkSystem.AddPlayerMessage UnityEngine.Networking.NetworkManager::s_AddPlayerMessage
	AddPlayerMessage_t787692541 * ___s_AddPlayerMessage_44;
	// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage UnityEngine.Networking.NetworkManager::s_RemovePlayerMessage
	RemovePlayerMessage_t1120190071 * ___s_RemovePlayerMessage_45;
	// UnityEngine.Networking.NetworkSystem.ErrorMessage UnityEngine.Networking.NetworkManager::s_ErrorMessage
	ErrorMessage_t4257973676 * ___s_ErrorMessage_46;
	// UnityEngine.AsyncOperation UnityEngine.Networking.NetworkManager::s_LoadingSceneAsync
	AsyncOperation_t1445031843 * ___s_LoadingSceneAsync_47;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkManager::s_ClientReadyConnection
	NetworkConnection_t2705220091 * ___s_ClientReadyConnection_48;
	// System.String UnityEngine.Networking.NetworkManager::s_Address
	String_t* ___s_Address_49;

public:
	inline static int32_t get_offset_of_networkSceneName_35() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___networkSceneName_35)); }
	inline String_t* get_networkSceneName_35() const { return ___networkSceneName_35; }
	inline String_t** get_address_of_networkSceneName_35() { return &___networkSceneName_35; }
	inline void set_networkSceneName_35(String_t* value)
	{
		___networkSceneName_35 = value;
		Il2CppCodeGenWriteBarrier((&___networkSceneName_35), value);
	}

	inline static int32_t get_offset_of_s_StartPositions_38() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_StartPositions_38)); }
	inline List_1_t777473367 * get_s_StartPositions_38() const { return ___s_StartPositions_38; }
	inline List_1_t777473367 ** get_address_of_s_StartPositions_38() { return &___s_StartPositions_38; }
	inline void set_s_StartPositions_38(List_1_t777473367 * value)
	{
		___s_StartPositions_38 = value;
		Il2CppCodeGenWriteBarrier((&___s_StartPositions_38), value);
	}

	inline static int32_t get_offset_of_s_StartPositionIndex_39() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_StartPositionIndex_39)); }
	inline int32_t get_s_StartPositionIndex_39() const { return ___s_StartPositionIndex_39; }
	inline int32_t* get_address_of_s_StartPositionIndex_39() { return &___s_StartPositionIndex_39; }
	inline void set_s_StartPositionIndex_39(int32_t value)
	{
		___s_StartPositionIndex_39 = value;
	}

	inline static int32_t get_offset_of_singleton_43() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___singleton_43)); }
	inline NetworkManager_t468665779 * get_singleton_43() const { return ___singleton_43; }
	inline NetworkManager_t468665779 ** get_address_of_singleton_43() { return &___singleton_43; }
	inline void set_singleton_43(NetworkManager_t468665779 * value)
	{
		___singleton_43 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_43), value);
	}

	inline static int32_t get_offset_of_s_AddPlayerMessage_44() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_AddPlayerMessage_44)); }
	inline AddPlayerMessage_t787692541 * get_s_AddPlayerMessage_44() const { return ___s_AddPlayerMessage_44; }
	inline AddPlayerMessage_t787692541 ** get_address_of_s_AddPlayerMessage_44() { return &___s_AddPlayerMessage_44; }
	inline void set_s_AddPlayerMessage_44(AddPlayerMessage_t787692541 * value)
	{
		___s_AddPlayerMessage_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_AddPlayerMessage_44), value);
	}

	inline static int32_t get_offset_of_s_RemovePlayerMessage_45() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_RemovePlayerMessage_45)); }
	inline RemovePlayerMessage_t1120190071 * get_s_RemovePlayerMessage_45() const { return ___s_RemovePlayerMessage_45; }
	inline RemovePlayerMessage_t1120190071 ** get_address_of_s_RemovePlayerMessage_45() { return &___s_RemovePlayerMessage_45; }
	inline void set_s_RemovePlayerMessage_45(RemovePlayerMessage_t1120190071 * value)
	{
		___s_RemovePlayerMessage_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_RemovePlayerMessage_45), value);
	}

	inline static int32_t get_offset_of_s_ErrorMessage_46() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_ErrorMessage_46)); }
	inline ErrorMessage_t4257973676 * get_s_ErrorMessage_46() const { return ___s_ErrorMessage_46; }
	inline ErrorMessage_t4257973676 ** get_address_of_s_ErrorMessage_46() { return &___s_ErrorMessage_46; }
	inline void set_s_ErrorMessage_46(ErrorMessage_t4257973676 * value)
	{
		___s_ErrorMessage_46 = value;
		Il2CppCodeGenWriteBarrier((&___s_ErrorMessage_46), value);
	}

	inline static int32_t get_offset_of_s_LoadingSceneAsync_47() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_LoadingSceneAsync_47)); }
	inline AsyncOperation_t1445031843 * get_s_LoadingSceneAsync_47() const { return ___s_LoadingSceneAsync_47; }
	inline AsyncOperation_t1445031843 ** get_address_of_s_LoadingSceneAsync_47() { return &___s_LoadingSceneAsync_47; }
	inline void set_s_LoadingSceneAsync_47(AsyncOperation_t1445031843 * value)
	{
		___s_LoadingSceneAsync_47 = value;
		Il2CppCodeGenWriteBarrier((&___s_LoadingSceneAsync_47), value);
	}

	inline static int32_t get_offset_of_s_ClientReadyConnection_48() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_ClientReadyConnection_48)); }
	inline NetworkConnection_t2705220091 * get_s_ClientReadyConnection_48() const { return ___s_ClientReadyConnection_48; }
	inline NetworkConnection_t2705220091 ** get_address_of_s_ClientReadyConnection_48() { return &___s_ClientReadyConnection_48; }
	inline void set_s_ClientReadyConnection_48(NetworkConnection_t2705220091 * value)
	{
		___s_ClientReadyConnection_48 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientReadyConnection_48), value);
	}

	inline static int32_t get_offset_of_s_Address_49() { return static_cast<int32_t>(offsetof(NetworkManager_t468665779_StaticFields, ___s_Address_49)); }
	inline String_t* get_s_Address_49() const { return ___s_Address_49; }
	inline String_t** get_address_of_s_Address_49() { return &___s_Address_49; }
	inline void set_s_Address_49(String_t* value)
	{
		___s_Address_49 = value;
		Il2CppCodeGenWriteBarrier((&___s_Address_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMANAGER_T468665779_H
#ifndef HEXCOLORFIELD_T944280679_H
#define HEXCOLORFIELD_T944280679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HexColorField
struct  HexColorField_t944280679  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker HexColorField::hsvpicker
	ColorPicker_t228004619 * ___hsvpicker_2;
	// System.Boolean HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField HexColorField::hexInputField
	InputField_t3762917431 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___hsvpicker_2)); }
	inline ColorPicker_t228004619 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t228004619 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t228004619 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___hexInputField_4)); }
	inline InputField_t3762917431 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t3762917431 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t3762917431 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T944280679_H
#ifndef PARTICLEPAINTER_T1984011264_H
#define PARTICLEPAINTER_T1984011264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePainter
struct  ParticlePainter_t1984011264  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem ParticlePainter::painterParticlePrefab
	ParticleSystem_t1800779281 * ___painterParticlePrefab_2;
	// System.Single ParticlePainter::minDistanceThreshold
	float ___minDistanceThreshold_3;
	// System.Single ParticlePainter::maxDistanceThreshold
	float ___maxDistanceThreshold_4;
	// System.Boolean ParticlePainter::frameUpdated
	bool ___frameUpdated_5;
	// System.Single ParticlePainter::particleSize
	float ___particleSize_6;
	// System.Single ParticlePainter::penDistance
	float ___penDistance_7;
	// ColorPicker ParticlePainter::colorPicker
	ColorPicker_t228004619 * ___colorPicker_8;
	// UnityEngine.ParticleSystem ParticlePainter::currentPS
	ParticleSystem_t1800779281 * ___currentPS_9;
	// UnityEngine.ParticleSystem/Particle[] ParticlePainter::particles
	ParticleU5BU5D_t3069227754* ___particles_10;
	// UnityEngine.Vector3 ParticlePainter::previousPosition
	Vector3_t3722313464  ___previousPosition_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ParticlePainter::currentPaintVertices
	List_1_t899420910 * ___currentPaintVertices_12;
	// UnityEngine.Color ParticlePainter::currentColor
	Color_t2555686324  ___currentColor_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> ParticlePainter::paintSystems
	List_1_t3272854023 * ___paintSystems_14;
	// System.Int32 ParticlePainter::paintMode
	int32_t ___paintMode_15;

public:
	inline static int32_t get_offset_of_painterParticlePrefab_2() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___painterParticlePrefab_2)); }
	inline ParticleSystem_t1800779281 * get_painterParticlePrefab_2() const { return ___painterParticlePrefab_2; }
	inline ParticleSystem_t1800779281 ** get_address_of_painterParticlePrefab_2() { return &___painterParticlePrefab_2; }
	inline void set_painterParticlePrefab_2(ParticleSystem_t1800779281 * value)
	{
		___painterParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___painterParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_minDistanceThreshold_3() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___minDistanceThreshold_3)); }
	inline float get_minDistanceThreshold_3() const { return ___minDistanceThreshold_3; }
	inline float* get_address_of_minDistanceThreshold_3() { return &___minDistanceThreshold_3; }
	inline void set_minDistanceThreshold_3(float value)
	{
		___minDistanceThreshold_3 = value;
	}

	inline static int32_t get_offset_of_maxDistanceThreshold_4() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___maxDistanceThreshold_4)); }
	inline float get_maxDistanceThreshold_4() const { return ___maxDistanceThreshold_4; }
	inline float* get_address_of_maxDistanceThreshold_4() { return &___maxDistanceThreshold_4; }
	inline void set_maxDistanceThreshold_4(float value)
	{
		___maxDistanceThreshold_4 = value;
	}

	inline static int32_t get_offset_of_frameUpdated_5() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___frameUpdated_5)); }
	inline bool get_frameUpdated_5() const { return ___frameUpdated_5; }
	inline bool* get_address_of_frameUpdated_5() { return &___frameUpdated_5; }
	inline void set_frameUpdated_5(bool value)
	{
		___frameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_penDistance_7() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___penDistance_7)); }
	inline float get_penDistance_7() const { return ___penDistance_7; }
	inline float* get_address_of_penDistance_7() { return &___penDistance_7; }
	inline void set_penDistance_7(float value)
	{
		___penDistance_7 = value;
	}

	inline static int32_t get_offset_of_colorPicker_8() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___colorPicker_8)); }
	inline ColorPicker_t228004619 * get_colorPicker_8() const { return ___colorPicker_8; }
	inline ColorPicker_t228004619 ** get_address_of_colorPicker_8() { return &___colorPicker_8; }
	inline void set_colorPicker_8(ColorPicker_t228004619 * value)
	{
		___colorPicker_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_8), value);
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentPS_9)); }
	inline ParticleSystem_t1800779281 * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t1800779281 ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t1800779281 * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___particles_10)); }
	inline ParticleU5BU5D_t3069227754* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_t3069227754** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_t3069227754* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}

	inline static int32_t get_offset_of_previousPosition_11() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___previousPosition_11)); }
	inline Vector3_t3722313464  get_previousPosition_11() const { return ___previousPosition_11; }
	inline Vector3_t3722313464 * get_address_of_previousPosition_11() { return &___previousPosition_11; }
	inline void set_previousPosition_11(Vector3_t3722313464  value)
	{
		___previousPosition_11 = value;
	}

	inline static int32_t get_offset_of_currentPaintVertices_12() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentPaintVertices_12)); }
	inline List_1_t899420910 * get_currentPaintVertices_12() const { return ___currentPaintVertices_12; }
	inline List_1_t899420910 ** get_address_of_currentPaintVertices_12() { return &___currentPaintVertices_12; }
	inline void set_currentPaintVertices_12(List_1_t899420910 * value)
	{
		___currentPaintVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPaintVertices_12), value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentColor_13)); }
	inline Color_t2555686324  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t2555686324 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t2555686324  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_paintSystems_14() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___paintSystems_14)); }
	inline List_1_t3272854023 * get_paintSystems_14() const { return ___paintSystems_14; }
	inline List_1_t3272854023 ** get_address_of_paintSystems_14() { return &___paintSystems_14; }
	inline void set_paintSystems_14(List_1_t3272854023 * value)
	{
		___paintSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___paintSystems_14), value);
	}

	inline static int32_t get_offset_of_paintMode_15() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___paintMode_15)); }
	inline int32_t get_paintMode_15() const { return ___paintMode_15; }
	inline int32_t* get_address_of_paintMode_15() { return &___paintMode_15; }
	inline void set_paintMode_15(int32_t value)
	{
		___paintMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPAINTER_T1984011264_H
#ifndef AR3DOFCAMERAMANAGER_T1160001149_H
#define AR3DOFCAMERAMANAGER_T1160001149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AR3DOFCameraManager
struct  AR3DOFCameraManager_t1160001149  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera AR3DOFCameraManager::m_camera
	Camera_t4157153871 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface AR3DOFCameraManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.Material AR3DOFCameraManager::savedClearMaterial
	Material_t340375123 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___m_camera_2)); }
	inline Camera_t4157153871 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t4157153871 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t4157153871 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___savedClearMaterial_4)); }
	inline Material_t340375123 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t340375123 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t340375123 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AR3DOFCAMERAMANAGER_T1160001149_H
#ifndef OBJECTSCANMANAGER_T4080581421_H
#define OBJECTSCANMANAGER_T4080581421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectScanManager
struct  ObjectScanManager_t4080581421  : public MonoBehaviour_t3962482529
{
public:
	// ObjectScanSessionManager ObjectScanManager::m_ARSessionManager
	ObjectScanSessionManager_t3985853727 * ___m_ARSessionManager_2;
	// UnityEngine.UI.Text ObjectScanManager::listOfObjects
	Text_t1901882714 * ___listOfObjects_3;
	// System.Int32 ObjectScanManager::objIndex
	int32_t ___objIndex_4;
	// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARReferenceObject> ObjectScanManager::scannedObjects
	List_1_t2537441456 * ___scannedObjects_5;
	// System.Boolean ObjectScanManager::detectionMode
	bool ___detectionMode_6;
	// UnityEngine.XR.iOS.PickBoundingBox ObjectScanManager::pickBoundingBox
	PickBoundingBox_t873364976 * ___pickBoundingBox_7;

public:
	inline static int32_t get_offset_of_m_ARSessionManager_2() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___m_ARSessionManager_2)); }
	inline ObjectScanSessionManager_t3985853727 * get_m_ARSessionManager_2() const { return ___m_ARSessionManager_2; }
	inline ObjectScanSessionManager_t3985853727 ** get_address_of_m_ARSessionManager_2() { return &___m_ARSessionManager_2; }
	inline void set_m_ARSessionManager_2(ObjectScanSessionManager_t3985853727 * value)
	{
		___m_ARSessionManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARSessionManager_2), value);
	}

	inline static int32_t get_offset_of_listOfObjects_3() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___listOfObjects_3)); }
	inline Text_t1901882714 * get_listOfObjects_3() const { return ___listOfObjects_3; }
	inline Text_t1901882714 ** get_address_of_listOfObjects_3() { return &___listOfObjects_3; }
	inline void set_listOfObjects_3(Text_t1901882714 * value)
	{
		___listOfObjects_3 = value;
		Il2CppCodeGenWriteBarrier((&___listOfObjects_3), value);
	}

	inline static int32_t get_offset_of_objIndex_4() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___objIndex_4)); }
	inline int32_t get_objIndex_4() const { return ___objIndex_4; }
	inline int32_t* get_address_of_objIndex_4() { return &___objIndex_4; }
	inline void set_objIndex_4(int32_t value)
	{
		___objIndex_4 = value;
	}

	inline static int32_t get_offset_of_scannedObjects_5() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___scannedObjects_5)); }
	inline List_1_t2537441456 * get_scannedObjects_5() const { return ___scannedObjects_5; }
	inline List_1_t2537441456 ** get_address_of_scannedObjects_5() { return &___scannedObjects_5; }
	inline void set_scannedObjects_5(List_1_t2537441456 * value)
	{
		___scannedObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___scannedObjects_5), value);
	}

	inline static int32_t get_offset_of_detectionMode_6() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___detectionMode_6)); }
	inline bool get_detectionMode_6() const { return ___detectionMode_6; }
	inline bool* get_address_of_detectionMode_6() { return &___detectionMode_6; }
	inline void set_detectionMode_6(bool value)
	{
		___detectionMode_6 = value;
	}

	inline static int32_t get_offset_of_pickBoundingBox_7() { return static_cast<int32_t>(offsetof(ObjectScanManager_t4080581421, ___pickBoundingBox_7)); }
	inline PickBoundingBox_t873364976 * get_pickBoundingBox_7() const { return ___pickBoundingBox_7; }
	inline PickBoundingBox_t873364976 ** get_address_of_pickBoundingBox_7() { return &___pickBoundingBox_7; }
	inline void set_pickBoundingBox_7(PickBoundingBox_t873364976 * value)
	{
		___pickBoundingBox_7 = value;
		Il2CppCodeGenWriteBarrier((&___pickBoundingBox_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSCANMANAGER_T4080581421_H
#ifndef DETECTEDOBJECTMANAGER_T2555724390_H
#define DETECTEDOBJECTMANAGER_T2555724390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DetectedObjectManager
struct  DetectedObjectManager_t2555724390  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DetectedObjectManager::m_ObjectPrefab
	GameObject_t1113636619 * ___m_ObjectPrefab_2;
	// Collections.Hybrid.Generic.LinkedListDictionary`2<System.String,UnityEngine.GameObject> DetectedObjectManager::objectAnchorMap
	LinkedListDictionary_2_t1717611866 * ___objectAnchorMap_3;

public:
	inline static int32_t get_offset_of_m_ObjectPrefab_2() { return static_cast<int32_t>(offsetof(DetectedObjectManager_t2555724390, ___m_ObjectPrefab_2)); }
	inline GameObject_t1113636619 * get_m_ObjectPrefab_2() const { return ___m_ObjectPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_m_ObjectPrefab_2() { return &___m_ObjectPrefab_2; }
	inline void set_m_ObjectPrefab_2(GameObject_t1113636619 * value)
	{
		___m_ObjectPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectPrefab_2), value);
	}

	inline static int32_t get_offset_of_objectAnchorMap_3() { return static_cast<int32_t>(offsetof(DetectedObjectManager_t2555724390, ___objectAnchorMap_3)); }
	inline LinkedListDictionary_2_t1717611866 * get_objectAnchorMap_3() const { return ___objectAnchorMap_3; }
	inline LinkedListDictionary_2_t1717611866 ** get_address_of_objectAnchorMap_3() { return &___objectAnchorMap_3; }
	inline void set_objectAnchorMap_3(LinkedListDictionary_2_t1717611866 * value)
	{
		___objectAnchorMap_3 = value;
		Il2CppCodeGenWriteBarrier((&___objectAnchorMap_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDOBJECTMANAGER_T2555724390_H
#ifndef WORLDMAPMANAGER_T2538599596_H
#define WORLDMAPMANAGER_T2538599596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapManager
struct  WorldMapManager_t2538599596  : public MonoBehaviour_t3962482529
{
public:
	// UnityARCameraManager WorldMapManager::m_ARCameraManager
	UnityARCameraManager_t4002280589 * ___m_ARCameraManager_2;
	// UnityEngine.XR.iOS.ARWorldMap WorldMapManager::m_LoadedMap
	ARWorldMap_t2240790807 * ___m_LoadedMap_3;
	// UnityEngine.XR.iOS.serializableARWorldMap WorldMapManager::serializedWorldMap
	serializableARWorldMap_t2514323794 * ___serializedWorldMap_4;
	// UnityEngine.XR.iOS.ARTrackingStateReason WorldMapManager::m_LastReason
	int32_t ___m_LastReason_5;

public:
	inline static int32_t get_offset_of_m_ARCameraManager_2() { return static_cast<int32_t>(offsetof(WorldMapManager_t2538599596, ___m_ARCameraManager_2)); }
	inline UnityARCameraManager_t4002280589 * get_m_ARCameraManager_2() const { return ___m_ARCameraManager_2; }
	inline UnityARCameraManager_t4002280589 ** get_address_of_m_ARCameraManager_2() { return &___m_ARCameraManager_2; }
	inline void set_m_ARCameraManager_2(UnityARCameraManager_t4002280589 * value)
	{
		___m_ARCameraManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARCameraManager_2), value);
	}

	inline static int32_t get_offset_of_m_LoadedMap_3() { return static_cast<int32_t>(offsetof(WorldMapManager_t2538599596, ___m_LoadedMap_3)); }
	inline ARWorldMap_t2240790807 * get_m_LoadedMap_3() const { return ___m_LoadedMap_3; }
	inline ARWorldMap_t2240790807 ** get_address_of_m_LoadedMap_3() { return &___m_LoadedMap_3; }
	inline void set_m_LoadedMap_3(ARWorldMap_t2240790807 * value)
	{
		___m_LoadedMap_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_LoadedMap_3), value);
	}

	inline static int32_t get_offset_of_serializedWorldMap_4() { return static_cast<int32_t>(offsetof(WorldMapManager_t2538599596, ___serializedWorldMap_4)); }
	inline serializableARWorldMap_t2514323794 * get_serializedWorldMap_4() const { return ___serializedWorldMap_4; }
	inline serializableARWorldMap_t2514323794 ** get_address_of_serializedWorldMap_4() { return &___serializedWorldMap_4; }
	inline void set_serializedWorldMap_4(serializableARWorldMap_t2514323794 * value)
	{
		___serializedWorldMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___serializedWorldMap_4), value);
	}

	inline static int32_t get_offset_of_m_LastReason_5() { return static_cast<int32_t>(offsetof(WorldMapManager_t2538599596, ___m_LastReason_5)); }
	inline int32_t get_m_LastReason_5() const { return ___m_LastReason_5; }
	inline int32_t* get_address_of_m_LastReason_5() { return &___m_LastReason_5; }
	inline void set_m_LastReason_5(int32_t value)
	{
		___m_LastReason_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDMAPMANAGER_T2538599596_H
#ifndef UPDATEWORLDMAPPINGSTATUS_T2738391865_H
#define UPDATEWORLDMAPPINGSTATUS_T2738391865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateWorldMappingStatus
struct  UpdateWorldMappingStatus_t2738391865  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text UpdateWorldMappingStatus::text
	Text_t1901882714 * ___text_2;
	// UnityEngine.UI.Text UpdateWorldMappingStatus::tracking
	Text_t1901882714 * ___tracking_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(UpdateWorldMappingStatus_t2738391865, ___text_2)); }
	inline Text_t1901882714 * get_text_2() const { return ___text_2; }
	inline Text_t1901882714 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t1901882714 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_tracking_3() { return static_cast<int32_t>(offsetof(UpdateWorldMappingStatus_t2738391865, ___tracking_3)); }
	inline Text_t1901882714 * get_tracking_3() const { return ___tracking_3; }
	inline Text_t1901882714 ** get_address_of_tracking_3() { return &___tracking_3; }
	inline void set_tracking_3(Text_t1901882714 * value)
	{
		___tracking_3 = value;
		Il2CppCodeGenWriteBarrier((&___tracking_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEWORLDMAPPINGSTATUS_T2738391865_H
#ifndef GENERATEOBJECTANCHOR_T1604756478_H
#define GENERATEOBJECTANCHOR_T1604756478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenerateObjectAnchor
struct  GenerateObjectAnchor_t1604756478  : public MonoBehaviour_t3962482529
{
public:
	// ARReferenceObjectAsset GenerateObjectAnchor::referenceObjectAsset
	ARReferenceObjectAsset_t1156404477 * ___referenceObjectAsset_2;
	// UnityEngine.GameObject GenerateObjectAnchor::prefabToGenerate
	GameObject_t1113636619 * ___prefabToGenerate_3;
	// UnityEngine.GameObject GenerateObjectAnchor::objectAnchorGO
	GameObject_t1113636619 * ___objectAnchorGO_4;

public:
	inline static int32_t get_offset_of_referenceObjectAsset_2() { return static_cast<int32_t>(offsetof(GenerateObjectAnchor_t1604756478, ___referenceObjectAsset_2)); }
	inline ARReferenceObjectAsset_t1156404477 * get_referenceObjectAsset_2() const { return ___referenceObjectAsset_2; }
	inline ARReferenceObjectAsset_t1156404477 ** get_address_of_referenceObjectAsset_2() { return &___referenceObjectAsset_2; }
	inline void set_referenceObjectAsset_2(ARReferenceObjectAsset_t1156404477 * value)
	{
		___referenceObjectAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___referenceObjectAsset_2), value);
	}

	inline static int32_t get_offset_of_prefabToGenerate_3() { return static_cast<int32_t>(offsetof(GenerateObjectAnchor_t1604756478, ___prefabToGenerate_3)); }
	inline GameObject_t1113636619 * get_prefabToGenerate_3() const { return ___prefabToGenerate_3; }
	inline GameObject_t1113636619 ** get_address_of_prefabToGenerate_3() { return &___prefabToGenerate_3; }
	inline void set_prefabToGenerate_3(GameObject_t1113636619 * value)
	{
		___prefabToGenerate_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefabToGenerate_3), value);
	}

	inline static int32_t get_offset_of_objectAnchorGO_4() { return static_cast<int32_t>(offsetof(GenerateObjectAnchor_t1604756478, ___objectAnchorGO_4)); }
	inline GameObject_t1113636619 * get_objectAnchorGO_4() const { return ___objectAnchorGO_4; }
	inline GameObject_t1113636619 ** get_address_of_objectAnchorGO_4() { return &___objectAnchorGO_4; }
	inline void set_objectAnchorGO_4(GameObject_t1113636619 * value)
	{
		___objectAnchorGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectAnchorGO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEOBJECTANCHOR_T1604756478_H
#ifndef REFLECTIONPROBEGAMEOBJECT_T172576734_H
#define REFLECTIONPROBEGAMEOBJECT_T172576734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReflectionProbeGameObject
struct  ReflectionProbeGameObject_t172576734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ReflectionProbe ReflectionProbeGameObject::reflectionProbe
	ReflectionProbe_t175708936 * ___reflectionProbe_2;
	// System.Boolean ReflectionProbeGameObject::latchUpdate
	bool ___latchUpdate_3;
	// UnityEngine.Cubemap ReflectionProbeGameObject::latchedTexture
	Cubemap_t1972384409 * ___latchedTexture_4;
	// UnityEngine.GameObject ReflectionProbeGameObject::debugExtentGO
	GameObject_t1113636619 * ___debugExtentGO_5;

public:
	inline static int32_t get_offset_of_reflectionProbe_2() { return static_cast<int32_t>(offsetof(ReflectionProbeGameObject_t172576734, ___reflectionProbe_2)); }
	inline ReflectionProbe_t175708936 * get_reflectionProbe_2() const { return ___reflectionProbe_2; }
	inline ReflectionProbe_t175708936 ** get_address_of_reflectionProbe_2() { return &___reflectionProbe_2; }
	inline void set_reflectionProbe_2(ReflectionProbe_t175708936 * value)
	{
		___reflectionProbe_2 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionProbe_2), value);
	}

	inline static int32_t get_offset_of_latchUpdate_3() { return static_cast<int32_t>(offsetof(ReflectionProbeGameObject_t172576734, ___latchUpdate_3)); }
	inline bool get_latchUpdate_3() const { return ___latchUpdate_3; }
	inline bool* get_address_of_latchUpdate_3() { return &___latchUpdate_3; }
	inline void set_latchUpdate_3(bool value)
	{
		___latchUpdate_3 = value;
	}

	inline static int32_t get_offset_of_latchedTexture_4() { return static_cast<int32_t>(offsetof(ReflectionProbeGameObject_t172576734, ___latchedTexture_4)); }
	inline Cubemap_t1972384409 * get_latchedTexture_4() const { return ___latchedTexture_4; }
	inline Cubemap_t1972384409 ** get_address_of_latchedTexture_4() { return &___latchedTexture_4; }
	inline void set_latchedTexture_4(Cubemap_t1972384409 * value)
	{
		___latchedTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___latchedTexture_4), value);
	}

	inline static int32_t get_offset_of_debugExtentGO_5() { return static_cast<int32_t>(offsetof(ReflectionProbeGameObject_t172576734, ___debugExtentGO_5)); }
	inline GameObject_t1113636619 * get_debugExtentGO_5() const { return ___debugExtentGO_5; }
	inline GameObject_t1113636619 ** get_address_of_debugExtentGO_5() { return &___debugExtentGO_5; }
	inline void set_debugExtentGO_5(GameObject_t1113636619 * value)
	{
		___debugExtentGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___debugExtentGO_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONPROBEGAMEOBJECT_T172576734_H
#ifndef HITCREATEENVIRONMENTPROBE_T2118888515_H
#define HITCREATEENVIRONMENTPROBE_T2118888515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitCreateEnvironmentProbe
struct  HitCreateEnvironmentProbe_t2118888515  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HitCreateEnvironmentProbe::maxRayDistance
	float ___maxRayDistance_2;
	// UnityEngine.LayerMask HitCreateEnvironmentProbe::collisionLayer
	LayerMask_t3493934918  ___collisionLayer_3;

public:
	inline static int32_t get_offset_of_maxRayDistance_2() { return static_cast<int32_t>(offsetof(HitCreateEnvironmentProbe_t2118888515, ___maxRayDistance_2)); }
	inline float get_maxRayDistance_2() const { return ___maxRayDistance_2; }
	inline float* get_address_of_maxRayDistance_2() { return &___maxRayDistance_2; }
	inline void set_maxRayDistance_2(float value)
	{
		___maxRayDistance_2 = value;
	}

	inline static int32_t get_offset_of_collisionLayer_3() { return static_cast<int32_t>(offsetof(HitCreateEnvironmentProbe_t2118888515, ___collisionLayer_3)); }
	inline LayerMask_t3493934918  get_collisionLayer_3() const { return ___collisionLayer_3; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayer_3() { return &___collisionLayer_3; }
	inline void set_collisionLayer_3(LayerMask_t3493934918  value)
	{
		___collisionLayer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCREATEENVIRONMENTPROBE_T2118888515_H
#ifndef GENERATEENVIRONMENTPROBEANCHORS_T2290703479_H
#define GENERATEENVIRONMENTPROBEANCHORS_T2290703479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenerateEnvironmentProbeAnchors
struct  GenerateEnvironmentProbeAnchors_t2290703479  : public MonoBehaviour_t3962482529
{
public:
	// ReflectionProbeGameObject GenerateEnvironmentProbeAnchors::m_ReflectionProbePrefab
	ReflectionProbeGameObject_t172576734 * ___m_ReflectionProbePrefab_2;
	// Collections.Hybrid.Generic.LinkedListDictionary`2<System.String,ReflectionProbeGameObject> GenerateEnvironmentProbeAnchors::probeAnchorMap
	LinkedListDictionary_2_t776551981 * ___probeAnchorMap_3;

public:
	inline static int32_t get_offset_of_m_ReflectionProbePrefab_2() { return static_cast<int32_t>(offsetof(GenerateEnvironmentProbeAnchors_t2290703479, ___m_ReflectionProbePrefab_2)); }
	inline ReflectionProbeGameObject_t172576734 * get_m_ReflectionProbePrefab_2() const { return ___m_ReflectionProbePrefab_2; }
	inline ReflectionProbeGameObject_t172576734 ** get_address_of_m_ReflectionProbePrefab_2() { return &___m_ReflectionProbePrefab_2; }
	inline void set_m_ReflectionProbePrefab_2(ReflectionProbeGameObject_t172576734 * value)
	{
		___m_ReflectionProbePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionProbePrefab_2), value);
	}

	inline static int32_t get_offset_of_probeAnchorMap_3() { return static_cast<int32_t>(offsetof(GenerateEnvironmentProbeAnchors_t2290703479, ___probeAnchorMap_3)); }
	inline LinkedListDictionary_2_t776551981 * get_probeAnchorMap_3() const { return ___probeAnchorMap_3; }
	inline LinkedListDictionary_2_t776551981 ** get_address_of_probeAnchorMap_3() { return &___probeAnchorMap_3; }
	inline void set_probeAnchorMap_3(LinkedListDictionary_2_t776551981 * value)
	{
		___probeAnchorMap_3 = value;
		Il2CppCodeGenWriteBarrier((&___probeAnchorMap_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEENVIRONMENTPROBEANCHORS_T2290703479_H
#ifndef VIDEOFORMATSEXAMPLE_T2303262312_H
#define VIDEOFORMATSEXAMPLE_T2303262312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoFormatsExample
struct  VideoFormatsExample_t2303262312  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform VideoFormatsExample::formatsParent
	Transform_t3600365921 * ___formatsParent_2;
	// UnityEngine.GameObject VideoFormatsExample::videoFormatButtonPrefab
	GameObject_t1113636619 * ___videoFormatButtonPrefab_3;

public:
	inline static int32_t get_offset_of_formatsParent_2() { return static_cast<int32_t>(offsetof(VideoFormatsExample_t2303262312, ___formatsParent_2)); }
	inline Transform_t3600365921 * get_formatsParent_2() const { return ___formatsParent_2; }
	inline Transform_t3600365921 ** get_address_of_formatsParent_2() { return &___formatsParent_2; }
	inline void set_formatsParent_2(Transform_t3600365921 * value)
	{
		___formatsParent_2 = value;
		Il2CppCodeGenWriteBarrier((&___formatsParent_2), value);
	}

	inline static int32_t get_offset_of_videoFormatButtonPrefab_3() { return static_cast<int32_t>(offsetof(VideoFormatsExample_t2303262312, ___videoFormatButtonPrefab_3)); }
	inline GameObject_t1113636619 * get_videoFormatButtonPrefab_3() const { return ___videoFormatButtonPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_videoFormatButtonPrefab_3() { return &___videoFormatButtonPrefab_3; }
	inline void set_videoFormatButtonPrefab_3(GameObject_t1113636619 * value)
	{
		___videoFormatButtonPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___videoFormatButtonPrefab_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOFORMATSEXAMPLE_T2303262312_H
#ifndef VIDEOFORMATBUTTON_T1937817916_H
#define VIDEOFORMATBUTTON_T1937817916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoFormatButton
struct  VideoFormatButton_t1937817916  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text VideoFormatButton::videoFormatDescription
	Text_t1901882714 * ___videoFormatDescription_2;
	// UnityEngine.XR.iOS.UnityARVideoFormat VideoFormatButton::arVideoFormat
	UnityARVideoFormat_t1944454781  ___arVideoFormat_3;

public:
	inline static int32_t get_offset_of_videoFormatDescription_2() { return static_cast<int32_t>(offsetof(VideoFormatButton_t1937817916, ___videoFormatDescription_2)); }
	inline Text_t1901882714 * get_videoFormatDescription_2() const { return ___videoFormatDescription_2; }
	inline Text_t1901882714 ** get_address_of_videoFormatDescription_2() { return &___videoFormatDescription_2; }
	inline void set_videoFormatDescription_2(Text_t1901882714 * value)
	{
		___videoFormatDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoFormatDescription_2), value);
	}

	inline static int32_t get_offset_of_arVideoFormat_3() { return static_cast<int32_t>(offsetof(VideoFormatButton_t1937817916, ___arVideoFormat_3)); }
	inline UnityARVideoFormat_t1944454781  get_arVideoFormat_3() const { return ___arVideoFormat_3; }
	inline UnityARVideoFormat_t1944454781 * get_address_of_arVideoFormat_3() { return &___arVideoFormat_3; }
	inline void set_arVideoFormat_3(UnityARVideoFormat_t1944454781  value)
	{
		___arVideoFormat_3 = value;
	}
};

struct VideoFormatButton_t1937817916_StaticFields
{
public:
	// VideoFormatButton/VideoFormatButtonPressed VideoFormatButton::FormatButtonPressedEvent
	VideoFormatButtonPressed_t1187798507 * ___FormatButtonPressedEvent_4;

public:
	inline static int32_t get_offset_of_FormatButtonPressedEvent_4() { return static_cast<int32_t>(offsetof(VideoFormatButton_t1937817916_StaticFields, ___FormatButtonPressedEvent_4)); }
	inline VideoFormatButtonPressed_t1187798507 * get_FormatButtonPressedEvent_4() const { return ___FormatButtonPressedEvent_4; }
	inline VideoFormatButtonPressed_t1187798507 ** get_address_of_FormatButtonPressedEvent_4() { return &___FormatButtonPressedEvent_4; }
	inline void set_FormatButtonPressedEvent_4(VideoFormatButtonPressed_t1187798507 * value)
	{
		___FormatButtonPressedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___FormatButtonPressedEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOFORMATBUTTON_T1937817916_H
#ifndef SETWORLDORIGINCONTROL_T1961164635_H
#define SETWORLDORIGINCONTROL_T1961164635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetWorldOriginControl
struct  SetWorldOriginControl_t1961164635  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera SetWorldOriginControl::arCamera
	Camera_t4157153871 * ___arCamera_2;
	// UnityEngine.UI.Text SetWorldOriginControl::positionText
	Text_t1901882714 * ___positionText_3;
	// UnityEngine.UI.Text SetWorldOriginControl::rotationText
	Text_t1901882714 * ___rotationText_4;

public:
	inline static int32_t get_offset_of_arCamera_2() { return static_cast<int32_t>(offsetof(SetWorldOriginControl_t1961164635, ___arCamera_2)); }
	inline Camera_t4157153871 * get_arCamera_2() const { return ___arCamera_2; }
	inline Camera_t4157153871 ** get_address_of_arCamera_2() { return &___arCamera_2; }
	inline void set_arCamera_2(Camera_t4157153871 * value)
	{
		___arCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___arCamera_2), value);
	}

	inline static int32_t get_offset_of_positionText_3() { return static_cast<int32_t>(offsetof(SetWorldOriginControl_t1961164635, ___positionText_3)); }
	inline Text_t1901882714 * get_positionText_3() const { return ___positionText_3; }
	inline Text_t1901882714 ** get_address_of_positionText_3() { return &___positionText_3; }
	inline void set_positionText_3(Text_t1901882714 * value)
	{
		___positionText_3 = value;
		Il2CppCodeGenWriteBarrier((&___positionText_3), value);
	}

	inline static int32_t get_offset_of_rotationText_4() { return static_cast<int32_t>(offsetof(SetWorldOriginControl_t1961164635, ___rotationText_4)); }
	inline Text_t1901882714 * get_rotationText_4() const { return ___rotationText_4; }
	inline Text_t1901882714 ** get_address_of_rotationText_4() { return &___rotationText_4; }
	inline void set_rotationText_4(Text_t1901882714 * value)
	{
		___rotationText_4 = value;
		Il2CppCodeGenWriteBarrier((&___rotationText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETWORLDORIGINCONTROL_T1961164635_H
#ifndef ARKITPLANEMESHRENDER_T1298532386_H
#define ARKITPLANEMESHRENDER_T1298532386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKitPlaneMeshRender
struct  ARKitPlaneMeshRender_t1298532386  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshFilter ARKitPlaneMeshRender::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_2;
	// UnityEngine.LineRenderer ARKitPlaneMeshRender::lineRenderer
	LineRenderer_t3154350270 * ___lineRenderer_3;
	// UnityEngine.Mesh ARKitPlaneMeshRender::planeMesh
	Mesh_t3648964284 * ___planeMesh_4;

public:
	inline static int32_t get_offset_of_meshFilter_2() { return static_cast<int32_t>(offsetof(ARKitPlaneMeshRender_t1298532386, ___meshFilter_2)); }
	inline MeshFilter_t3523625662 * get_meshFilter_2() const { return ___meshFilter_2; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_2() { return &___meshFilter_2; }
	inline void set_meshFilter_2(MeshFilter_t3523625662 * value)
	{
		___meshFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_2), value);
	}

	inline static int32_t get_offset_of_lineRenderer_3() { return static_cast<int32_t>(offsetof(ARKitPlaneMeshRender_t1298532386, ___lineRenderer_3)); }
	inline LineRenderer_t3154350270 * get_lineRenderer_3() const { return ___lineRenderer_3; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRenderer_3() { return &___lineRenderer_3; }
	inline void set_lineRenderer_3(LineRenderer_t3154350270 * value)
	{
		___lineRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_3), value);
	}

	inline static int32_t get_offset_of_planeMesh_4() { return static_cast<int32_t>(offsetof(ARKitPlaneMeshRender_t1298532386, ___planeMesh_4)); }
	inline Mesh_t3648964284 * get_planeMesh_4() const { return ___planeMesh_4; }
	inline Mesh_t3648964284 ** get_address_of_planeMesh_4() { return &___planeMesh_4; }
	inline void set_planeMesh_4(Mesh_t3648964284 * value)
	{
		___planeMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITPLANEMESHRENDER_T1298532386_H
#ifndef RELOCALIZATIONCONTROL_T377950233_H
#define RELOCALIZATIONCONTROL_T377950233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelocalizationControl
struct  RelocalizationControl_t377950233  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text RelocalizationControl::buttonText
	Text_t1901882714 * ___buttonText_2;
	// UnityEngine.UI.Text RelocalizationControl::trackingStateText
	Text_t1901882714 * ___trackingStateText_3;
	// UnityEngine.UI.Text RelocalizationControl::trackingReasonText
	Text_t1901882714 * ___trackingReasonText_4;

public:
	inline static int32_t get_offset_of_buttonText_2() { return static_cast<int32_t>(offsetof(RelocalizationControl_t377950233, ___buttonText_2)); }
	inline Text_t1901882714 * get_buttonText_2() const { return ___buttonText_2; }
	inline Text_t1901882714 ** get_address_of_buttonText_2() { return &___buttonText_2; }
	inline void set_buttonText_2(Text_t1901882714 * value)
	{
		___buttonText_2 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_2), value);
	}

	inline static int32_t get_offset_of_trackingStateText_3() { return static_cast<int32_t>(offsetof(RelocalizationControl_t377950233, ___trackingStateText_3)); }
	inline Text_t1901882714 * get_trackingStateText_3() const { return ___trackingStateText_3; }
	inline Text_t1901882714 ** get_address_of_trackingStateText_3() { return &___trackingStateText_3; }
	inline void set_trackingStateText_3(Text_t1901882714 * value)
	{
		___trackingStateText_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackingStateText_3), value);
	}

	inline static int32_t get_offset_of_trackingReasonText_4() { return static_cast<int32_t>(offsetof(RelocalizationControl_t377950233, ___trackingReasonText_4)); }
	inline Text_t1901882714 * get_trackingReasonText_4() const { return ___trackingReasonText_4; }
	inline Text_t1901882714 ** get_address_of_trackingReasonText_4() { return &___trackingReasonText_4; }
	inline void set_trackingReasonText_4(Text_t1901882714 * value)
	{
		___trackingReasonText_4 = value;
		Il2CppCodeGenWriteBarrier((&___trackingReasonText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELOCALIZATIONCONTROL_T377950233_H
#ifndef GENERATEIMAGEANCHOR_T3213337420_H
#define GENERATEIMAGEANCHOR_T3213337420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenerateImageAnchor
struct  GenerateImageAnchor_t3213337420  : public MonoBehaviour_t3962482529
{
public:
	// ARReferenceImage GenerateImageAnchor::referenceImage
	ARReferenceImage_t2463148469 * ___referenceImage_2;
	// UnityEngine.GameObject GenerateImageAnchor::prefabToGenerate
	GameObject_t1113636619 * ___prefabToGenerate_3;
	// UnityEngine.GameObject GenerateImageAnchor::imageAnchorGO
	GameObject_t1113636619 * ___imageAnchorGO_4;

public:
	inline static int32_t get_offset_of_referenceImage_2() { return static_cast<int32_t>(offsetof(GenerateImageAnchor_t3213337420, ___referenceImage_2)); }
	inline ARReferenceImage_t2463148469 * get_referenceImage_2() const { return ___referenceImage_2; }
	inline ARReferenceImage_t2463148469 ** get_address_of_referenceImage_2() { return &___referenceImage_2; }
	inline void set_referenceImage_2(ARReferenceImage_t2463148469 * value)
	{
		___referenceImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___referenceImage_2), value);
	}

	inline static int32_t get_offset_of_prefabToGenerate_3() { return static_cast<int32_t>(offsetof(GenerateImageAnchor_t3213337420, ___prefabToGenerate_3)); }
	inline GameObject_t1113636619 * get_prefabToGenerate_3() const { return ___prefabToGenerate_3; }
	inline GameObject_t1113636619 ** get_address_of_prefabToGenerate_3() { return &___prefabToGenerate_3; }
	inline void set_prefabToGenerate_3(GameObject_t1113636619 * value)
	{
		___prefabToGenerate_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefabToGenerate_3), value);
	}

	inline static int32_t get_offset_of_imageAnchorGO_4() { return static_cast<int32_t>(offsetof(GenerateImageAnchor_t3213337420, ___imageAnchorGO_4)); }
	inline GameObject_t1113636619 * get_imageAnchorGO_4() const { return ___imageAnchorGO_4; }
	inline GameObject_t1113636619 ** get_address_of_imageAnchorGO_4() { return &___imageAnchorGO_4; }
	inline void set_imageAnchorGO_4(GameObject_t1113636619 * value)
	{
		___imageAnchorGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageAnchorGO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEIMAGEANCHOR_T3213337420_H
#ifndef UNITYARUSERANCHOREXAMPLE_T2657819511_H
#define UNITYARUSERANCHOREXAMPLE_T2657819511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARUserAnchorExample
struct  UnityARUserAnchorExample_t2657819511  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityARUserAnchorExample::prefabObject
	GameObject_t1113636619 * ___prefabObject_2;
	// System.Int32 UnityARUserAnchorExample::distanceFromCamera
	int32_t ___distanceFromCamera_3;
	// System.Collections.Generic.HashSet`1<System.String> UnityARUserAnchorExample::m_Clones
	HashSet_1_t412400163 * ___m_Clones_4;
	// System.Single UnityARUserAnchorExample::m_TimeUntilRemove
	float ___m_TimeUntilRemove_5;

public:
	inline static int32_t get_offset_of_prefabObject_2() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___prefabObject_2)); }
	inline GameObject_t1113636619 * get_prefabObject_2() const { return ___prefabObject_2; }
	inline GameObject_t1113636619 ** get_address_of_prefabObject_2() { return &___prefabObject_2; }
	inline void set_prefabObject_2(GameObject_t1113636619 * value)
	{
		___prefabObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabObject_2), value);
	}

	inline static int32_t get_offset_of_distanceFromCamera_3() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___distanceFromCamera_3)); }
	inline int32_t get_distanceFromCamera_3() const { return ___distanceFromCamera_3; }
	inline int32_t* get_address_of_distanceFromCamera_3() { return &___distanceFromCamera_3; }
	inline void set_distanceFromCamera_3(int32_t value)
	{
		___distanceFromCamera_3 = value;
	}

	inline static int32_t get_offset_of_m_Clones_4() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___m_Clones_4)); }
	inline HashSet_1_t412400163 * get_m_Clones_4() const { return ___m_Clones_4; }
	inline HashSet_1_t412400163 ** get_address_of_m_Clones_4() { return &___m_Clones_4; }
	inline void set_m_Clones_4(HashSet_1_t412400163 * value)
	{
		___m_Clones_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clones_4), value);
	}

	inline static int32_t get_offset_of_m_TimeUntilRemove_5() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___m_TimeUntilRemove_5)); }
	inline float get_m_TimeUntilRemove_5() const { return ___m_TimeUntilRemove_5; }
	inline float* get_address_of_m_TimeUntilRemove_5() { return &___m_TimeUntilRemove_5; }
	inline void set_m_TimeUntilRemove_5(float value)
	{
		___m_TimeUntilRemove_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHOREXAMPLE_T2657819511_H
#ifndef UNITYREMOTEVIDEO_T705138647_H
#define UNITYREMOTEVIDEO_T705138647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityRemoteVideo
struct  UnityRemoteVideo_t705138647  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.XR.iOS.ConnectToEditor UnityEngine.XR.iOS.UnityRemoteVideo::connectToEditor
	ConnectToEditor_t595742893 * ___connectToEditor_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityRemoteVideo::m_Session
	UnityARSessionNativeInterface_t3929719369 * ___m_Session_3;
	// System.Boolean UnityEngine.XR.iOS.UnityRemoteVideo::bTexturesInitialized
	bool ___bTexturesInitialized_4;
	// System.Int32 UnityEngine.XR.iOS.UnityRemoteVideo::currentFrameIndex
	int32_t ___currentFrameIndex_5;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes
	ByteU5BU5D_t4116647657* ___m_textureYBytes_6;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes
	ByteU5BU5D_t4116647657* ___m_textureUVBytes_7;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes2
	ByteU5BU5D_t4116647657* ___m_textureYBytes2_8;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes2
	ByteU5BU5D_t4116647657* ___m_textureUVBytes2_9;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedYArray
	GCHandle_t3351438187  ___m_pinnedYArray_10;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedUVArray
	GCHandle_t3351438187  ___m_pinnedUVArray_11;

public:
	inline static int32_t get_offset_of_connectToEditor_2() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___connectToEditor_2)); }
	inline ConnectToEditor_t595742893 * get_connectToEditor_2() const { return ___connectToEditor_2; }
	inline ConnectToEditor_t595742893 ** get_address_of_connectToEditor_2() { return &___connectToEditor_2; }
	inline void set_connectToEditor_2(ConnectToEditor_t595742893 * value)
	{
		___connectToEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectToEditor_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_4() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___bTexturesInitialized_4)); }
	inline bool get_bTexturesInitialized_4() const { return ___bTexturesInitialized_4; }
	inline bool* get_address_of_bTexturesInitialized_4() { return &___bTexturesInitialized_4; }
	inline void set_bTexturesInitialized_4(bool value)
	{
		___bTexturesInitialized_4 = value;
	}

	inline static int32_t get_offset_of_currentFrameIndex_5() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___currentFrameIndex_5)); }
	inline int32_t get_currentFrameIndex_5() const { return ___currentFrameIndex_5; }
	inline int32_t* get_address_of_currentFrameIndex_5() { return &___currentFrameIndex_5; }
	inline void set_currentFrameIndex_5(int32_t value)
	{
		___currentFrameIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_textureYBytes_6() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureYBytes_6)); }
	inline ByteU5BU5D_t4116647657* get_m_textureYBytes_6() const { return ___m_textureYBytes_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureYBytes_6() { return &___m_textureYBytes_6; }
	inline void set_m_textureYBytes_6(ByteU5BU5D_t4116647657* value)
	{
		___m_textureYBytes_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes_6), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes_7() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureUVBytes_7)); }
	inline ByteU5BU5D_t4116647657* get_m_textureUVBytes_7() const { return ___m_textureUVBytes_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureUVBytes_7() { return &___m_textureUVBytes_7; }
	inline void set_m_textureUVBytes_7(ByteU5BU5D_t4116647657* value)
	{
		___m_textureUVBytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes_7), value);
	}

	inline static int32_t get_offset_of_m_textureYBytes2_8() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureYBytes2_8)); }
	inline ByteU5BU5D_t4116647657* get_m_textureYBytes2_8() const { return ___m_textureYBytes2_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureYBytes2_8() { return &___m_textureYBytes2_8; }
	inline void set_m_textureYBytes2_8(ByteU5BU5D_t4116647657* value)
	{
		___m_textureYBytes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes2_8), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes2_9() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureUVBytes2_9)); }
	inline ByteU5BU5D_t4116647657* get_m_textureUVBytes2_9() const { return ___m_textureUVBytes2_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureUVBytes2_9() { return &___m_textureUVBytes2_9; }
	inline void set_m_textureUVBytes2_9(ByteU5BU5D_t4116647657* value)
	{
		___m_textureUVBytes2_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes2_9), value);
	}

	inline static int32_t get_offset_of_m_pinnedYArray_10() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_pinnedYArray_10)); }
	inline GCHandle_t3351438187  get_m_pinnedYArray_10() const { return ___m_pinnedYArray_10; }
	inline GCHandle_t3351438187 * get_address_of_m_pinnedYArray_10() { return &___m_pinnedYArray_10; }
	inline void set_m_pinnedYArray_10(GCHandle_t3351438187  value)
	{
		___m_pinnedYArray_10 = value;
	}

	inline static int32_t get_offset_of_m_pinnedUVArray_11() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_pinnedUVArray_11)); }
	inline GCHandle_t3351438187  get_m_pinnedUVArray_11() const { return ___m_pinnedUVArray_11; }
	inline GCHandle_t3351438187 * get_address_of_m_pinnedUVArray_11() { return &___m_pinnedUVArray_11; }
	inline void set_m_pinnedUVArray_11(GCHandle_t3351438187  value)
	{
		___m_pinnedUVArray_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREMOTEVIDEO_T705138647_H
#ifndef EDITORHITTEST_T1253817588_H
#define EDITORHITTEST_T1253817588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.EditorHitTest
struct  EditorHitTest_t1253817588  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.EditorHitTest::m_HitTransform
	Transform_t3600365921 * ___m_HitTransform_2;
	// System.Single UnityEngine.XR.iOS.EditorHitTest::maxRayDistance
	float ___maxRayDistance_3;
	// UnityEngine.LayerMask UnityEngine.XR.iOS.EditorHitTest::collisionLayerMask
	LayerMask_t3493934918  ___collisionLayerMask_4;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___m_HitTransform_2)); }
	inline Transform_t3600365921 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3600365921 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3600365921 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_3() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___maxRayDistance_3)); }
	inline float get_maxRayDistance_3() const { return ___maxRayDistance_3; }
	inline float* get_address_of_maxRayDistance_3() { return &___maxRayDistance_3; }
	inline void set_maxRayDistance_3(float value)
	{
		___maxRayDistance_3 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_4() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___collisionLayerMask_4)); }
	inline LayerMask_t3493934918  get_collisionLayerMask_4() const { return ___collisionLayerMask_4; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayerMask_4() { return &___collisionLayerMask_4; }
	inline void set_collisionLayerMask_4(LayerMask_t3493934918  value)
	{
		___collisionLayerMask_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHITTEST_T1253817588_H
#ifndef CONNECTTOEDITOR_T595742893_H
#define CONNECTTOEDITOR_T595742893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectToEditor
struct  ConnectToEditor_t595742893  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.ConnectToEditor::playerConnection
	PlayerConnection_t3081694049 * ___playerConnection_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.ConnectToEditor::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// System.Int32 UnityEngine.XR.iOS.ConnectToEditor::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.ConnectToEditor::frameBufferTex
	Texture2D_t3840446185 * ___frameBufferTex_5;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___playerConnection_2)); }
	inline PlayerConnection_t3081694049 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t3081694049 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t3081694049 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_frameBufferTex_5() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___frameBufferTex_5)); }
	inline Texture2D_t3840446185 * get_frameBufferTex_5() const { return ___frameBufferTex_5; }
	inline Texture2D_t3840446185 ** get_address_of_frameBufferTex_5() { return &___frameBufferTex_5; }
	inline void set_frameBufferTex_5(Texture2D_t3840446185 * value)
	{
		___frameBufferTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameBufferTex_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOEDITOR_T595742893_H
#ifndef ARSESSIONMANAGER_T3964960879_H
#define ARSESSIONMANAGER_T3964960879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARSessionManager
struct  ARSessionManager_t3964960879  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ARSessionManager::m_camera
	Camera_t4157153871 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface ARSessionManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// ARMapper ARSessionManager::arMapArea
	ARMapper_t696765295 * ___arMapArea_4;
	// UnityEngine.XR.iOS.UnityARAlignment ARSessionManager::startAlignment
	int32_t ___startAlignment_5;
	// UnityEngine.XR.iOS.UnityARPlaneDetection ARSessionManager::planeDetection
	int32_t ___planeDetection_6;
	// ARReferenceImagesSet ARSessionManager::detectionImages
	ARReferenceImagesSet_t4271437859 * ___detectionImages_7;
	// System.Boolean ARSessionManager::getPointCloud
	bool ___getPointCloud_8;
	// System.Boolean ARSessionManager::enableLightEstimation
	bool ___enableLightEstimation_9;
	// System.Boolean ARSessionManager::enableAutoFocus
	bool ___enableAutoFocus_10;
	// System.Boolean ARSessionManager::sessionStarted
	bool ___sessionStarted_11;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___m_camera_2)); }
	inline Camera_t4157153871 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t4157153871 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t4157153871 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_arMapArea_4() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___arMapArea_4)); }
	inline ARMapper_t696765295 * get_arMapArea_4() const { return ___arMapArea_4; }
	inline ARMapper_t696765295 ** get_address_of_arMapArea_4() { return &___arMapArea_4; }
	inline void set_arMapArea_4(ARMapper_t696765295 * value)
	{
		___arMapArea_4 = value;
		Il2CppCodeGenWriteBarrier((&___arMapArea_4), value);
	}

	inline static int32_t get_offset_of_startAlignment_5() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___startAlignment_5)); }
	inline int32_t get_startAlignment_5() const { return ___startAlignment_5; }
	inline int32_t* get_address_of_startAlignment_5() { return &___startAlignment_5; }
	inline void set_startAlignment_5(int32_t value)
	{
		___startAlignment_5 = value;
	}

	inline static int32_t get_offset_of_planeDetection_6() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___planeDetection_6)); }
	inline int32_t get_planeDetection_6() const { return ___planeDetection_6; }
	inline int32_t* get_address_of_planeDetection_6() { return &___planeDetection_6; }
	inline void set_planeDetection_6(int32_t value)
	{
		___planeDetection_6 = value;
	}

	inline static int32_t get_offset_of_detectionImages_7() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___detectionImages_7)); }
	inline ARReferenceImagesSet_t4271437859 * get_detectionImages_7() const { return ___detectionImages_7; }
	inline ARReferenceImagesSet_t4271437859 ** get_address_of_detectionImages_7() { return &___detectionImages_7; }
	inline void set_detectionImages_7(ARReferenceImagesSet_t4271437859 * value)
	{
		___detectionImages_7 = value;
		Il2CppCodeGenWriteBarrier((&___detectionImages_7), value);
	}

	inline static int32_t get_offset_of_getPointCloud_8() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___getPointCloud_8)); }
	inline bool get_getPointCloud_8() const { return ___getPointCloud_8; }
	inline bool* get_address_of_getPointCloud_8() { return &___getPointCloud_8; }
	inline void set_getPointCloud_8(bool value)
	{
		___getPointCloud_8 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_9() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___enableLightEstimation_9)); }
	inline bool get_enableLightEstimation_9() const { return ___enableLightEstimation_9; }
	inline bool* get_address_of_enableLightEstimation_9() { return &___enableLightEstimation_9; }
	inline void set_enableLightEstimation_9(bool value)
	{
		___enableLightEstimation_9 = value;
	}

	inline static int32_t get_offset_of_enableAutoFocus_10() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___enableAutoFocus_10)); }
	inline bool get_enableAutoFocus_10() const { return ___enableAutoFocus_10; }
	inline bool* get_address_of_enableAutoFocus_10() { return &___enableAutoFocus_10; }
	inline void set_enableAutoFocus_10(bool value)
	{
		___enableAutoFocus_10 = value;
	}

	inline static int32_t get_offset_of_sessionStarted_11() { return static_cast<int32_t>(offsetof(ARSessionManager_t3964960879, ___sessionStarted_11)); }
	inline bool get_sessionStarted_11() const { return ___sessionStarted_11; }
	inline bool* get_address_of_sessionStarted_11() { return &___sessionStarted_11; }
	inline void set_sessionStarted_11(bool value)
	{
		___sessionStarted_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONMANAGER_T3964960879_H
#ifndef ARMAPPER_T696765295_H
#define ARMAPPER_T696765295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMapper
struct  ARMapper_t696765295  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ARMapper::statusText
	Text_t1901882714 * ___statusText_2;
	// UnityEngine.XR.iOS.ARWorldMap ARMapper::<mappedWorld>k__BackingField
	ARWorldMap_t2240790807 * ___U3CmappedWorldU3Ek__BackingField_3;
	// System.Boolean ARMapper::<worldMapSaved>k__BackingField
	bool ___U3CworldMapSavedU3Ek__BackingField_4;
	// System.Boolean ARMapper::hasStartedMapping
	bool ___hasStartedMapping_5;
	// System.Boolean ARMapper::mappingDone
	bool ___mappingDone_6;
	// UnityEngine.XR.iOS.ARWorldMappingStatus ARMapper::currentMapStatus
	int32_t ___currentMapStatus_7;

public:
	inline static int32_t get_offset_of_statusText_2() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___statusText_2)); }
	inline Text_t1901882714 * get_statusText_2() const { return ___statusText_2; }
	inline Text_t1901882714 ** get_address_of_statusText_2() { return &___statusText_2; }
	inline void set_statusText_2(Text_t1901882714 * value)
	{
		___statusText_2 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_2), value);
	}

	inline static int32_t get_offset_of_U3CmappedWorldU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___U3CmappedWorldU3Ek__BackingField_3)); }
	inline ARWorldMap_t2240790807 * get_U3CmappedWorldU3Ek__BackingField_3() const { return ___U3CmappedWorldU3Ek__BackingField_3; }
	inline ARWorldMap_t2240790807 ** get_address_of_U3CmappedWorldU3Ek__BackingField_3() { return &___U3CmappedWorldU3Ek__BackingField_3; }
	inline void set_U3CmappedWorldU3Ek__BackingField_3(ARWorldMap_t2240790807 * value)
	{
		___U3CmappedWorldU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmappedWorldU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CworldMapSavedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___U3CworldMapSavedU3Ek__BackingField_4)); }
	inline bool get_U3CworldMapSavedU3Ek__BackingField_4() const { return ___U3CworldMapSavedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CworldMapSavedU3Ek__BackingField_4() { return &___U3CworldMapSavedU3Ek__BackingField_4; }
	inline void set_U3CworldMapSavedU3Ek__BackingField_4(bool value)
	{
		___U3CworldMapSavedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_hasStartedMapping_5() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___hasStartedMapping_5)); }
	inline bool get_hasStartedMapping_5() const { return ___hasStartedMapping_5; }
	inline bool* get_address_of_hasStartedMapping_5() { return &___hasStartedMapping_5; }
	inline void set_hasStartedMapping_5(bool value)
	{
		___hasStartedMapping_5 = value;
	}

	inline static int32_t get_offset_of_mappingDone_6() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___mappingDone_6)); }
	inline bool get_mappingDone_6() const { return ___mappingDone_6; }
	inline bool* get_address_of_mappingDone_6() { return &___mappingDone_6; }
	inline void set_mappingDone_6(bool value)
	{
		___mappingDone_6 = value;
	}

	inline static int32_t get_offset_of_currentMapStatus_7() { return static_cast<int32_t>(offsetof(ARMapper_t696765295, ___currentMapStatus_7)); }
	inline int32_t get_currentMapStatus_7() const { return ___currentMapStatus_7; }
	inline int32_t* get_address_of_currentMapStatus_7() { return &___currentMapStatus_7; }
	inline void set_currentMapStatus_7(int32_t value)
	{
		___currentMapStatus_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMAPPER_T696765295_H
#ifndef OBJECTTEXT_T3366579983_H
#define OBJECTTEXT_T3366579983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectText
struct  ObjectText_t3366579983  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.TextMesh ObjectText::textMesh
	TextMesh_t1536577757 * ___textMesh_2;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(ObjectText_t3366579983, ___textMesh_2)); }
	inline TextMesh_t1536577757 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1536577757 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1536577757 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTEXT_T3366579983_H
#ifndef PICKBOUNDINGBOX_T873364976_H
#define PICKBOUNDINGBOX_T873364976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.PickBoundingBox
struct  PickBoundingBox_t873364976  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Top
	Transform_t3600365921 * ___m_Top_2;
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Bottom
	Transform_t3600365921 * ___m_Bottom_3;
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Left
	Transform_t3600365921 * ___m_Left_4;
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Right
	Transform_t3600365921 * ___m_Right_5;
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Back
	Transform_t3600365921 * ___m_Back_6;
	// UnityEngine.Transform UnityEngine.XR.iOS.PickBoundingBox::m_Front
	Transform_t3600365921 * ___m_Front_7;
	// UnityEngine.Material UnityEngine.XR.iOS.PickBoundingBox::m_UnselectedMaterial
	Material_t340375123 * ___m_UnselectedMaterial_8;
	// UnityEngine.Material UnityEngine.XR.iOS.PickBoundingBox::m_SelectedMaterial
	Material_t340375123 * ___m_SelectedMaterial_9;
	// UnityEngine.RaycastHit UnityEngine.XR.iOS.PickBoundingBox::m_PhysicsHit
	RaycastHit_t1056001966  ___m_PhysicsHit_10;
	// UnityEngine.XR.iOS.ARHitTestResult UnityEngine.XR.iOS.PickBoundingBox::m_ARHit
	ARHitTestResult_t1279023930  ___m_ARHit_11;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.PickBoundingBox::m_BeganFacePosition
	Vector3_t3722313464  ___m_BeganFacePosition_12;
	// UnityEngine.XR.iOS.PickBoundingBox/HitType UnityEngine.XR.iOS.PickBoundingBox::m_LastHitType
	int32_t ___m_LastHitType_13;
	// System.Single UnityEngine.XR.iOS.PickBoundingBox::m_InitialPinchDistance
	float ___m_InitialPinchDistance_14;
	// UnityEngine.Bounds UnityEngine.XR.iOS.PickBoundingBox::m_InitialPinchBounds
	Bounds_t2266837910  ___m_InitialPinchBounds_15;
	// System.Single UnityEngine.XR.iOS.PickBoundingBox::m_TurnAngleDelta
	float ___m_TurnAngleDelta_20;
	// System.Single UnityEngine.XR.iOS.PickBoundingBox::m_PinchDistanceDelta
	float ___m_PinchDistanceDelta_21;
	// System.Single UnityEngine.XR.iOS.PickBoundingBox::m_PinchDistance
	float ___m_PinchDistance_22;

public:
	inline static int32_t get_offset_of_m_Top_2() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Top_2)); }
	inline Transform_t3600365921 * get_m_Top_2() const { return ___m_Top_2; }
	inline Transform_t3600365921 ** get_address_of_m_Top_2() { return &___m_Top_2; }
	inline void set_m_Top_2(Transform_t3600365921 * value)
	{
		___m_Top_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Top_2), value);
	}

	inline static int32_t get_offset_of_m_Bottom_3() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Bottom_3)); }
	inline Transform_t3600365921 * get_m_Bottom_3() const { return ___m_Bottom_3; }
	inline Transform_t3600365921 ** get_address_of_m_Bottom_3() { return &___m_Bottom_3; }
	inline void set_m_Bottom_3(Transform_t3600365921 * value)
	{
		___m_Bottom_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bottom_3), value);
	}

	inline static int32_t get_offset_of_m_Left_4() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Left_4)); }
	inline Transform_t3600365921 * get_m_Left_4() const { return ___m_Left_4; }
	inline Transform_t3600365921 ** get_address_of_m_Left_4() { return &___m_Left_4; }
	inline void set_m_Left_4(Transform_t3600365921 * value)
	{
		___m_Left_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Left_4), value);
	}

	inline static int32_t get_offset_of_m_Right_5() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Right_5)); }
	inline Transform_t3600365921 * get_m_Right_5() const { return ___m_Right_5; }
	inline Transform_t3600365921 ** get_address_of_m_Right_5() { return &___m_Right_5; }
	inline void set_m_Right_5(Transform_t3600365921 * value)
	{
		___m_Right_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Right_5), value);
	}

	inline static int32_t get_offset_of_m_Back_6() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Back_6)); }
	inline Transform_t3600365921 * get_m_Back_6() const { return ___m_Back_6; }
	inline Transform_t3600365921 ** get_address_of_m_Back_6() { return &___m_Back_6; }
	inline void set_m_Back_6(Transform_t3600365921 * value)
	{
		___m_Back_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Back_6), value);
	}

	inline static int32_t get_offset_of_m_Front_7() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_Front_7)); }
	inline Transform_t3600365921 * get_m_Front_7() const { return ___m_Front_7; }
	inline Transform_t3600365921 ** get_address_of_m_Front_7() { return &___m_Front_7; }
	inline void set_m_Front_7(Transform_t3600365921 * value)
	{
		___m_Front_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Front_7), value);
	}

	inline static int32_t get_offset_of_m_UnselectedMaterial_8() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_UnselectedMaterial_8)); }
	inline Material_t340375123 * get_m_UnselectedMaterial_8() const { return ___m_UnselectedMaterial_8; }
	inline Material_t340375123 ** get_address_of_m_UnselectedMaterial_8() { return &___m_UnselectedMaterial_8; }
	inline void set_m_UnselectedMaterial_8(Material_t340375123 * value)
	{
		___m_UnselectedMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnselectedMaterial_8), value);
	}

	inline static int32_t get_offset_of_m_SelectedMaterial_9() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_SelectedMaterial_9)); }
	inline Material_t340375123 * get_m_SelectedMaterial_9() const { return ___m_SelectedMaterial_9; }
	inline Material_t340375123 ** get_address_of_m_SelectedMaterial_9() { return &___m_SelectedMaterial_9; }
	inline void set_m_SelectedMaterial_9(Material_t340375123 * value)
	{
		___m_SelectedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectedMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_PhysicsHit_10() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_PhysicsHit_10)); }
	inline RaycastHit_t1056001966  get_m_PhysicsHit_10() const { return ___m_PhysicsHit_10; }
	inline RaycastHit_t1056001966 * get_address_of_m_PhysicsHit_10() { return &___m_PhysicsHit_10; }
	inline void set_m_PhysicsHit_10(RaycastHit_t1056001966  value)
	{
		___m_PhysicsHit_10 = value;
	}

	inline static int32_t get_offset_of_m_ARHit_11() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_ARHit_11)); }
	inline ARHitTestResult_t1279023930  get_m_ARHit_11() const { return ___m_ARHit_11; }
	inline ARHitTestResult_t1279023930 * get_address_of_m_ARHit_11() { return &___m_ARHit_11; }
	inline void set_m_ARHit_11(ARHitTestResult_t1279023930  value)
	{
		___m_ARHit_11 = value;
	}

	inline static int32_t get_offset_of_m_BeganFacePosition_12() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_BeganFacePosition_12)); }
	inline Vector3_t3722313464  get_m_BeganFacePosition_12() const { return ___m_BeganFacePosition_12; }
	inline Vector3_t3722313464 * get_address_of_m_BeganFacePosition_12() { return &___m_BeganFacePosition_12; }
	inline void set_m_BeganFacePosition_12(Vector3_t3722313464  value)
	{
		___m_BeganFacePosition_12 = value;
	}

	inline static int32_t get_offset_of_m_LastHitType_13() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_LastHitType_13)); }
	inline int32_t get_m_LastHitType_13() const { return ___m_LastHitType_13; }
	inline int32_t* get_address_of_m_LastHitType_13() { return &___m_LastHitType_13; }
	inline void set_m_LastHitType_13(int32_t value)
	{
		___m_LastHitType_13 = value;
	}

	inline static int32_t get_offset_of_m_InitialPinchDistance_14() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_InitialPinchDistance_14)); }
	inline float get_m_InitialPinchDistance_14() const { return ___m_InitialPinchDistance_14; }
	inline float* get_address_of_m_InitialPinchDistance_14() { return &___m_InitialPinchDistance_14; }
	inline void set_m_InitialPinchDistance_14(float value)
	{
		___m_InitialPinchDistance_14 = value;
	}

	inline static int32_t get_offset_of_m_InitialPinchBounds_15() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_InitialPinchBounds_15)); }
	inline Bounds_t2266837910  get_m_InitialPinchBounds_15() const { return ___m_InitialPinchBounds_15; }
	inline Bounds_t2266837910 * get_address_of_m_InitialPinchBounds_15() { return &___m_InitialPinchBounds_15; }
	inline void set_m_InitialPinchBounds_15(Bounds_t2266837910  value)
	{
		___m_InitialPinchBounds_15 = value;
	}

	inline static int32_t get_offset_of_m_TurnAngleDelta_20() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_TurnAngleDelta_20)); }
	inline float get_m_TurnAngleDelta_20() const { return ___m_TurnAngleDelta_20; }
	inline float* get_address_of_m_TurnAngleDelta_20() { return &___m_TurnAngleDelta_20; }
	inline void set_m_TurnAngleDelta_20(float value)
	{
		___m_TurnAngleDelta_20 = value;
	}

	inline static int32_t get_offset_of_m_PinchDistanceDelta_21() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_PinchDistanceDelta_21)); }
	inline float get_m_PinchDistanceDelta_21() const { return ___m_PinchDistanceDelta_21; }
	inline float* get_address_of_m_PinchDistanceDelta_21() { return &___m_PinchDistanceDelta_21; }
	inline void set_m_PinchDistanceDelta_21(float value)
	{
		___m_PinchDistanceDelta_21 = value;
	}

	inline static int32_t get_offset_of_m_PinchDistance_22() { return static_cast<int32_t>(offsetof(PickBoundingBox_t873364976, ___m_PinchDistance_22)); }
	inline float get_m_PinchDistance_22() const { return ___m_PinchDistance_22; }
	inline float* get_address_of_m_PinchDistance_22() { return &___m_PinchDistance_22; }
	inline void set_m_PinchDistance_22(float value)
	{
		___m_PinchDistance_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKBOUNDINGBOX_T873364976_H
#ifndef OBJECTSCANSESSIONMANAGER_T3985853727_H
#define OBJECTSCANSESSIONMANAGER_T3985853727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectScanSessionManager
struct  ObjectScanSessionManager_t3985853727  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ObjectScanSessionManager::m_camera
	Camera_t4157153871 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface ObjectScanSessionManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.XR.iOS.UnityARAlignment ObjectScanSessionManager::startAlignment
	int32_t ___startAlignment_4;
	// UnityEngine.XR.iOS.UnityARPlaneDetection ObjectScanSessionManager::planeDetection
	int32_t ___planeDetection_5;
	// System.Boolean ObjectScanSessionManager::getPointCloudData
	bool ___getPointCloudData_6;
	// System.Boolean ObjectScanSessionManager::enableLightEstimation
	bool ___enableLightEstimation_7;
	// System.Boolean ObjectScanSessionManager::enableAutoFocus
	bool ___enableAutoFocus_8;
	// System.Boolean ObjectScanSessionManager::sessionStarted
	bool ___sessionStarted_9;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___m_camera_2)); }
	inline Camera_t4157153871 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t4157153871 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t4157153871 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_startAlignment_4() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___startAlignment_4)); }
	inline int32_t get_startAlignment_4() const { return ___startAlignment_4; }
	inline int32_t* get_address_of_startAlignment_4() { return &___startAlignment_4; }
	inline void set_startAlignment_4(int32_t value)
	{
		___startAlignment_4 = value;
	}

	inline static int32_t get_offset_of_planeDetection_5() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___planeDetection_5)); }
	inline int32_t get_planeDetection_5() const { return ___planeDetection_5; }
	inline int32_t* get_address_of_planeDetection_5() { return &___planeDetection_5; }
	inline void set_planeDetection_5(int32_t value)
	{
		___planeDetection_5 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_6() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___getPointCloudData_6)); }
	inline bool get_getPointCloudData_6() const { return ___getPointCloudData_6; }
	inline bool* get_address_of_getPointCloudData_6() { return &___getPointCloudData_6; }
	inline void set_getPointCloudData_6(bool value)
	{
		___getPointCloudData_6 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_7() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___enableLightEstimation_7)); }
	inline bool get_enableLightEstimation_7() const { return ___enableLightEstimation_7; }
	inline bool* get_address_of_enableLightEstimation_7() { return &___enableLightEstimation_7; }
	inline void set_enableLightEstimation_7(bool value)
	{
		___enableLightEstimation_7 = value;
	}

	inline static int32_t get_offset_of_enableAutoFocus_8() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___enableAutoFocus_8)); }
	inline bool get_enableAutoFocus_8() const { return ___enableAutoFocus_8; }
	inline bool* get_address_of_enableAutoFocus_8() { return &___enableAutoFocus_8; }
	inline void set_enableAutoFocus_8(bool value)
	{
		___enableAutoFocus_8 = value;
	}

	inline static int32_t get_offset_of_sessionStarted_9() { return static_cast<int32_t>(offsetof(ObjectScanSessionManager_t3985853727, ___sessionStarted_9)); }
	inline bool get_sessionStarted_9() const { return ___sessionStarted_9; }
	inline bool* get_address_of_sessionStarted_9() { return &___sessionStarted_9; }
	inline void set_sessionStarted_9(bool value)
	{
		___sessionStarted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSCANSESSIONMANAGER_T3985853727_H
#ifndef TONGUEDETECTOR_T2247763851_H
#define TONGUEDETECTOR_T2247763851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TongueDetector
struct  TongueDetector_t2247763851  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TongueDetector::tongueImage
	GameObject_t1113636619 * ___tongueImage_2;
	// System.Boolean TongueDetector::shapeEnabled
	bool ___shapeEnabled_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> TongueDetector::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_4;

public:
	inline static int32_t get_offset_of_tongueImage_2() { return static_cast<int32_t>(offsetof(TongueDetector_t2247763851, ___tongueImage_2)); }
	inline GameObject_t1113636619 * get_tongueImage_2() const { return ___tongueImage_2; }
	inline GameObject_t1113636619 ** get_address_of_tongueImage_2() { return &___tongueImage_2; }
	inline void set_tongueImage_2(GameObject_t1113636619 * value)
	{
		___tongueImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___tongueImage_2), value);
	}

	inline static int32_t get_offset_of_shapeEnabled_3() { return static_cast<int32_t>(offsetof(TongueDetector_t2247763851, ___shapeEnabled_3)); }
	inline bool get_shapeEnabled_3() const { return ___shapeEnabled_3; }
	inline bool* get_address_of_shapeEnabled_3() { return &___shapeEnabled_3; }
	inline void set_shapeEnabled_3(bool value)
	{
		___shapeEnabled_3 = value;
	}

	inline static int32_t get_offset_of_currentBlendShapes_4() { return static_cast<int32_t>(offsetof(TongueDetector_t2247763851, ___currentBlendShapes_4)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_4() const { return ___currentBlendShapes_4; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_4() { return &___currentBlendShapes_4; }
	inline void set_currentBlendShapes_4(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONGUEDETECTOR_T2247763851_H
#ifndef SVBOXSLIDER_T4192470748_H
#define SVBOXSLIDER_T4192470748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVBoxSlider
struct  SVBoxSlider_t4192470748  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker SVBoxSlider::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.UI.BoxSlider SVBoxSlider::slider
	BoxSlider_t2380464200 * ___slider_3;
	// UnityEngine.UI.RawImage SVBoxSlider::image
	RawImage_t3182918964 * ___image_4;
	// System.Single SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___slider_3)); }
	inline BoxSlider_t2380464200 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t2380464200 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t2380464200 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___image_4)); }
	inline RawImage_t3182918964 * get_image_4() const { return ___image_4; }
	inline RawImage_t3182918964 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t3182918964 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T4192470748_H
#ifndef COLORSLIDERIMAGE_T1393030097_H
#define COLORSLIDERIMAGE_T1393030097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSliderImage
struct  ColorSliderImage_t1393030097  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorSliderImage::picker
	ColorPicker_t228004619 * ___picker_2;
	// ColorValues ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage ColorSliderImage::image
	RawImage_t3182918964 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___image_5)); }
	inline RawImage_t3182918964 * get_image_5() const { return ___image_5; }
	inline RawImage_t3182918964 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t3182918964 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T1393030097_H
#ifndef COLORSLIDER_T2624382019_H
#define COLORSLIDER_T2624382019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSlider
struct  ColorSlider_t2624382019  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorSlider::hsvpicker
	ColorPicker_t228004619 * ___hsvpicker_2;
	// ColorValues ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider ColorSlider::slider
	Slider_t3903728902 * ___slider_4;
	// System.Boolean ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___hsvpicker_2)); }
	inline ColorPicker_t228004619 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t228004619 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t228004619 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T2624382019_H
#ifndef COLORPRESETS_T2117877396_H
#define COLORPRESETS_T2117877396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPresets
struct  ColorPresets_t2117877396  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorPresets::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.GameObject[] ColorPresets::presets
	GameObjectU5BU5D_t3328599146* ___presets_3;
	// UnityEngine.UI.Image ColorPresets::createPresetImage
	Image_t2670269651 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___presets_3)); }
	inline GameObjectU5BU5D_t3328599146* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t3328599146* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___createPresetImage_4)); }
	inline Image_t2670269651 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t2670269651 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t2670269651 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPRESETS_T2117877396_H
#ifndef COLORPICKER_T228004619_H
#define COLORPICKER_T228004619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t228004619  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ColorPicker::_hue
	float ____hue_2;
	// System.Single ColorPicker::_saturation
	float ____saturation_3;
	// System.Single ColorPicker::_brightness
	float ____brightness_4;
	// System.Single ColorPicker::_red
	float ____red_5;
	// System.Single ColorPicker::_green
	float ____green_6;
	// System.Single ColorPicker::_blue
	float ____blue_7;
	// System.Single ColorPicker::_alpha
	float ____alpha_8;
	// ColorChangedEvent ColorPicker::onValueChanged
	ColorChangedEvent_t3019780707 * ___onValueChanged_9;
	// HSVChangedEvent ColorPicker::onHSVChanged
	HSVChangedEvent_t911780251 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ___onValueChanged_9)); }
	inline ColorChangedEvent_t3019780707 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t3019780707 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t3019780707 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t911780251 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t911780251 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t911780251 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKER_T228004619_H
#ifndef COLORLABEL_T2272707290_H
#define COLORLABEL_T2272707290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLabel
struct  ColorLabel_t2272707290  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorLabel::picker
	ColorPicker_t228004619 * ___picker_2;
	// ColorValues ColorLabel::type
	int32_t ___type_3;
	// System.String ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single ColorLabel::minValue
	float ___minValue_5;
	// System.Single ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text ColorLabel::label
	Text_t1901882714 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___label_8)); }
	inline Text_t1901882714 * get_label_8() const { return ___label_8; }
	inline Text_t1901882714 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t1901882714 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T2272707290_H
#ifndef COLORIMAGE_T1922452376_H
#define COLORIMAGE_T1922452376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorImage
struct  ColorImage_t1922452376  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorImage::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.UI.Image ColorImage::image
	Image_t2670269651 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t1922452376, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t1922452376, ___image_3)); }
	inline Image_t2670269651 * get_image_3() const { return ___image_3; }
	inline Image_t2670269651 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2670269651 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T1922452376_H
#ifndef PRINTBOUNDS_T2722994133_H
#define PRINTBOUNDS_T2722994133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.PrintBounds
struct  PrintBounds_t2722994133  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.XR.iOS.PickBoundingBox UnityEngine.XR.iOS.PrintBounds::m_Picker
	PickBoundingBox_t873364976 * ___m_Picker_2;
	// UnityEngine.UI.Text UnityEngine.XR.iOS.PrintBounds::m_BoundsText
	Text_t1901882714 * ___m_BoundsText_3;

public:
	inline static int32_t get_offset_of_m_Picker_2() { return static_cast<int32_t>(offsetof(PrintBounds_t2722994133, ___m_Picker_2)); }
	inline PickBoundingBox_t873364976 * get_m_Picker_2() const { return ___m_Picker_2; }
	inline PickBoundingBox_t873364976 ** get_address_of_m_Picker_2() { return &___m_Picker_2; }
	inline void set_m_Picker_2(PickBoundingBox_t873364976 * value)
	{
		___m_Picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Picker_2), value);
	}

	inline static int32_t get_offset_of_m_BoundsText_3() { return static_cast<int32_t>(offsetof(PrintBounds_t2722994133, ___m_BoundsText_3)); }
	inline Text_t1901882714 * get_m_BoundsText_3() const { return ___m_BoundsText_3; }
	inline Text_t1901882714 ** get_address_of_m_BoundsText_3() { return &___m_BoundsText_3; }
	inline void set_m_BoundsText_3(Text_t1901882714 * value)
	{
		___m_BoundsText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundsText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINTBOUNDS_T2722994133_H
#ifndef COLORPICKERTESTER_T3074432426_H
#define COLORPICKERTESTER_T3074432426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPickerTester
struct  ColorPickerTester_t3074432426  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer ColorPickerTester::renderer
	Renderer_t2627027031 * ___renderer_2;
	// ColorPicker ColorPickerTester::picker
	ColorPicker_t228004619 * ___picker_3;

public:
	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3074432426, ___renderer_2)); }
	inline Renderer_t2627027031 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t2627027031 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3074432426, ___picker_3)); }
	inline ColorPicker_t228004619 * get_picker_3() const { return ___picker_3; }
	inline ColorPicker_t228004619 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPicker_t228004619 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T3074432426_H
#ifndef MODESWITCHER_T3643344453_H
#define MODESWITCHER_T3643344453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSwitcher
struct  ModeSwitcher_t3643344453  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ModeSwitcher::ballMake
	GameObject_t1113636619 * ___ballMake_2;
	// UnityEngine.GameObject ModeSwitcher::ballMove
	GameObject_t1113636619 * ___ballMove_3;
	// System.Int32 ModeSwitcher::appMode
	int32_t ___appMode_4;

public:
	inline static int32_t get_offset_of_ballMake_2() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___ballMake_2)); }
	inline GameObject_t1113636619 * get_ballMake_2() const { return ___ballMake_2; }
	inline GameObject_t1113636619 ** get_address_of_ballMake_2() { return &___ballMake_2; }
	inline void set_ballMake_2(GameObject_t1113636619 * value)
	{
		___ballMake_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballMake_2), value);
	}

	inline static int32_t get_offset_of_ballMove_3() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___ballMove_3)); }
	inline GameObject_t1113636619 * get_ballMove_3() const { return ___ballMove_3; }
	inline GameObject_t1113636619 ** get_address_of_ballMove_3() { return &___ballMove_3; }
	inline void set_ballMove_3(GameObject_t1113636619 * value)
	{
		___ballMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___ballMove_3), value);
	}

	inline static int32_t get_offset_of_appMode_4() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___appMode_4)); }
	inline int32_t get_appMode_4() const { return ___appMode_4; }
	inline int32_t* get_address_of_appMode_4() { return &___appMode_4; }
	inline void set_appMode_4(int32_t value)
	{
		___appMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESWITCHER_T3643344453_H
#ifndef TILTWINDOW_T335293945_H
#define TILTWINDOW_T335293945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltWindow
struct  TiltWindow_t335293945  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 TiltWindow::range
	Vector2_t2156229523  ___range_2;
	// UnityEngine.Transform TiltWindow::mTrans
	Transform_t3600365921 * ___mTrans_3;
	// UnityEngine.Quaternion TiltWindow::mStart
	Quaternion_t2301928331  ___mStart_4;
	// UnityEngine.Vector2 TiltWindow::mRot
	Vector2_t2156229523  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___range_2)); }
	inline Vector2_t2156229523  get_range_2() const { return ___range_2; }
	inline Vector2_t2156229523 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t2156229523  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mTrans_3)); }
	inline Transform_t3600365921 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3600365921 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3600365921 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mStart_4)); }
	inline Quaternion_t2301928331  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t2301928331 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t2301928331  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mRot_5)); }
	inline Vector2_t2156229523  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t2156229523 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t2156229523  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T335293945_H
#ifndef BALLMOVER_T2920303374_H
#define BALLMOVER_T2920303374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMover
struct  BallMover_t2920303374  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BallMover::collBallPrefab
	GameObject_t1113636619 * ___collBallPrefab_2;
	// System.Single BallMover::maxRayDistance
	float ___maxRayDistance_3;
	// UnityEngine.LayerMask BallMover::collisionLayer
	LayerMask_t3493934918  ___collisionLayer_4;
	// UnityEngine.GameObject BallMover::collBallGO
	GameObject_t1113636619 * ___collBallGO_5;

public:
	inline static int32_t get_offset_of_collBallPrefab_2() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___collBallPrefab_2)); }
	inline GameObject_t1113636619 * get_collBallPrefab_2() const { return ___collBallPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_collBallPrefab_2() { return &___collBallPrefab_2; }
	inline void set_collBallPrefab_2(GameObject_t1113636619 * value)
	{
		___collBallPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___collBallPrefab_2), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_3() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___maxRayDistance_3)); }
	inline float get_maxRayDistance_3() const { return ___maxRayDistance_3; }
	inline float* get_address_of_maxRayDistance_3() { return &___maxRayDistance_3; }
	inline void set_maxRayDistance_3(float value)
	{
		___maxRayDistance_3 = value;
	}

	inline static int32_t get_offset_of_collisionLayer_4() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___collisionLayer_4)); }
	inline LayerMask_t3493934918  get_collisionLayer_4() const { return ___collisionLayer_4; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayer_4() { return &___collisionLayer_4; }
	inline void set_collisionLayer_4(LayerMask_t3493934918  value)
	{
		___collisionLayer_4 = value;
	}

	inline static int32_t get_offset_of_collBallGO_5() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___collBallGO_5)); }
	inline GameObject_t1113636619 * get_collBallGO_5() const { return ___collBallGO_5; }
	inline GameObject_t1113636619 ** get_address_of_collBallGO_5() { return &___collBallGO_5; }
	inline void set_collBallGO_5(GameObject_t1113636619 * value)
	{
		___collBallGO_5 = value;
		Il2CppCodeGenWriteBarrier((&___collBallGO_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMOVER_T2920303374_H
#ifndef UNITYEYEMANAGER_T3949445033_H
#define UNITYEYEMANAGER_T3949445033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEyeManager
struct  UnityEyeManager_t3949445033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityEyeManager::eyePrefab
	GameObject_t1113636619 * ___eyePrefab_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEyeManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.GameObject UnityEyeManager::leftEyeGo
	GameObject_t1113636619 * ___leftEyeGo_4;
	// UnityEngine.GameObject UnityEyeManager::rightEyeGo
	GameObject_t1113636619 * ___rightEyeGo_5;

public:
	inline static int32_t get_offset_of_eyePrefab_2() { return static_cast<int32_t>(offsetof(UnityEyeManager_t3949445033, ___eyePrefab_2)); }
	inline GameObject_t1113636619 * get_eyePrefab_2() const { return ___eyePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_eyePrefab_2() { return &___eyePrefab_2; }
	inline void set_eyePrefab_2(GameObject_t1113636619 * value)
	{
		___eyePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___eyePrefab_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityEyeManager_t3949445033, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_leftEyeGo_4() { return static_cast<int32_t>(offsetof(UnityEyeManager_t3949445033, ___leftEyeGo_4)); }
	inline GameObject_t1113636619 * get_leftEyeGo_4() const { return ___leftEyeGo_4; }
	inline GameObject_t1113636619 ** get_address_of_leftEyeGo_4() { return &___leftEyeGo_4; }
	inline void set_leftEyeGo_4(GameObject_t1113636619 * value)
	{
		___leftEyeGo_4 = value;
		Il2CppCodeGenWriteBarrier((&___leftEyeGo_4), value);
	}

	inline static int32_t get_offset_of_rightEyeGo_5() { return static_cast<int32_t>(offsetof(UnityEyeManager_t3949445033, ___rightEyeGo_5)); }
	inline GameObject_t1113636619 * get_rightEyeGo_5() const { return ___rightEyeGo_5; }
	inline GameObject_t1113636619 ** get_address_of_rightEyeGo_5() { return &___rightEyeGo_5; }
	inline void set_rightEyeGo_5(GameObject_t1113636619 * value)
	{
		___rightEyeGo_5 = value;
		Il2CppCodeGenWriteBarrier((&___rightEyeGo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEYEMANAGER_T3949445033_H
#ifndef BALLZ_T1012779874_H
#define BALLZ_T1012779874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ballz
struct  Ballz_t1012779874  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Ballz::yDistanceThreshold
	float ___yDistanceThreshold_2;
	// System.Single Ballz::startingY
	float ___startingY_3;

public:
	inline static int32_t get_offset_of_yDistanceThreshold_2() { return static_cast<int32_t>(offsetof(Ballz_t1012779874, ___yDistanceThreshold_2)); }
	inline float get_yDistanceThreshold_2() const { return ___yDistanceThreshold_2; }
	inline float* get_address_of_yDistanceThreshold_2() { return &___yDistanceThreshold_2; }
	inline void set_yDistanceThreshold_2(float value)
	{
		___yDistanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_startingY_3() { return static_cast<int32_t>(offsetof(Ballz_t1012779874, ___startingY_3)); }
	inline float get_startingY_3() const { return ___startingY_3; }
	inline float* get_address_of_startingY_3() { return &___startingY_3; }
	inline void set_startingY_3(float value)
	{
		___startingY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLZ_T1012779874_H
#ifndef BLENDSHAPEDRIVER_T961242622_H
#define BLENDSHAPEDRIVER_T961242622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendshapeDriver
struct  BlendshapeDriver_t961242622  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SkinnedMeshRenderer BlendshapeDriver::skinnedMeshRenderer
	SkinnedMeshRenderer_t245602842 * ___skinnedMeshRenderer_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> BlendshapeDriver::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_3;

public:
	inline static int32_t get_offset_of_skinnedMeshRenderer_2() { return static_cast<int32_t>(offsetof(BlendshapeDriver_t961242622, ___skinnedMeshRenderer_2)); }
	inline SkinnedMeshRenderer_t245602842 * get_skinnedMeshRenderer_2() const { return ___skinnedMeshRenderer_2; }
	inline SkinnedMeshRenderer_t245602842 ** get_address_of_skinnedMeshRenderer_2() { return &___skinnedMeshRenderer_2; }
	inline void set_skinnedMeshRenderer_2(SkinnedMeshRenderer_t245602842 * value)
	{
		___skinnedMeshRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skinnedMeshRenderer_2), value);
	}

	inline static int32_t get_offset_of_currentBlendShapes_3() { return static_cast<int32_t>(offsetof(BlendshapeDriver_t961242622, ___currentBlendShapes_3)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_3() const { return ___currentBlendShapes_3; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_3() { return &___currentBlendShapes_3; }
	inline void set_currentBlendShapes_3(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEDRIVER_T961242622_H
#ifndef BLENDSHAPEPRINTER_T4276887874_H
#define BLENDSHAPEPRINTER_T4276887874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendshapePrinter
struct  BlendshapePrinter_t4276887874  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BlendshapePrinter::shapeEnabled
	bool ___shapeEnabled_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> BlendshapePrinter::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_3;

public:
	inline static int32_t get_offset_of_shapeEnabled_2() { return static_cast<int32_t>(offsetof(BlendshapePrinter_t4276887874, ___shapeEnabled_2)); }
	inline bool get_shapeEnabled_2() const { return ___shapeEnabled_2; }
	inline bool* get_address_of_shapeEnabled_2() { return &___shapeEnabled_2; }
	inline void set_shapeEnabled_2(bool value)
	{
		___shapeEnabled_2 = value;
	}

	inline static int32_t get_offset_of_currentBlendShapes_3() { return static_cast<int32_t>(offsetof(BlendshapePrinter_t4276887874, ___currentBlendShapes_3)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_3() const { return ___currentBlendShapes_3; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_3() { return &___currentBlendShapes_3; }
	inline void set_currentBlendShapes_3(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEPRINTER_T4276887874_H
#ifndef ARCAMERATRACKER_T1108422940_H
#define ARCAMERATRACKER_T1108422940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARCameraTracker
struct  ARCameraTracker_t1108422940  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ARCameraTracker::trackedCamera
	Camera_t4157153871 * ___trackedCamera_2;
	// System.Boolean ARCameraTracker::sessionStarted
	bool ___sessionStarted_3;

public:
	inline static int32_t get_offset_of_trackedCamera_2() { return static_cast<int32_t>(offsetof(ARCameraTracker_t1108422940, ___trackedCamera_2)); }
	inline Camera_t4157153871 * get_trackedCamera_2() const { return ___trackedCamera_2; }
	inline Camera_t4157153871 ** get_address_of_trackedCamera_2() { return &___trackedCamera_2; }
	inline void set_trackedCamera_2(Camera_t4157153871 * value)
	{
		___trackedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___trackedCamera_2), value);
	}

	inline static int32_t get_offset_of_sessionStarted_3() { return static_cast<int32_t>(offsetof(ARCameraTracker_t1108422940, ___sessionStarted_3)); }
	inline bool get_sessionStarted_3() const { return ___sessionStarted_3; }
	inline bool* get_address_of_sessionStarted_3() { return &___sessionStarted_3; }
	inline void set_sessionStarted_3(bool value)
	{
		___sessionStarted_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERATRACKER_T1108422940_H
#ifndef UNITYARFACEMESHMANAGER_T3766034196_H
#define UNITYARFACEMESHMANAGER_T3766034196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARFaceMeshManager
struct  UnityARFaceMeshManager_t3766034196  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshFilter UnityARFaceMeshManager::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARFaceMeshManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.Mesh UnityARFaceMeshManager::faceMesh
	Mesh_t3648964284 * ___faceMesh_4;

public:
	inline static int32_t get_offset_of_meshFilter_2() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___meshFilter_2)); }
	inline MeshFilter_t3523625662 * get_meshFilter_2() const { return ___meshFilter_2; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_2() { return &___meshFilter_2; }
	inline void set_meshFilter_2(MeshFilter_t3523625662 * value)
	{
		___meshFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_faceMesh_4() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___faceMesh_4)); }
	inline Mesh_t3648964284 * get_faceMesh_4() const { return ___faceMesh_4; }
	inline Mesh_t3648964284 ** get_address_of_faceMesh_4() { return &___faceMesh_4; }
	inline void set_faceMesh_4(Mesh_t3648964284 * value)
	{
		___faceMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEMESHMANAGER_T3766034196_H
#ifndef FOCUSSQUARE_T2880014214_H
#define FOCUSSQUARE_T2880014214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocusSquare
struct  FocusSquare_t2880014214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FocusSquare::findingSquare
	GameObject_t1113636619 * ___findingSquare_2;
	// UnityEngine.GameObject FocusSquare::foundSquare
	GameObject_t1113636619 * ___foundSquare_3;
	// System.Single FocusSquare::maxRayDistance
	float ___maxRayDistance_4;
	// UnityEngine.LayerMask FocusSquare::collisionLayerMask
	LayerMask_t3493934918  ___collisionLayerMask_5;
	// System.Single FocusSquare::findingSquareDist
	float ___findingSquareDist_6;
	// FocusSquare/FocusState FocusSquare::squareState
	int32_t ___squareState_7;
	// System.Boolean FocusSquare::trackingInitialized
	bool ___trackingInitialized_8;

public:
	inline static int32_t get_offset_of_findingSquare_2() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___findingSquare_2)); }
	inline GameObject_t1113636619 * get_findingSquare_2() const { return ___findingSquare_2; }
	inline GameObject_t1113636619 ** get_address_of_findingSquare_2() { return &___findingSquare_2; }
	inline void set_findingSquare_2(GameObject_t1113636619 * value)
	{
		___findingSquare_2 = value;
		Il2CppCodeGenWriteBarrier((&___findingSquare_2), value);
	}

	inline static int32_t get_offset_of_foundSquare_3() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___foundSquare_3)); }
	inline GameObject_t1113636619 * get_foundSquare_3() const { return ___foundSquare_3; }
	inline GameObject_t1113636619 ** get_address_of_foundSquare_3() { return &___foundSquare_3; }
	inline void set_foundSquare_3(GameObject_t1113636619 * value)
	{
		___foundSquare_3 = value;
		Il2CppCodeGenWriteBarrier((&___foundSquare_3), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_4() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___maxRayDistance_4)); }
	inline float get_maxRayDistance_4() const { return ___maxRayDistance_4; }
	inline float* get_address_of_maxRayDistance_4() { return &___maxRayDistance_4; }
	inline void set_maxRayDistance_4(float value)
	{
		___maxRayDistance_4 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_5() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___collisionLayerMask_5)); }
	inline LayerMask_t3493934918  get_collisionLayerMask_5() const { return ___collisionLayerMask_5; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayerMask_5() { return &___collisionLayerMask_5; }
	inline void set_collisionLayerMask_5(LayerMask_t3493934918  value)
	{
		___collisionLayerMask_5 = value;
	}

	inline static int32_t get_offset_of_findingSquareDist_6() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___findingSquareDist_6)); }
	inline float get_findingSquareDist_6() const { return ___findingSquareDist_6; }
	inline float* get_address_of_findingSquareDist_6() { return &___findingSquareDist_6; }
	inline void set_findingSquareDist_6(float value)
	{
		___findingSquareDist_6 = value;
	}

	inline static int32_t get_offset_of_squareState_7() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___squareState_7)); }
	inline int32_t get_squareState_7() const { return ___squareState_7; }
	inline int32_t* get_address_of_squareState_7() { return &___squareState_7; }
	inline void set_squareState_7(int32_t value)
	{
		___squareState_7 = value;
	}

	inline static int32_t get_offset_of_trackingInitialized_8() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___trackingInitialized_8)); }
	inline bool get_trackingInitialized_8() const { return ___trackingInitialized_8; }
	inline bool* get_address_of_trackingInitialized_8() { return &___trackingInitialized_8; }
	inline void set_trackingInitialized_8(bool value)
	{
		___trackingInitialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSSQUARE_T2880014214_H
#ifndef BALLMAKER_T4057675501_H
#define BALLMAKER_T4057675501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMaker
struct  BallMaker_t4057675501  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BallMaker::ballPrefab
	GameObject_t1113636619 * ___ballPrefab_2;
	// System.Single BallMaker::createHeight
	float ___createHeight_3;
	// System.Single BallMaker::maxRayDistance
	float ___maxRayDistance_4;
	// UnityEngine.LayerMask BallMaker::collisionLayer
	LayerMask_t3493934918  ___collisionLayer_5;
	// UnityEngine.MaterialPropertyBlock BallMaker::props
	MaterialPropertyBlock_t3213117958 * ___props_6;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___ballPrefab_2)); }
	inline GameObject_t1113636619 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t1113636619 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_2), value);
	}

	inline static int32_t get_offset_of_createHeight_3() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___createHeight_3)); }
	inline float get_createHeight_3() const { return ___createHeight_3; }
	inline float* get_address_of_createHeight_3() { return &___createHeight_3; }
	inline void set_createHeight_3(float value)
	{
		___createHeight_3 = value;
	}

	inline static int32_t get_offset_of_maxRayDistance_4() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___maxRayDistance_4)); }
	inline float get_maxRayDistance_4() const { return ___maxRayDistance_4; }
	inline float* get_address_of_maxRayDistance_4() { return &___maxRayDistance_4; }
	inline void set_maxRayDistance_4(float value)
	{
		___maxRayDistance_4 = value;
	}

	inline static int32_t get_offset_of_collisionLayer_5() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___collisionLayer_5)); }
	inline LayerMask_t3493934918  get_collisionLayer_5() const { return ___collisionLayer_5; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayer_5() { return &___collisionLayer_5; }
	inline void set_collisionLayer_5(LayerMask_t3493934918  value)
	{
		___collisionLayer_5 = value;
	}

	inline static int32_t get_offset_of_props_6() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___props_6)); }
	inline MaterialPropertyBlock_t3213117958 * get_props_6() const { return ___props_6; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_props_6() { return &___props_6; }
	inline void set_props_6(MaterialPropertyBlock_t3213117958 * value)
	{
		___props_6 = value;
		Il2CppCodeGenWriteBarrier((&___props_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMAKER_T4057675501_H
#ifndef UNITYARFACEANCHORMANAGER_T1630882107_H
#define UNITYARFACEANCHORMANAGER_T1630882107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARFaceAnchorManager
struct  UnityARFaceAnchorManager_t1630882107  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityARFaceAnchorManager::anchorPrefab
	GameObject_t1113636619 * ___anchorPrefab_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARFaceAnchorManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;

public:
	inline static int32_t get_offset_of_anchorPrefab_2() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorManager_t1630882107, ___anchorPrefab_2)); }
	inline GameObject_t1113636619 * get_anchorPrefab_2() const { return ___anchorPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_anchorPrefab_2() { return &___anchorPrefab_2; }
	inline void set_anchorPrefab_2(GameObject_t1113636619 * value)
	{
		___anchorPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___anchorPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorManager_t1630882107, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEANCHORMANAGER_T1630882107_H
#ifndef ARLOCATIONSYNC_T2697562060_H
#define ARLOCATIONSYNC_T2697562060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARLocationSync
struct  ARLocationSync_t2697562060  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ARLocationSync::statusGO
	GameObject_t1113636619 * ___statusGO_2;
	// UnityEngine.UI.Text ARLocationSync::statusText
	Text_t1901882714 * ___statusText_3;
	// UnityEngine.XR.iOS.ARTrackingState ARLocationSync::_arTrackingState
	int32_t ____arTrackingState_4;
	// UnityEngine.XR.iOS.ARTrackingStateReason ARLocationSync::_arTrackingStateReason
	int32_t ____arTrackingStateReason_5;

public:
	inline static int32_t get_offset_of_statusGO_2() { return static_cast<int32_t>(offsetof(ARLocationSync_t2697562060, ___statusGO_2)); }
	inline GameObject_t1113636619 * get_statusGO_2() const { return ___statusGO_2; }
	inline GameObject_t1113636619 ** get_address_of_statusGO_2() { return &___statusGO_2; }
	inline void set_statusGO_2(GameObject_t1113636619 * value)
	{
		___statusGO_2 = value;
		Il2CppCodeGenWriteBarrier((&___statusGO_2), value);
	}

	inline static int32_t get_offset_of_statusText_3() { return static_cast<int32_t>(offsetof(ARLocationSync_t2697562060, ___statusText_3)); }
	inline Text_t1901882714 * get_statusText_3() const { return ___statusText_3; }
	inline Text_t1901882714 ** get_address_of_statusText_3() { return &___statusText_3; }
	inline void set_statusText_3(Text_t1901882714 * value)
	{
		___statusText_3 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_3), value);
	}

	inline static int32_t get_offset_of__arTrackingState_4() { return static_cast<int32_t>(offsetof(ARLocationSync_t2697562060, ____arTrackingState_4)); }
	inline int32_t get__arTrackingState_4() const { return ____arTrackingState_4; }
	inline int32_t* get_address_of__arTrackingState_4() { return &____arTrackingState_4; }
	inline void set__arTrackingState_4(int32_t value)
	{
		____arTrackingState_4 = value;
	}

	inline static int32_t get_offset_of__arTrackingStateReason_5() { return static_cast<int32_t>(offsetof(ARLocationSync_t2697562060, ____arTrackingStateReason_5)); }
	inline int32_t get__arTrackingStateReason_5() const { return ____arTrackingStateReason_5; }
	inline int32_t* get_address_of__arTrackingStateReason_5() { return &____arTrackingStateReason_5; }
	inline void set__arTrackingStateReason_5(int32_t value)
	{
		____arTrackingStateReason_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARLOCATIONSYNC_T2697562060_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef PLAYERSCRIPT_T1783516946_H
#define PLAYERSCRIPT_T1783516946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerScript
struct  PlayerScript_t1783516946  : public NetworkBehaviour_t204670959
{
public:

public:
};

struct PlayerScript_t1783516946_StaticFields
{
public:
	// System.Int32 PlayerScript::kCmdCmdSendWorldMap
	int32_t ___kCmdCmdSendWorldMap_8;

public:
	inline static int32_t get_offset_of_kCmdCmdSendWorldMap_8() { return static_cast<int32_t>(offsetof(PlayerScript_t1783516946_StaticFields, ___kCmdCmdSendWorldMap_8)); }
	inline int32_t get_kCmdCmdSendWorldMap_8() const { return ___kCmdCmdSendWorldMap_8; }
	inline int32_t* get_address_of_kCmdCmdSendWorldMap_8() { return &___kCmdCmdSendWorldMap_8; }
	inline void set_kCmdCmdSendWorldMap_8(int32_t value)
	{
		___kCmdCmdSendWorldMap_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSCRIPT_T1783516946_H
#ifndef NETWORKTRANSMITTER_T3634351483_H
#define NETWORKTRANSMITTER_T3634351483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkTransmitter
struct  NetworkTransmitter_t3634351483  : public NetworkBehaviour_t204670959
{
public:
	// System.Collections.Generic.List`1<System.Int32> NetworkTransmitter::serverTransmissionIds
	List_1_t128053199 * ___serverTransmissionIds_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,NetworkTransmitter/TransmissionData> NetworkTransmitter::clientTransmissionData
	Dictionary_2_t1652245364 * ___clientTransmissionData_12;
	// UnityEngine.Events.UnityAction`2<System.Int32,System.Byte[]> NetworkTransmitter::OnDataComepletelySent
	UnityAction_2_t1861820427 * ___OnDataComepletelySent_13;
	// UnityEngine.Events.UnityAction`2<System.Int32,System.Byte[]> NetworkTransmitter::OnDataFragmentSent
	UnityAction_2_t1861820427 * ___OnDataFragmentSent_14;
	// UnityEngine.Events.UnityAction`2<System.Int32,System.Byte[]> NetworkTransmitter::OnDataFragmentReceived
	UnityAction_2_t1861820427 * ___OnDataFragmentReceived_15;
	// UnityEngine.Events.UnityAction`2<System.Int32,System.Byte[]> NetworkTransmitter::OnDataCompletelyReceived
	UnityAction_2_t1861820427 * ___OnDataCompletelyReceived_16;

public:
	inline static int32_t get_offset_of_serverTransmissionIds_11() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___serverTransmissionIds_11)); }
	inline List_1_t128053199 * get_serverTransmissionIds_11() const { return ___serverTransmissionIds_11; }
	inline List_1_t128053199 ** get_address_of_serverTransmissionIds_11() { return &___serverTransmissionIds_11; }
	inline void set_serverTransmissionIds_11(List_1_t128053199 * value)
	{
		___serverTransmissionIds_11 = value;
		Il2CppCodeGenWriteBarrier((&___serverTransmissionIds_11), value);
	}

	inline static int32_t get_offset_of_clientTransmissionData_12() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___clientTransmissionData_12)); }
	inline Dictionary_2_t1652245364 * get_clientTransmissionData_12() const { return ___clientTransmissionData_12; }
	inline Dictionary_2_t1652245364 ** get_address_of_clientTransmissionData_12() { return &___clientTransmissionData_12; }
	inline void set_clientTransmissionData_12(Dictionary_2_t1652245364 * value)
	{
		___clientTransmissionData_12 = value;
		Il2CppCodeGenWriteBarrier((&___clientTransmissionData_12), value);
	}

	inline static int32_t get_offset_of_OnDataComepletelySent_13() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___OnDataComepletelySent_13)); }
	inline UnityAction_2_t1861820427 * get_OnDataComepletelySent_13() const { return ___OnDataComepletelySent_13; }
	inline UnityAction_2_t1861820427 ** get_address_of_OnDataComepletelySent_13() { return &___OnDataComepletelySent_13; }
	inline void set_OnDataComepletelySent_13(UnityAction_2_t1861820427 * value)
	{
		___OnDataComepletelySent_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataComepletelySent_13), value);
	}

	inline static int32_t get_offset_of_OnDataFragmentSent_14() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___OnDataFragmentSent_14)); }
	inline UnityAction_2_t1861820427 * get_OnDataFragmentSent_14() const { return ___OnDataFragmentSent_14; }
	inline UnityAction_2_t1861820427 ** get_address_of_OnDataFragmentSent_14() { return &___OnDataFragmentSent_14; }
	inline void set_OnDataFragmentSent_14(UnityAction_2_t1861820427 * value)
	{
		___OnDataFragmentSent_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataFragmentSent_14), value);
	}

	inline static int32_t get_offset_of_OnDataFragmentReceived_15() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___OnDataFragmentReceived_15)); }
	inline UnityAction_2_t1861820427 * get_OnDataFragmentReceived_15() const { return ___OnDataFragmentReceived_15; }
	inline UnityAction_2_t1861820427 ** get_address_of_OnDataFragmentReceived_15() { return &___OnDataFragmentReceived_15; }
	inline void set_OnDataFragmentReceived_15(UnityAction_2_t1861820427 * value)
	{
		___OnDataFragmentReceived_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataFragmentReceived_15), value);
	}

	inline static int32_t get_offset_of_OnDataCompletelyReceived_16() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483, ___OnDataCompletelyReceived_16)); }
	inline UnityAction_2_t1861820427 * get_OnDataCompletelyReceived_16() const { return ___OnDataCompletelyReceived_16; }
	inline UnityAction_2_t1861820427 ** get_address_of_OnDataCompletelyReceived_16() { return &___OnDataCompletelyReceived_16; }
	inline void set_OnDataCompletelyReceived_16(UnityAction_2_t1861820427 * value)
	{
		___OnDataCompletelyReceived_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataCompletelyReceived_16), value);
	}
};

struct NetworkTransmitter_t3634351483_StaticFields
{
public:
	// System.String NetworkTransmitter::LOG_PREFIX
	String_t* ___LOG_PREFIX_8;
	// System.Int32 NetworkTransmitter::defaultBufferSize
	int32_t ___defaultBufferSize_10;
	// System.Int32 NetworkTransmitter::kRpcRpcPrepareToReceiveBytes
	int32_t ___kRpcRpcPrepareToReceiveBytes_17;
	// System.Int32 NetworkTransmitter::kRpcRpcReceiveBytes
	int32_t ___kRpcRpcReceiveBytes_18;

public:
	inline static int32_t get_offset_of_LOG_PREFIX_8() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483_StaticFields, ___LOG_PREFIX_8)); }
	inline String_t* get_LOG_PREFIX_8() const { return ___LOG_PREFIX_8; }
	inline String_t** get_address_of_LOG_PREFIX_8() { return &___LOG_PREFIX_8; }
	inline void set_LOG_PREFIX_8(String_t* value)
	{
		___LOG_PREFIX_8 = value;
		Il2CppCodeGenWriteBarrier((&___LOG_PREFIX_8), value);
	}

	inline static int32_t get_offset_of_defaultBufferSize_10() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483_StaticFields, ___defaultBufferSize_10)); }
	inline int32_t get_defaultBufferSize_10() const { return ___defaultBufferSize_10; }
	inline int32_t* get_address_of_defaultBufferSize_10() { return &___defaultBufferSize_10; }
	inline void set_defaultBufferSize_10(int32_t value)
	{
		___defaultBufferSize_10 = value;
	}

	inline static int32_t get_offset_of_kRpcRpcPrepareToReceiveBytes_17() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483_StaticFields, ___kRpcRpcPrepareToReceiveBytes_17)); }
	inline int32_t get_kRpcRpcPrepareToReceiveBytes_17() const { return ___kRpcRpcPrepareToReceiveBytes_17; }
	inline int32_t* get_address_of_kRpcRpcPrepareToReceiveBytes_17() { return &___kRpcRpcPrepareToReceiveBytes_17; }
	inline void set_kRpcRpcPrepareToReceiveBytes_17(int32_t value)
	{
		___kRpcRpcPrepareToReceiveBytes_17 = value;
	}

	inline static int32_t get_offset_of_kRpcRpcReceiveBytes_18() { return static_cast<int32_t>(offsetof(NetworkTransmitter_t3634351483_StaticFields, ___kRpcRpcReceiveBytes_18)); }
	inline int32_t get_kRpcRpcReceiveBytes_18() const { return ___kRpcRpcReceiveBytes_18; }
	inline int32_t* get_address_of_kRpcRpcReceiveBytes_18() { return &___kRpcRpcReceiveBytes_18; }
	inline void set_kRpcRpcReceiveBytes_18(int32_t value)
	{
		___kRpcRpcReceiveBytes_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKTRANSMITTER_T3634351483_H
#ifndef CUSTOMNETWORKMANGER_T3270576195_H
#define CUSTOMNETWORKMANGER_T3270576195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomNetworkManger
struct  CustomNetworkManger_t3270576195  : public NetworkManager_t468665779
{
public:
	// ARSessionManager CustomNetworkManger::sessionManager
	ARSessionManager_t3964960879 * ___sessionManager_51;
	// UnityEngine.GameObject CustomNetworkManger::homePanel
	GameObject_t1113636619 * ___homePanel_52;
	// UnityARCameraManager CustomNetworkManger::cameraManager
	UnityARCameraManager_t4002280589 * ___cameraManager_53;
	// PlayerScript CustomNetworkManger::currentPlayer
	PlayerScript_t1783516946 * ___currentPlayer_54;

public:
	inline static int32_t get_offset_of_sessionManager_51() { return static_cast<int32_t>(offsetof(CustomNetworkManger_t3270576195, ___sessionManager_51)); }
	inline ARSessionManager_t3964960879 * get_sessionManager_51() const { return ___sessionManager_51; }
	inline ARSessionManager_t3964960879 ** get_address_of_sessionManager_51() { return &___sessionManager_51; }
	inline void set_sessionManager_51(ARSessionManager_t3964960879 * value)
	{
		___sessionManager_51 = value;
		Il2CppCodeGenWriteBarrier((&___sessionManager_51), value);
	}

	inline static int32_t get_offset_of_homePanel_52() { return static_cast<int32_t>(offsetof(CustomNetworkManger_t3270576195, ___homePanel_52)); }
	inline GameObject_t1113636619 * get_homePanel_52() const { return ___homePanel_52; }
	inline GameObject_t1113636619 ** get_address_of_homePanel_52() { return &___homePanel_52; }
	inline void set_homePanel_52(GameObject_t1113636619 * value)
	{
		___homePanel_52 = value;
		Il2CppCodeGenWriteBarrier((&___homePanel_52), value);
	}

	inline static int32_t get_offset_of_cameraManager_53() { return static_cast<int32_t>(offsetof(CustomNetworkManger_t3270576195, ___cameraManager_53)); }
	inline UnityARCameraManager_t4002280589 * get_cameraManager_53() const { return ___cameraManager_53; }
	inline UnityARCameraManager_t4002280589 ** get_address_of_cameraManager_53() { return &___cameraManager_53; }
	inline void set_cameraManager_53(UnityARCameraManager_t4002280589 * value)
	{
		___cameraManager_53 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_53), value);
	}

	inline static int32_t get_offset_of_currentPlayer_54() { return static_cast<int32_t>(offsetof(CustomNetworkManger_t3270576195, ___currentPlayer_54)); }
	inline PlayerScript_t1783516946 * get_currentPlayer_54() const { return ___currentPlayer_54; }
	inline PlayerScript_t1783516946 ** get_address_of_currentPlayer_54() { return &___currentPlayer_54; }
	inline void set_currentPlayer_54(PlayerScript_t1783516946 * value)
	{
		___currentPlayer_54 = value;
		Il2CppCodeGenWriteBarrier((&___currentPlayer_54), value);
	}
};

struct CustomNetworkManger_t3270576195_StaticFields
{
public:
	// CustomNetworkManger CustomNetworkManger::instance
	CustomNetworkManger_t3270576195 * ___instance_50;

public:
	inline static int32_t get_offset_of_instance_50() { return static_cast<int32_t>(offsetof(CustomNetworkManger_t3270576195_StaticFields, ___instance_50)); }
	inline CustomNetworkManger_t3270576195 * get_instance_50() const { return ___instance_50; }
	inline CustomNetworkManger_t3270576195 ** get_address_of_instance_50() { return &___instance_50; }
	inline void set_instance_50(CustomNetworkManger_t3270576195 * value)
	{
		___instance_50 = value;
		Il2CppCodeGenWriteBarrier((&___instance_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNETWORKMANGER_T3270576195_H
#ifndef CUSTOMNETWORKDISCOVERY_T3559261671_H
#define CUSTOMNETWORKDISCOVERY_T3559261671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomNetworkDiscovery
struct  CustomNetworkDiscovery_t3559261671  : public NetworkDiscovery_t986186286
{
public:

public:
};

struct CustomNetworkDiscovery_t3559261671_StaticFields
{
public:
	// CustomNetworkDiscovery CustomNetworkDiscovery::instance
	CustomNetworkDiscovery_t3559261671 * ___instance_21;

public:
	inline static int32_t get_offset_of_instance_21() { return static_cast<int32_t>(offsetof(CustomNetworkDiscovery_t3559261671_StaticFields, ___instance_21)); }
	inline CustomNetworkDiscovery_t3559261671 * get_instance_21() const { return ___instance_21; }
	inline CustomNetworkDiscovery_t3559261671 ** get_address_of_instance_21() { return &___instance_21; }
	inline void set_instance_21(CustomNetworkDiscovery_t3559261671 * value)
	{
		___instance_21 = value;
		Il2CppCodeGenWriteBarrier((&___instance_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNETWORKDISCOVERY_T3559261671_H
#ifndef BOXSLIDER_T2380464200_H
#define BOXSLIDER_T2380464200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_t2380464200  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_20;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t439394298 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_t2156229523  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleRect_16)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_Value_20() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Value_20)); }
	inline float get_m_Value_20() const { return ___m_Value_20; }
	inline float* get_address_of_m_Value_20() { return &___m_Value_20; }
	inline void set_m_Value_20(float value)
	{
		___m_Value_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t439394298 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t439394298 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t439394298 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleTransform_23)); }
	inline Transform_t3600365921 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3600365921 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleContainerRect_24)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Offset_25)); }
	inline Vector2_t2156229523  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t2156229523  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T2380464200_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (ARLocationSync_t2697562060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	ARLocationSync_t2697562060::get_offset_of_statusGO_2(),
	ARLocationSync_t2697562060::get_offset_of_statusText_3(),
	ARLocationSync_t2697562060::get_offset_of__arTrackingState_4(),
	ARLocationSync_t2697562060::get_offset_of__arTrackingStateReason_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CRelocateU3Ec__Iterator0_t661745925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[7] = 
{
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_receivedBytes_0(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U3CarWorldMapU3E__0_1(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U3CsmU3E__0_2(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U24this_3(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U24current_4(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U24disposing_5(),
	U3CRelocateU3Ec__Iterator0_t661745925::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (ARMapper_t696765295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[6] = 
{
	ARMapper_t696765295::get_offset_of_statusText_2(),
	ARMapper_t696765295::get_offset_of_U3CmappedWorldU3Ek__BackingField_3(),
	ARMapper_t696765295::get_offset_of_U3CworldMapSavedU3Ek__BackingField_4(),
	ARMapper_t696765295::get_offset_of_hasStartedMapping_5(),
	ARMapper_t696765295::get_offset_of_mappingDone_6(),
	ARMapper_t696765295::get_offset_of_currentMapStatus_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CMapAreaU3Ec__Iterator0_t980979684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[4] = 
{
	U3CMapAreaU3Ec__Iterator0_t980979684::get_offset_of_U24this_0(),
	U3CMapAreaU3Ec__Iterator0_t980979684::get_offset_of_U24current_1(),
	U3CMapAreaU3Ec__Iterator0_t980979684::get_offset_of_U24disposing_2(),
	U3CMapAreaU3Ec__Iterator0_t980979684::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (ARSessionManager_t3964960879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[10] = 
{
	ARSessionManager_t3964960879::get_offset_of_m_camera_2(),
	ARSessionManager_t3964960879::get_offset_of_m_session_3(),
	ARSessionManager_t3964960879::get_offset_of_arMapArea_4(),
	ARSessionManager_t3964960879::get_offset_of_startAlignment_5(),
	ARSessionManager_t3964960879::get_offset_of_planeDetection_6(),
	ARSessionManager_t3964960879::get_offset_of_detectionImages_7(),
	ARSessionManager_t3964960879::get_offset_of_getPointCloud_8(),
	ARSessionManager_t3964960879::get_offset_of_enableLightEstimation_9(),
	ARSessionManager_t3964960879::get_offset_of_enableAutoFocus_10(),
	ARSessionManager_t3964960879::get_offset_of_sessionStarted_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CMappingCoroutineU3Ec__Iterator0_t2201572394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[4] = 
{
	U3CMappingCoroutineU3Ec__Iterator0_t2201572394::get_offset_of_U24this_0(),
	U3CMappingCoroutineU3Ec__Iterator0_t2201572394::get_offset_of_U24current_1(),
	U3CMappingCoroutineU3Ec__Iterator0_t2201572394::get_offset_of_U24disposing_2(),
	U3CMappingCoroutineU3Ec__Iterator0_t2201572394::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (CustomNetworkDiscovery_t3559261671), -1, sizeof(CustomNetworkDiscovery_t3559261671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	CustomNetworkDiscovery_t3559261671_StaticFields::get_offset_of_instance_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (CustomNetworkManger_t3270576195), -1, sizeof(CustomNetworkManger_t3270576195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[5] = 
{
	CustomNetworkManger_t3270576195_StaticFields::get_offset_of_instance_50(),
	CustomNetworkManger_t3270576195::get_offset_of_sessionManager_51(),
	CustomNetworkManger_t3270576195::get_offset_of_homePanel_52(),
	CustomNetworkManger_t3270576195::get_offset_of_cameraManager_53(),
	CustomNetworkManger_t3270576195::get_offset_of_currentPlayer_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (NetworkTransmitter_t3634351483), -1, sizeof(NetworkTransmitter_t3634351483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[11] = 
{
	NetworkTransmitter_t3634351483_StaticFields::get_offset_of_LOG_PREFIX_8(),
	0,
	NetworkTransmitter_t3634351483_StaticFields::get_offset_of_defaultBufferSize_10(),
	NetworkTransmitter_t3634351483::get_offset_of_serverTransmissionIds_11(),
	NetworkTransmitter_t3634351483::get_offset_of_clientTransmissionData_12(),
	NetworkTransmitter_t3634351483::get_offset_of_OnDataComepletelySent_13(),
	NetworkTransmitter_t3634351483::get_offset_of_OnDataFragmentSent_14(),
	NetworkTransmitter_t3634351483::get_offset_of_OnDataFragmentReceived_15(),
	NetworkTransmitter_t3634351483::get_offset_of_OnDataCompletelyReceived_16(),
	NetworkTransmitter_t3634351483_StaticFields::get_offset_of_kRpcRpcPrepareToReceiveBytes_17(),
	NetworkTransmitter_t3634351483_StaticFields::get_offset_of_kRpcRpcReceiveBytes_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (TransmissionData_t2763532033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[2] = 
{
	TransmissionData_t2763532033::get_offset_of_curDataIndex_0(),
	TransmissionData_t2763532033::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[10] = 
{
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_transmissionId_0(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_data_1(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U3CdataToTransmitU3E__0_2(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U3CbufferSizeU3E__0_3(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U3CremainingU3E__1_4(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U3CbufferU3E__1_5(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U24this_6(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U24current_7(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U24disposing_8(),
	U3CSendBytesToClientsRoutineU3Ec__Iterator0_t487921339::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (PlayerScript_t1783516946), -1, sizeof(PlayerScript_t1783516946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	PlayerScript_t1783516946_StaticFields::get_offset_of_kCmdCmdSendWorldMap_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ConnectionMessageIds_t1387126779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (SubMessageIds_t1008824323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (ConnectToEditor_t595742893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[4] = 
{
	ConnectToEditor_t595742893::get_offset_of_playerConnection_2(),
	ConnectToEditor_t595742893::get_offset_of_m_session_3(),
	ConnectToEditor_t595742893::get_offset_of_editorID_4(),
	ConnectToEditor_t595742893::get_offset_of_frameBufferTex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (EditorHitTest_t1253817588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[3] = 
{
	EditorHitTest_t1253817588::get_offset_of_m_HitTransform_2(),
	EditorHitTest_t1253817588::get_offset_of_maxRayDistance_3(),
	EditorHitTest_t1253817588::get_offset_of_collisionLayerMask_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ObjectSerializationExtension_t1997214439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (SerializableVector4_t2739337940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	SerializableVector4_t2739337940::get_offset_of_x_0(),
	SerializableVector4_t2739337940::get_offset_of_y_1(),
	SerializableVector4_t2739337940::get_offset_of_z_2(),
	SerializableVector4_t2739337940::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (serializableUnityARMatrix4x4_t255097097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[4] = 
{
	serializableUnityARMatrix4x4_t255097097::get_offset_of_column0_0(),
	serializableUnityARMatrix4x4_t255097097::get_offset_of_column1_1(),
	serializableUnityARMatrix4x4_t255097097::get_offset_of_column2_2(),
	serializableUnityARMatrix4x4_t255097097::get_offset_of_column3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (serializableSHC_t226029808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	serializableSHC_t226029808::get_offset_of_shcData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (serializableUnityARLightData_t3029229948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[5] = 
{
	serializableUnityARLightData_t3029229948::get_offset_of_whichLight_0(),
	serializableUnityARLightData_t3029229948::get_offset_of_lightSHC_1(),
	serializableUnityARLightData_t3029229948::get_offset_of_primaryLightDirAndIntensity_2(),
	serializableUnityARLightData_t3029229948::get_offset_of_ambientIntensity_3(),
	serializableUnityARLightData_t3029229948::get_offset_of_ambientColorTemperature_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (serializableUnityARCamera_t1848993995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[9] = 
{
	serializableUnityARCamera_t1848993995::get_offset_of_worldTransform_0(),
	serializableUnityARCamera_t1848993995::get_offset_of_projectionMatrix_1(),
	serializableUnityARCamera_t1848993995::get_offset_of_trackingState_2(),
	serializableUnityARCamera_t1848993995::get_offset_of_trackingReason_3(),
	serializableUnityARCamera_t1848993995::get_offset_of_videoParams_4(),
	serializableUnityARCamera_t1848993995::get_offset_of_lightData_5(),
	serializableUnityARCamera_t1848993995::get_offset_of_pointCloud_6(),
	serializableUnityARCamera_t1848993995::get_offset_of_displayTransform_7(),
	serializableUnityARCamera_t1848993995::get_offset_of_worldMappingStatus_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (serializablePlaneGeometry_t3471745378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[4] = 
{
	serializablePlaneGeometry_t3471745378::get_offset_of_vertices_0(),
	serializablePlaneGeometry_t3471745378::get_offset_of_texCoords_1(),
	serializablePlaneGeometry_t3471745378::get_offset_of_triIndices_2(),
	serializablePlaneGeometry_t3471745378::get_offset_of_boundaryVertices_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (serializableUnityARPlaneAnchor_t3965207599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[6] = 
{
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_worldTransform_0(),
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_center_1(),
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_extent_2(),
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_planeAlignment_3(),
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_planeGeometry_4(),
	serializableUnityARPlaneAnchor_t3965207599::get_offset_of_identifierStr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (serializableFaceGeometry_t1893768467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[3] = 
{
	serializableFaceGeometry_t1893768467::get_offset_of_vertices_0(),
	serializableFaceGeometry_t1893768467::get_offset_of_texCoords_1(),
	serializableFaceGeometry_t1893768467::get_offset_of_triIndices_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (serializableUnityARFaceAnchor_t1413500457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[4] = 
{
	serializableUnityARFaceAnchor_t1413500457::get_offset_of_worldTransform_0(),
	serializableUnityARFaceAnchor_t1413500457::get_offset_of_faceGeometry_1(),
	serializableUnityARFaceAnchor_t1413500457::get_offset_of_arBlendShapes_2(),
	serializableUnityARFaceAnchor_t1413500457::get_offset_of_identifierStr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (serializablePointCloud_t4241265545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[1] = 
{
	serializablePointCloud_t4241265545::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (serializableARSessionConfiguration_t30565192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[5] = 
{
	serializableARSessionConfiguration_t30565192::get_offset_of_alignment_0(),
	serializableARSessionConfiguration_t30565192::get_offset_of_planeDetection_1(),
	serializableARSessionConfiguration_t30565192::get_offset_of_getPointCloudData_2(),
	serializableARSessionConfiguration_t30565192::get_offset_of_enableLightEstimation_3(),
	serializableARSessionConfiguration_t30565192::get_offset_of_enableAutoFocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (serializableARKitInit_t3839227232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	serializableARKitInit_t3839227232::get_offset_of_config_0(),
	serializableARKitInit_t3839227232::get_offset_of_runOption_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (serializableFromEditorMessage_t2731251371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[2] = 
{
	serializableFromEditorMessage_t2731251371::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_t2731251371::get_offset_of_arkitConfigMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (UnityRemoteVideo_t705138647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[10] = 
{
	UnityRemoteVideo_t705138647::get_offset_of_connectToEditor_2(),
	UnityRemoteVideo_t705138647::get_offset_of_m_Session_3(),
	UnityRemoteVideo_t705138647::get_offset_of_bTexturesInitialized_4(),
	UnityRemoteVideo_t705138647::get_offset_of_currentFrameIndex_5(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureYBytes_6(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureUVBytes_7(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureYBytes2_8(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureUVBytes2_9(),
	UnityRemoteVideo_t705138647::get_offset_of_m_pinnedYArray_10(),
	UnityRemoteVideo_t705138647::get_offset_of_m_pinnedUVArray_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (UnityARUserAnchorExample_t2657819511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[4] = 
{
	UnityARUserAnchorExample_t2657819511::get_offset_of_prefabObject_2(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_distanceFromCamera_3(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_m_Clones_4(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_m_TimeUntilRemove_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (GenerateImageAnchor_t3213337420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[3] = 
{
	GenerateImageAnchor_t3213337420::get_offset_of_referenceImage_2(),
	GenerateImageAnchor_t3213337420::get_offset_of_prefabToGenerate_3(),
	GenerateImageAnchor_t3213337420::get_offset_of_imageAnchorGO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (RelocalizationControl_t377950233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[3] = 
{
	RelocalizationControl_t377950233::get_offset_of_buttonText_2(),
	RelocalizationControl_t377950233::get_offset_of_trackingStateText_3(),
	RelocalizationControl_t377950233::get_offset_of_trackingReasonText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (ARKitPlaneMeshRender_t1298532386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	ARKitPlaneMeshRender_t1298532386::get_offset_of_meshFilter_2(),
	ARKitPlaneMeshRender_t1298532386::get_offset_of_lineRenderer_3(),
	ARKitPlaneMeshRender_t1298532386::get_offset_of_planeMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (SetWorldOriginControl_t1961164635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[3] = 
{
	SetWorldOriginControl_t1961164635::get_offset_of_arCamera_2(),
	SetWorldOriginControl_t1961164635::get_offset_of_positionText_3(),
	SetWorldOriginControl_t1961164635::get_offset_of_rotationText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (VideoFormatButton_t1937817916), -1, sizeof(VideoFormatButton_t1937817916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	VideoFormatButton_t1937817916::get_offset_of_videoFormatDescription_2(),
	VideoFormatButton_t1937817916::get_offset_of_arVideoFormat_3(),
	VideoFormatButton_t1937817916_StaticFields::get_offset_of_FormatButtonPressedEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (VideoFormatButtonPressed_t1187798507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (VideoFormatsExample_t2303262312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[2] = 
{
	VideoFormatsExample_t2303262312::get_offset_of_formatsParent_2(),
	VideoFormatsExample_t2303262312::get_offset_of_videoFormatButtonPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (GenerateEnvironmentProbeAnchors_t2290703479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[2] = 
{
	GenerateEnvironmentProbeAnchors_t2290703479::get_offset_of_m_ReflectionProbePrefab_2(),
	GenerateEnvironmentProbeAnchors_t2290703479::get_offset_of_probeAnchorMap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (HitCreateEnvironmentProbe_t2118888515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[2] = 
{
	HitCreateEnvironmentProbe_t2118888515::get_offset_of_maxRayDistance_2(),
	HitCreateEnvironmentProbe_t2118888515::get_offset_of_collisionLayer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (ReflectionProbeGameObject_t172576734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[4] = 
{
	ReflectionProbeGameObject_t172576734::get_offset_of_reflectionProbe_2(),
	ReflectionProbeGameObject_t172576734::get_offset_of_latchUpdate_3(),
	ReflectionProbeGameObject_t172576734::get_offset_of_latchedTexture_4(),
	ReflectionProbeGameObject_t172576734::get_offset_of_debugExtentGO_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (GenerateObjectAnchor_t1604756478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[3] = 
{
	GenerateObjectAnchor_t1604756478::get_offset_of_referenceObjectAsset_2(),
	GenerateObjectAnchor_t1604756478::get_offset_of_prefabToGenerate_3(),
	GenerateObjectAnchor_t1604756478::get_offset_of_objectAnchorGO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (UpdateWorldMappingStatus_t2738391865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[2] = 
{
	UpdateWorldMappingStatus_t2738391865::get_offset_of_text_2(),
	UpdateWorldMappingStatus_t2738391865::get_offset_of_tracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (WorldMapManager_t2538599596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[4] = 
{
	WorldMapManager_t2538599596::get_offset_of_m_ARCameraManager_2(),
	WorldMapManager_t2538599596::get_offset_of_m_LoadedMap_3(),
	WorldMapManager_t2538599596::get_offset_of_serializedWorldMap_4(),
	WorldMapManager_t2538599596::get_offset_of_m_LastReason_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (DetectedObjectManager_t2555724390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[2] = 
{
	DetectedObjectManager_t2555724390::get_offset_of_m_ObjectPrefab_2(),
	DetectedObjectManager_t2555724390::get_offset_of_objectAnchorMap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (ObjectScanManager_t4080581421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[6] = 
{
	ObjectScanManager_t4080581421::get_offset_of_m_ARSessionManager_2(),
	ObjectScanManager_t4080581421::get_offset_of_listOfObjects_3(),
	ObjectScanManager_t4080581421::get_offset_of_objIndex_4(),
	ObjectScanManager_t4080581421::get_offset_of_scannedObjects_5(),
	ObjectScanManager_t4080581421::get_offset_of_detectionMode_6(),
	ObjectScanManager_t4080581421::get_offset_of_pickBoundingBox_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (ObjectScanSessionManager_t3985853727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[8] = 
{
	ObjectScanSessionManager_t3985853727::get_offset_of_m_camera_2(),
	ObjectScanSessionManager_t3985853727::get_offset_of_m_session_3(),
	ObjectScanSessionManager_t3985853727::get_offset_of_startAlignment_4(),
	ObjectScanSessionManager_t3985853727::get_offset_of_planeDetection_5(),
	ObjectScanSessionManager_t3985853727::get_offset_of_getPointCloudData_6(),
	ObjectScanSessionManager_t3985853727::get_offset_of_enableLightEstimation_7(),
	ObjectScanSessionManager_t3985853727::get_offset_of_enableAutoFocus_8(),
	ObjectScanSessionManager_t3985853727::get_offset_of_sessionStarted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (ObjectText_t3366579983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[1] = 
{
	ObjectText_t3366579983::get_offset_of_textMesh_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (PickBoundingBox_t873364976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[21] = 
{
	PickBoundingBox_t873364976::get_offset_of_m_Top_2(),
	PickBoundingBox_t873364976::get_offset_of_m_Bottom_3(),
	PickBoundingBox_t873364976::get_offset_of_m_Left_4(),
	PickBoundingBox_t873364976::get_offset_of_m_Right_5(),
	PickBoundingBox_t873364976::get_offset_of_m_Back_6(),
	PickBoundingBox_t873364976::get_offset_of_m_Front_7(),
	PickBoundingBox_t873364976::get_offset_of_m_UnselectedMaterial_8(),
	PickBoundingBox_t873364976::get_offset_of_m_SelectedMaterial_9(),
	PickBoundingBox_t873364976::get_offset_of_m_PhysicsHit_10(),
	PickBoundingBox_t873364976::get_offset_of_m_ARHit_11(),
	PickBoundingBox_t873364976::get_offset_of_m_BeganFacePosition_12(),
	PickBoundingBox_t873364976::get_offset_of_m_LastHitType_13(),
	PickBoundingBox_t873364976::get_offset_of_m_InitialPinchDistance_14(),
	PickBoundingBox_t873364976::get_offset_of_m_InitialPinchBounds_15(),
	0,
	0,
	0,
	0,
	PickBoundingBox_t873364976::get_offset_of_m_TurnAngleDelta_20(),
	PickBoundingBox_t873364976::get_offset_of_m_PinchDistanceDelta_21(),
	PickBoundingBox_t873364976::get_offset_of_m_PinchDistance_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (HitType_t3206133659)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[5] = 
{
	HitType_t3206133659::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (PrintBounds_t2722994133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[2] = 
{
	PrintBounds_t2722994133::get_offset_of_m_Picker_2(),
	PrintBounds_t2722994133::get_offset_of_m_BoundsText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (TongueDetector_t2247763851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	TongueDetector_t2247763851::get_offset_of_tongueImage_2(),
	TongueDetector_t2247763851::get_offset_of_shapeEnabled_3(),
	TongueDetector_t2247763851::get_offset_of_currentBlendShapes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (UnityEyeManager_t3949445033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	UnityEyeManager_t3949445033::get_offset_of_eyePrefab_2(),
	UnityEyeManager_t3949445033::get_offset_of_m_session_3(),
	UnityEyeManager_t3949445033::get_offset_of_leftEyeGo_4(),
	UnityEyeManager_t3949445033::get_offset_of_rightEyeGo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (ARCameraTracker_t1108422940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	ARCameraTracker_t1108422940::get_offset_of_trackedCamera_2(),
	ARCameraTracker_t1108422940::get_offset_of_sessionStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (BlendshapeDriver_t961242622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	BlendshapeDriver_t961242622::get_offset_of_skinnedMeshRenderer_2(),
	BlendshapeDriver_t961242622::get_offset_of_currentBlendShapes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (BlendshapePrinter_t4276887874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	BlendshapePrinter_t4276887874::get_offset_of_shapeEnabled_2(),
	BlendshapePrinter_t4276887874::get_offset_of_currentBlendShapes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (UnityARFaceAnchorManager_t1630882107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[2] = 
{
	UnityARFaceAnchorManager_t1630882107::get_offset_of_anchorPrefab_2(),
	UnityARFaceAnchorManager_t1630882107::get_offset_of_m_session_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (UnityARFaceMeshManager_t3766034196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	UnityARFaceMeshManager_t3766034196::get_offset_of_meshFilter_2(),
	UnityARFaceMeshManager_t3766034196::get_offset_of_m_session_3(),
	UnityARFaceMeshManager_t3766034196::get_offset_of_faceMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (FocusSquare_t2880014214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[7] = 
{
	FocusSquare_t2880014214::get_offset_of_findingSquare_2(),
	FocusSquare_t2880014214::get_offset_of_foundSquare_3(),
	FocusSquare_t2880014214::get_offset_of_maxRayDistance_4(),
	FocusSquare_t2880014214::get_offset_of_collisionLayerMask_5(),
	FocusSquare_t2880014214::get_offset_of_findingSquareDist_6(),
	FocusSquare_t2880014214::get_offset_of_squareState_7(),
	FocusSquare_t2880014214::get_offset_of_trackingInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (FocusState_t138798281)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[4] = 
{
	FocusState_t138798281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (BallMaker_t4057675501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[5] = 
{
	BallMaker_t4057675501::get_offset_of_ballPrefab_2(),
	BallMaker_t4057675501::get_offset_of_createHeight_3(),
	BallMaker_t4057675501::get_offset_of_maxRayDistance_4(),
	BallMaker_t4057675501::get_offset_of_collisionLayer_5(),
	BallMaker_t4057675501::get_offset_of_props_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (BallMover_t2920303374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[4] = 
{
	BallMover_t2920303374::get_offset_of_collBallPrefab_2(),
	BallMover_t2920303374::get_offset_of_maxRayDistance_3(),
	BallMover_t2920303374::get_offset_of_collisionLayer_4(),
	BallMover_t2920303374::get_offset_of_collBallGO_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (Ballz_t1012779874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[2] = 
{
	Ballz_t1012779874::get_offset_of_yDistanceThreshold_2(),
	Ballz_t1012779874::get_offset_of_startingY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (ModeSwitcher_t3643344453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[3] = 
{
	ModeSwitcher_t3643344453::get_offset_of_ballMake_2(),
	ModeSwitcher_t3643344453::get_offset_of_ballMove_3(),
	ModeSwitcher_t3643344453::get_offset_of_appMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ColorValues_t1603089408)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[8] = 
{
	ColorValues_t1603089408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ColorChangedEvent_t3019780707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (HSVChangedEvent_t911780251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ColorPickerTester_t3074432426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[2] = 
{
	ColorPickerTester_t3074432426::get_offset_of_renderer_2(),
	ColorPickerTester_t3074432426::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (TiltWindow_t335293945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	TiltWindow_t335293945::get_offset_of_range_2(),
	TiltWindow_t335293945::get_offset_of_mTrans_3(),
	TiltWindow_t335293945::get_offset_of_mStart_4(),
	TiltWindow_t335293945::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ColorImage_t1922452376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[2] = 
{
	ColorImage_t1922452376::get_offset_of_picker_2(),
	ColorImage_t1922452376::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ColorLabel_t2272707290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[7] = 
{
	ColorLabel_t2272707290::get_offset_of_picker_2(),
	ColorLabel_t2272707290::get_offset_of_type_3(),
	ColorLabel_t2272707290::get_offset_of_prefix_4(),
	ColorLabel_t2272707290::get_offset_of_minValue_5(),
	ColorLabel_t2272707290::get_offset_of_maxValue_6(),
	ColorLabel_t2272707290::get_offset_of_precision_7(),
	ColorLabel_t2272707290::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ColorPicker_t228004619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[9] = 
{
	ColorPicker_t228004619::get_offset_of__hue_2(),
	ColorPicker_t228004619::get_offset_of__saturation_3(),
	ColorPicker_t228004619::get_offset_of__brightness_4(),
	ColorPicker_t228004619::get_offset_of__red_5(),
	ColorPicker_t228004619::get_offset_of__green_6(),
	ColorPicker_t228004619::get_offset_of__blue_7(),
	ColorPicker_t228004619::get_offset_of__alpha_8(),
	ColorPicker_t228004619::get_offset_of_onValueChanged_9(),
	ColorPicker_t228004619::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ColorPresets_t2117877396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	ColorPresets_t2117877396::get_offset_of_picker_2(),
	ColorPresets_t2117877396::get_offset_of_presets_3(),
	ColorPresets_t2117877396::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ColorSlider_t2624382019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[4] = 
{
	ColorSlider_t2624382019::get_offset_of_hsvpicker_2(),
	ColorSlider_t2624382019::get_offset_of_type_3(),
	ColorSlider_t2624382019::get_offset_of_slider_4(),
	ColorSlider_t2624382019::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ColorSliderImage_t1393030097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	ColorSliderImage_t1393030097::get_offset_of_picker_2(),
	ColorSliderImage_t1393030097::get_offset_of_type_3(),
	ColorSliderImage_t1393030097::get_offset_of_direction_4(),
	ColorSliderImage_t1393030097::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (HexColorField_t944280679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	HexColorField_t944280679::get_offset_of_hsvpicker_2(),
	HexColorField_t944280679::get_offset_of_displayAlpha_3(),
	HexColorField_t944280679::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (SVBoxSlider_t4192470748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[5] = 
{
	SVBoxSlider_t4192470748::get_offset_of_picker_2(),
	SVBoxSlider_t4192470748::get_offset_of_slider_3(),
	SVBoxSlider_t4192470748::get_offset_of_image_4(),
	SVBoxSlider_t4192470748::get_offset_of_lastH_5(),
	SVBoxSlider_t4192470748::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (BoxSlider_t2380464200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[11] = 
{
	BoxSlider_t2380464200::get_offset_of_m_HandleRect_16(),
	BoxSlider_t2380464200::get_offset_of_m_MinValue_17(),
	BoxSlider_t2380464200::get_offset_of_m_MaxValue_18(),
	BoxSlider_t2380464200::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t2380464200::get_offset_of_m_Value_20(),
	BoxSlider_t2380464200::get_offset_of_m_ValueY_21(),
	BoxSlider_t2380464200::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t2380464200::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t2380464200::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t2380464200::get_offset_of_m_Offset_25(),
	BoxSlider_t2380464200::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (Direction_t524882829)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2379[5] = 
{
	Direction_t524882829::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (BoxSliderEvent_t439394298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (Axis_t1354568546)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[3] = 
{
	Axis_t1354568546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (HSVUtil_t1472193074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (HsvColor_t2280895388)+ sizeof (RuntimeObject), sizeof(HsvColor_t2280895388 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2383[3] = 
{
	HsvColor_t2280895388::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t2280895388::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t2280895388::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (ParticlePainter_t1984011264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[14] = 
{
	ParticlePainter_t1984011264::get_offset_of_painterParticlePrefab_2(),
	ParticlePainter_t1984011264::get_offset_of_minDistanceThreshold_3(),
	ParticlePainter_t1984011264::get_offset_of_maxDistanceThreshold_4(),
	ParticlePainter_t1984011264::get_offset_of_frameUpdated_5(),
	ParticlePainter_t1984011264::get_offset_of_particleSize_6(),
	ParticlePainter_t1984011264::get_offset_of_penDistance_7(),
	ParticlePainter_t1984011264::get_offset_of_colorPicker_8(),
	ParticlePainter_t1984011264::get_offset_of_currentPS_9(),
	ParticlePainter_t1984011264::get_offset_of_particles_10(),
	ParticlePainter_t1984011264::get_offset_of_previousPosition_11(),
	ParticlePainter_t1984011264::get_offset_of_currentPaintVertices_12(),
	ParticlePainter_t1984011264::get_offset_of_currentColor_13(),
	ParticlePainter_t1984011264::get_offset_of_paintSystems_14(),
	ParticlePainter_t1984011264::get_offset_of_paintMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (ARReferenceImage_t2463148469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	ARReferenceImage_t2463148469::get_offset_of_imageName_2(),
	ARReferenceImage_t2463148469::get_offset_of_imageTexture_3(),
	ARReferenceImage_t2463148469::get_offset_of_physicalSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (ARReferenceImagesSet_t4271437859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[2] = 
{
	ARReferenceImagesSet_t4271437859::get_offset_of_resourceGroupName_2(),
	ARReferenceImagesSet_t4271437859::get_offset_of_referenceImages_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ARReferenceObjectAsset_t1156404477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	ARReferenceObjectAsset_t1156404477::get_offset_of_objectName_2(),
	ARReferenceObjectAsset_t1156404477::get_offset_of_referenceObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ARReferenceObjectsSetAsset_t1991292594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[2] = 
{
	ARReferenceObjectsSetAsset_t1991292594::get_offset_of_resourceGroupName_2(),
	ARReferenceObjectsSetAsset_t1991292594::get_offset_of_referenceObjectAssets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (ARResourceGroupInfo_t191861313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[2] = 
{
	ARResourceGroupInfo_t191861313::get_offset_of_version_0(),
	ARResourceGroupInfo_t191861313::get_offset_of_author_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (ARResourceGroupResource_t1510500848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	ARResourceGroupResource_t1510500848::get_offset_of_filename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (ARResourceGroupContents_t1795740205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[2] = 
{
	ARResourceGroupContents_t1795740205::get_offset_of_info_0(),
	ARResourceGroupContents_t1795740205::get_offset_of_resources_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ARResourceInfo_t1991666228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[2] = 
{
	ARResourceInfo_t1991666228::get_offset_of_version_0(),
	ARResourceInfo_t1991666228::get_offset_of_author_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (ARResourceProperties_t3627430478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	ARResourceProperties_t3627430478::get_offset_of_width_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (ARResourceFilename_t3614159029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[2] = 
{
	ARResourceFilename_t3614159029::get_offset_of_idiom_0(),
	ARResourceFilename_t3614159029::get_offset_of_filename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (ARResourceContents_t645904403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	ARResourceContents_t645904403::get_offset_of_images_0(),
	ARResourceContents_t645904403::get_offset_of_info_1(),
	ARResourceContents_t645904403::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ARReferenceObjectResourceContents_t4233467830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[3] = 
{
	ARReferenceObjectResourceContents_t4233467830::get_offset_of_objects_0(),
	ARReferenceObjectResourceContents_t4233467830::get_offset_of_info_1(),
	ARReferenceObjectResourceContents_t4233467830::get_offset_of_referenceObjectName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (AR3DOFCameraManager_t1160001149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[3] = 
{
	AR3DOFCameraManager_t1160001149::get_offset_of_m_camera_2(),
	AR3DOFCameraManager_t1160001149::get_offset_of_m_session_3(),
	AR3DOFCameraManager_t1160001149::get_offset_of_savedClearMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (ARPlaneAnchorGameObject_t1947719815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[2] = 
{
	ARPlaneAnchorGameObject_t1947719815::get_offset_of_gameObject_0(),
	ARPlaneAnchorGameObject_t1947719815::get_offset_of_planeAnchor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (DontDestroyOnLoad_t1456007215), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
